<?php get_header(); ?>
    <section id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="container">
    <div class="row">
    <div class="col-sm-9">
    <div class="welcome">
    <h1 class="home">Welcome to <span class="blue">One</span> <span class="orange">Credit</span></h1>
    <p>We can fund your account with up to <span class="blue">&#8358;500,000 within 24 hours</span> of your loan being approved</p>
    </div>
    </div>
    <div class="col-sm-3"><a href="http://goo.gl/KPpDme" target="_blank"><img src="<?php echo get_template_directory_uri();  ?>/images/google_play_icon.png" class="googleplay" width="300" height="105" /></a></div>
    </div>
    
    <div class="calculator">
    <div class="row pay">
    <div class="col-md-3 padit">What is your net monthly pay?</div>
    <div class="col-md-6 hidden-sm hidden-xs slider"><div id="slider3"></div></div>
    <div class="col-md-3 col-sm-12">
    <div class="row">
    <div class="col-md-6"><input type="text" name="pay" class="form-control input3" value="47000"  /></div>
    <div class="col-md-6"><label class="control-label">&nbsp;</label></div>
    </div>
    </div>
    </div>
    <div class="row credit hidden-sm hidden-xs">
    <div class="col-md-3 padit">How much credit do you want?</div>
    <div class="col-md-6 hidden-sm hidden-xs slider"><div id="slider1"></div></div>
    <div class="col-md-3 col-sm-12">
    <div class="row">
    <div class="col-md-6"><input type="text" name="credit" class="form-control input1"  value="40000" /></div>
    <div class="col-md-6"><label class="control-label">Max <br />&#8358;500,000</label></div>
    </div>
    </div>
    </div>
    <div class="row duration hidden-sm hidden-xs">
    <div class="col-md-3 padit">How long do you want it for?</div>
    <div class="col-md-6 hidden-sm hidden-xs slider"><div id="slider2"></div></div>
    <div class="col-md-3 col-sm-12">
    <div class="row">
    <div class="col-md-6"><input type="text" name="duration" class="form-control input2" value="3"  /></div>
    <div class="col-md-6"><label class="control-label">Months</label></div>
    </div>
    </div>
    </div>
    
    <div class="row applybtn">
    <div class="col-md-9 col-sm-8 texti">
    <p class="notify orange"></p>
    <p>Total amount to be repaid will be <strong class="repay"></strong>, your monthly payment will be <strong class="monthly"></strong> for <strong class="days"></strong></p>
    
    <p class="blue" style="cursor:pointer;"><span class="tool" data-toggle="tooltip " data-html="true" title="

    <p>Valid identification i.e. driver's license, international passport or national ID</p>

<p>One recent passport photograph</p>
<p>Bank statements showing 6 (most recent) salary credits</p>
<p>Employment letter and/or promotion letter, confirmation letter or salary review letter(s)</p>
<p>Monthly repayment cheques dated in line with your pay day</p>
<p>Security cheque covering loan amount</p>
<p>Pay slips are compulsory for government workers</p>

<p><em>Note - One Credit reports cases of forged document to the Police</em></p>
    ">Click here to view required documents.</span></p>
    </div>
    <form name="calculater" action="<?php echo home_url(); ?>/apply/" method="post" id="calculatorform" class="calculatorform">
                <input type="hidden" name="credit" id="credit" />
                <input type="hidden" name="duration" id="duration" />
                <input type="hidden" name="pay" id="pay" />
                <input type="hidden" name="installments" id="installments" />
    <div class="col-md-3 col-sm-4"><button class="btn btn-primary btn-lg btn-block apply" >Apply</button></div>
    </form>
    </div>
    </div>
      <!-- Add Paylater -->
      <div class="paylaterad"><a href="http://www.paylater.ng" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/Web-Banners (1).jpg" /></a></div>
    <div class="howitworks">
    <h2 class="blue">How It Works</h2>
    <hr />
    <div class="row">
    <div class="col-md-4 col-sm-4">
    <img src="<?php echo get_template_directory_uri(); ?>/images/how1.png" width="218" height="218" />
    <a href="http://www.one-cred.com/apply/" class="btn btn-primary btn-block" style="margin-top:10px;">Apply online or call us,</a>
    </div>
    <div class="col-md-4 col-sm-4">
    <img src="<?php echo get_template_directory_uri(); ?>/images/how2.png" width="218" height="218" />
    <button class="btn btn-primary btn-block tool" data-toggle="tooltip" data-html="true" title="

    <p>Valid identification i.e. driver's license, international passport or national ID</p>

<p>One recent passport photograph</p>
<p>Bank statements showing 6 (most recent) salary credits</p>
<p>Employment letter and/or promotion letter, confirmation letter or salary review letter(s)</p>
<p>Monthly repayment cheques dated in line with your pay day</p>
<p>Security cheque covering loan amount</p>
<p>Pay slips are compulsory for government workers</p>

<p><em>Note - One Credit reports cases of forged document to the Police</em></p>
    ">we will pick your documents,</button>
    </div>
    <div class="col-md-4 col-sm-4">
    <img src="<?php echo get_template_directory_uri(); ?>/images/how3.png" width="218" height="218" />
    <button class="btn btn-primary btn-block">and credit your bank account.</button>
    </div>
    </div>
    <p>To qualify, you must be 25 years and over, work and live in Lagos with a minimum of one year in current employment, and a current account holder.</p>
    </div>
    </div>
    </section>

<?php get_footer(); ?>