<?php
/*
Template Name: Track Application Page
*/
 ?>
<?php get_header(); ?>
<section id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<div class="container">
<div class="jumbotron trackapp">
  <div class="spinlay">
  <div class="spinner">
      <div class="dot1"></div>
      <div class="dot2"></div>
    </div>
    </div>
  <div class="trackres" style="display:none;">
 
  <h2 class="trackprogress">Your Loan Application Status</h2>
  <div class="trackprogress progress">
    
  <div class="progress-bar progress-bar-primary progress-bar-striped active animated"  role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="100" >
    <span><span class="progresspercent"></span>% Complete</span>
  </div>
</div>
    <small class="text-info trackrep"></small>
  </div>
  <h1>Track Your Application</h1>
  
  <form id="trackapp" name="trackapp" action="#">
  <div class="input-group input-group-lg">
  <input type="text" class="form-control" placeholder="Enter Your Client ID" required="yes">
    <span class="input-group-btn">
        <button class="btn btn-primary" type="button"><i class="fa fa-paper-plane"></i> Check Application Status</button>
      </span>
  </div>
    <small class="text-info">Your Client ID was sent to you in a previous email.</small>
  </form>
</div>
  </div>
</section>
<?php get_footer(); ?>