<footer>
    <div class="container">
    <div class="row">
    <div class="col-md-6 col-sm-6 social">
<?php if ( is_active_sidebar( 'footer-sidebar1' ) ) : ?> <?php dynamic_sidebar( 'footer-sidebar1' ); ?>
            <?php else : ?><p>You need to drag a widget into your sidebar in the WordPress Admin</p>
    <?php endif; ?><!--Socialize with us: <a href="http://www.facebook.com/OneCredit" title="Like Us on FaceBook" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/social_01.png" width="43" height="44" /></a> <a href="http://www.twitter.com/Pastor_Chinedu" title="Follow Us on Twitter" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/social_02.png" width="43" height="45" /></a> <a href="http://plus.google.com/100610491759343813359" title="FaceBook" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/social_04.png" width="43" height="44" /></a> <img src="<?php echo get_template_directory_uri(); ?>/images/social_05.png" width="44" height="45" /> --></div>
    <div class="col-md-6 col-sm-6 subscribe">
    <?php if ( is_active_sidebar( 'footer-sidebar2' ) ) : ?> <?php dynamic_sidebar( 'footer-sidebar2' ); ?>
            <?php else : ?><p>You need to drag a widget into your sidebar in the WordPress Admin</p>
    <?php endif; ?>
    </div>
    </div>
    <p class="copyright">&copy; <?php echo date('Y'); ?> <span class="blue">One</span> <span class="orange">Credit</span></p>
    </div>
    </footer>
    
   <script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.10.2.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-ui.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
 <script src="<?php echo get_template_directory_uri(); ?>/js/onecredit.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.validate.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/datepicker/lib/picker.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/datepicker/lib/picker.date.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/datepicker/lib/legacy.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.bootstrap.wizard.min.js"></script>
<!-- start file upload -->
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/jquery.ui.widget.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="<?php echo get_template_directory_uri(); ?>/js/load-image.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="<?php echo get_template_directory_uri(); ?>/js/canvas-to-blob.min.js"></script>

<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.fileupload-image.js"></script>
<!-- The File Upload audio preview plugin -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.fileupload-audio.js"></script>
<!-- The File Upload video preview plugin -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.fileupload-video.js"></script>
<!-- The File Upload validation plugin -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.fileupload-validate.js"></script>

<!-- Firebase -->
<script src="<?php echo get_template_directory_uri(); ?>/js/facebook/firebase.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/facebook/firebase-simple-login.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/facebook/firebase2.js"></script>

<script src="<?php echo get_template_directory_uri(); ?>/js/modernizr.custom.97074.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.hoverdir.js"></script>

<!-- Get Agents -->
<script>
  var dataRef = new Firebase('https://onecred.firebaseio.com');
  var agentsRef = dataRef.child('/agents/');
  var agents = agentsRef.on('value',function(agentslist){
    $('#loading').show();
     this.$col = parseInt(0);
  this.$count = parseInt(0);
    this.inside = "";
    $('#da-thumbs').html('');
    var num = agentslist.numChildren();
    agentslist.forEach(function(agents){
      this.$col++;
      this.$count++;
      if(this.$col == 1){
        //console.log('start');
        this.inside = this.inside;
        //$('#agents').append('<div class="row">');
      }
      var agent = agents.val();
      var $src = agent.picture;
      var $name = agent.name;
      var $email = agent.email;
      var $phone = agent.phone;
      this.inside = this.inside + '<li><a href="#"><img src="http://www.one-cred.com/app/assets/agents/'+$src+'" alt="'+$name+'" /><div><span>'+$name+'</span></div></a></li>';
     // $('#agents').append('<div class="col-sm-6 col-md-4"><div class="thumbnail"><img src="http://www.one-cred.com/app/assets/agents/'+$src+'" alt="'+$name+'"><div class="caption"><h3>'+$name+'</h3><p>'+$email+'</p><p>'+$phone+'</p><p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p></div></div></div>');
      if(this.$col == 8 || this.$count == num){
        //console.log('end');
        this.$col = 0;
        this.inside = this.inside ;
       // $('#agents').append('</div>');
      }
    });
    $('#da-thumbs').html(this.inside);
    
			$(function() {

				$(' #da-thumbs > li ').each( function() { $(this).hoverdir({
					hoverDelay : 75
				}); } );

			});
		
    $('#loading').hide();
  });
</script>

<script>
/*jslint unparam: true, regexp: true */
/*global window, $ */
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
  var url = '../app/server/php/',
        uploadButton = $('<button/>')
            .addClass('btn btn-primary')
            .prop('disabled', true)
            .text('Processing...')
            .on('click', function () {
                var $this = $(this),
                    data = $this.data();
                $this
                    .off('click')
                    .text('Abort')
                    .on('click', function () {
                        $this.remove();
                        data.abort();
                    });
                data.submit().always(function () {
                    $this.remove();
                });
            });
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        autoUpload: false,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png|doc|docx|pdf)$/i,
        maxFileSize: 5000000, // 5 MB
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 100,
        previewMaxHeight: 100,
        previewCrop: true
    }).on('fileuploadadd', function (e, data) {
        data.context = $('<div/>').appendTo('#files');
        $.each(data.files, function (index, file) {
            var node = $('<p/>')
                    .append($('<span/>').text(file.name));
            if (!index) {
                node
                    .append('<br>')
                    .append(uploadButton.clone(true).data(data));
            }
            node.appendTo(data.context);
        });
    }).on('fileuploadprocessalways', function (e, data) {
        var index = data.index,
            file = data.files[index],
            node = $(data.context.children()[index]);
        if (file.preview) {
            node
                .prepend('<br>')
                .prepend(file.preview);
        }
        if (file.error) {
            node
                .append('<br>')
                .append($('<span class="text-danger"/>').text(file.error));
        }
        if (index + 1 === data.files.length) {
            data.context.find('button')
                .text('Upload')
                .prop('disabled', !!data.files.error);
        }
    }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
            'width',
            progress + '%'
        );
    }).on('fileuploaddone', function (e, data) {
        $.each(data.result.files, function (index, file) {
            if (file.url) {
                var link = $('<a>')
                    .attr('target', '_blank')
                    .prop('href', file.url);
                $(data.context.children()[index])
                    .wrap(link);
              var documents = $('input#documents').val();
              if(!documents){
              var documents = file.url;
                $('input#documents').val(documents);
              }else{
                var documents = documents + "+++" + file.url;
                $('input#documents').val(documents);
              }
            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index])
                    .append('<br>')
                    .append(error);
            }
        });
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index, file) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
            $(data.context.children()[index])
                .append('<br>')
                .append(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
</script>

<script>
    $(document).ready(function(){
        
        
        //fix text inputs
        $("input[type='text'],input[type='email'],textarea,select").addClass('form-control');
        $("input[type='submit']").addClass('btn btn-primary'); 
    });
    </script>
    <script>
$(document).ready(function() {
  	$('#rootwizard').bootstrapWizard(
    {'onTabShow': function(tab, navigation, index) {
		var $total = navigation.find('li').length;
		var $current = index+1;
		var $percent = ($current/$total) * 100;
		$('#rootwizard').find('.bar').css({width:$percent+'%'});
		
		// If it's the last tab then hide the last button and show the finish instead
		if($current >= $total) {
			$('#rootwizard').find('.pager .next').hide();
			$('#rootwizard').find('.pager .finish').show();
			$('#rootwizard').find('.pager .finish').removeClass('disabled');
		} else {
			$('#rootwizard').find('.pager .next').show();
			$('#rootwizard').find('.pager .finish').hide();
		}
		
	},
    'onNext': function(tab, navigation, index) {
	  			var $valid = $("form.applynow").valid();
	  			if(!$valid) {
	  			  $('#alert').show();
	  				$validator.focusInvalid();
	  				return false;
	  			}
	  		},
     'tabClass': 'nav nav-pills',
     'onTabClick': function(){
	  				     return false;
                    }
            }
      );
	});

</script>
<script>
$(document).ready(function(){
    $('#rootwizard a, #rootwizard button').click(function(){
       $('form.applynow').validate({ 
        rules: { 
            mobilenumber: { 
                required: true, 
                number: true, 
                maxlength: 11,
                minlength: 11
                },
            homenumber: { 
                required: true, 
                number: true, 
                maxlength: 11,
                minlength: 11
                },
            employerstelephone: { 
                required: true, 
                number: true, 
                maxlength: 11,
                minlength: 11
                },
            kinmobilenumber: { 
                required: true, 
                number: true, 
                maxlength: 11,
                minlength: 11
                },
            kinmobilenumber: { 
                required: true, 
                number: true, 
                maxlength: 11,
                minlength: 11
                },
            loanamount: { 
                required: true, 
                number: true, 
                max: 500000,
                min: 40000
                },
            accountnumber:{
                required: true, 
                number: true, 
                maxlength: 10,
                minlength: 10
            },
            household:{
                required: true, 
                number: true, 
                maxlength: 2,
            },
            loanamount0:{
                number: true
            },
            loanamount2:{
                number: true
            },
            loanamount3:{
                number: true
            },
            loanbalance:{
                number: true
            },
            monthlyrepayments0:{
                number: true
            },
            loanbalance2:{
                number: true
            },
            monthlyrepayments2:{
                number: true
            },
            loanbalance3:{
                number: true
            },
            monthlyrepayments3:{
                number: true
            }
            
                    } 
                    }); 
           
    });
    
                    
  $('#dateofbirth').pickadate({
    today: '',
    clear: 'Clear selection',
     selectYears: 100,
     selectMonths: true,
     format: 'yyyy-mm-dd'
});

$('#employmentdate').pickadate({
    today: '',
    clear: 'Clear selection',
     selectYears: 50,
     selectMonths: true,
     format: 'yyyy-mm-dd'
});

$('#payday').pickadate({
   today: '',
   clear: 'Clear selection',
   selectYears: 100,
   selectMonths: true,
   format: 'yyyy-mm-dd'
});
  
}

);
</script>
<script>
    $('.tool').tooltip({
      position: {
        my: "center bottom-20",
        at: "center top",
        using: function( position, feedback ) {
          $( this ).css( position );
          $( "<div>" )
            .addClass( "arrow" )
            .addClass( feedback.vertical )
            .addClass( feedback.horizontal )
            .appendTo( this );
        }
      },
      content: function() {
        return $(this).attr('title');
    }
    });
</script>    

<script>
$('#trackapp input').mouseover(function(){
$('#trackapp .text-info').addClass('animated shake');
});
 $('.trackres').hide();
$('.trackprogress').hide();
$(document).ready(function(){
  $('.spinlay').hide('slow');
});
  
$('#trackapp button').click(function(e){
  e.preventDefault();
  $('.spinlay').show('slow');
  $('.trackapp h1').hide();
  $('#trackapp').hide();
  var _clientid = $('#trackapp input').val();
  var app_status = $.get("http://www.one-cred.com/app/users/checkapplicationstatus/"+_clientid)
  app_status.complete(function(data){
    var _response = JSON.parse(data.responseText);
    //console.log(_response);
    switch (_response.clientStatus) {
        case "invalid":
            progresspercentage = 0;
            trackrep = "This account does not exist.";
            break;
        case "inactive":
            progresspercentage = 20;
            trackrep = "We are currently processing your loan.";
            break;
        case "active":
            progresspercentage = 50;
            trackrep = "We are currently processing your loan.";
            break;
    }
    
    switch (_response.loanStatus) {
        case "inactive":
            progresspercentage = 70;
            trackrep = "We are currently processing your loan.";
            break;
        case "active":
            progresspercentage = 100;
            trackrep = "Your loan has been disbured.";
            break;
    }
    $('.trackprogress > div').attr('aria-valuenow',progresspercentage);
    $('.trackprogress > div').css('width',progresspercentage+'%');
    $('.progresspercent').html(progresspercentage);
    $('.trackrep').html(trackrep);
    $('.trackres').show();
    $('.trackprogress').show('slow');
    $('.spinlay').hide('slow');
  });
  
});
</script>

<!-- Agents Search -->
<script>
// Run the code on every text entered
$('#agentfil').keyup(function () {
    // Get the text input value
    var inputval = $(this).val();
    // remove the delegate class from delegate class elements
    $("#da-thumbs li.delegate").removeClass("delegate");
    // Filter through all list element (see http://stackoverflow.com/a/7569871/2131693) and add the delegate class to mark the list elements we are interested in
    var x = $("#da-thumbs li").filter(function () {
        return $(this, "a > div > span").text().substr(0, inputval.length).toLowerCase() === inputval.toLowerCase();
    }).addClass("delegate");
    // show the delegate class elements
    $("#da-thumbs li.delegate").show("slow");
    // hide the non delegate class elements that we are not interested in
    $("#da-thumbs li").not('.delegate').hide('slow');
});
</script>
    
<script type='text/javascript'>(function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://widget.purechat.com/VisitorWidget/WidgetScript'; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({ c: '2526d08f-5f1b-4f33-bab2-66517f649fcc', f: true }); done = true; } }; })();</script>
<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53038520-1', 'auto');
  ga('send', 'pageview');

</script> 
    
   
    <?php wp_footer(); ?>
  </body>

</html>