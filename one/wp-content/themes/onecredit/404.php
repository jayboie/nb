<?php get_header(); ?>
<section id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<div class="container">
<div class="jumbotron">
  <h1>Error 404</h1>
  <p>The page you are looking for does not exist.</p>
  <p><a class="btn btn-primary btn-lg" href="<?php echo home_url(); ?>">Go Home</a></p>
</div>
</div>
</section>
<?php get_footer(); ?>