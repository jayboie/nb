<?php 

/**
 * Template Name: Application Form
 * */

 ?>
 <?php get_header(); ?>
 <section>
 <div class="container">
 <img src="<?php echo get_template_directory_uri(); ?>/images/apply.jpg" width="960" height="200" />
 <br />
 <br />
<?php if(have_posts()):?>
    <?php while(have_posts()): the_post();?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php the_content(); ?>
    </article>
    <?php endwhile; ?>
    <?php else: ?>
    <?php endif;?>
 <div class="crow">
  <div id= "alert" class="alert alert-danger alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
   <strong>Input Error</strong>
                <hr class="message-inner-separator"/>
                <p>
                Change a few things up and try again.</p>
  </div>
   <form role="form" class="applynow" action="<?php echo home_url() ?>/app/users/adduser" method="post">
 <!-- Wizard Starts -->
<div id="rootwizard">
	<ul>
	  	<li><a href="#tab1" data-toggle="tab">Personal Data</a></li>
		<li><a href="#tab2" data-toggle="tab">Income Details</a></li>
		<li><a href="#tab3" data-toggle="tab">Your Existing Loans</a></li>
		<li><a href="#tab4" data-toggle="tab">Employment Details</a></li>
		<li><a href="#tab5" data-toggle="tab">Level of Education</a></li>
		<li><a href="#tab6" data-toggle="tab">Next of Kin/Blood Relative</a></li>
		<li><a href="#tab7" data-toggle="tab">Bank/Disbursement Details</a></li>
        <li><a href="#tab8" data-toggle="tab">Loan Details</a></li>
	</ul>
  <p class="notify"></p>
	<div class="tab-content">
	    <div class="tab-pane" id="tab1">
        <p class="text-info"></p>
	      <fieldset name="Personal Data" title="Personal Data" >
   <div class="form-group">
      <label for="firstname">First Name</label>
      <input type="text" name="firstname" id="firstname" class="form-control" placeholder="First Name" required />
    </div>
   <div class="form-group">
      <label for="middlename">Middle Name</label>
      <input type="text" name="middlename" id="middlename" class="form-control" placeholder="Middle Name" required />
    </div>
    <div class="form-group">
      <label for="surname">Surname</label>
      <input type="text" name="surname" id="surname" class="form-control" placeholder="Surname" required />
    </div>
    <div class="form-group">
      <label for="maidenname">Other/Maiden Name</label>
      <input type="text" name="maidenname" id="maidenname" class="form-control" placeholder="Other/Maiden Name" required />
    </div>
    <div class="form-group">
      <label for="dateofbirth">Date of Birth</label>
      <input type="text" name="dateofbirth" id="dateofbirth" class="form-control" placeholder="Date of Birth" required />
    </div>
    <div class="form-group">
      <label for="mobilenumber">Mobile Number</label>
      <input type="text" name="mobilenumber" id="mobilenumber" class="form-control" placeholder="Mobile Number" required />
    </div>
    <div class="form-group">
      <label for="gender">Gender</label>
      <select name="gender" id="gender" class="form-control" required>
      <option value="">Select...</option>
      <option value="MALE">Male</option>
      <option value="FEMALE">Female</option>
      </select>
    </div>
    <div class="form-group">
      <label for="email">Email Address</label>
      <input type="text" name="email" id="email" class="form-control" placeholder="email" required />
    </div>
    <div class="form-group">
      <label for="homenumber">Home/Landline Number</label>
      <input type="text" name="homenumber" id="homenumber" class="form-control" placeholder="Home/Landline Number" required />
    </div>
    <div class="form-group">
      <label for="meansofidentification">Means of Identification</label>
      <select name="meansofidentification" id="meansofidentification" class="form-control" required>
      <option value="">Select...</option>
      <option value="Passport">Passport</option>
      <option value="Driver's License">Driver's License</option>
      <option value="National ID Card">National ID Card</option>
      </select>
    </div>
    <div class="form-group">
      <label for="idnumber">Identification Number</label>
      <input type="text" name="idnumber" id="idnumber" class="form-control" placeholder="Identification Number" required />
    </div>
    <div class="form-group">
      <label for="otheridentification">Other Means of Identification</label>
      <input type="text" name="otheridentification" id="otheridentification" class="form-control" placeholder="Other Means of Identification" />
    </div>
    <label for="homeaddress">Current Home Address</label>
    <div name="homeaddress" class="row">
        <div class="form-group col-sm-12">
          <input name="addressline1" id="addressline1" class="form-control" placeholder="Address Line 1" required/>
        </div>
        <div class="form-group col-sm-12">
          <input name="addressline2" id="addressline2" class="form-control" placeholder="Address Line 2"/>
        </div>
        <div class="form-group col-sm-6">
          <input name="city" id="city" class="form-control" placeholder="City/Area" required/>
        </div>
        <div class="form-group col-sm-6">
            <select name="state" id="state" class="form-control" required >
            <option value="">State...</option>
                <option value="ABIA">Abia</option>
                <option value="ABUJA">Abuja</option>
                <option value="CROSS RIVER">Cross River</option>
                <option value="DELTA">Delta</option>
                <option value="LAGOS">Lagos</option>
                <option value="OGUN">Ogun</option>
                <option value="OYO">Oyo</option>
            </select>
        </div>
        <div class="form-group col-sm-6">
          <input name="zip" id="zip" class="form-control" placeholder="Zip/Postal code"/>
        </div>
    </div>
    <div class="form-group">
      <label for="nearestlandmark">Nearest Landmark</label>
      <input type="text" name="nearestlandmark" id="nearestlandmark" class="form-control" placeholder="Nearest Landmark" required />
    </div>
    <div class="form-group">
      <label for="nearestbusstop">Nearest Bus Stop</label>
      <input type="text" name="nearestbusstop" id="nearestbusstop" class="form-control" placeholder="Nearest Bus Stop" required />
    </div>
    <div class="form-group">
      <label for="timeatcurrentaddress">Time at Current Address</label>
      <select name="timeatcurrentaddress" id="timeatcurrentaddress" class="form-control" required>
      <option value="">Select...</option>
      <option value="Less Than 1 Year">Less Than 1 Year</option>
      <option value="1 Year">1 Year</option>
      <option value="2 Years">2 Years</option>
      <option value="3 Years">3 Years</option>
      <option value="4 Years">4 Years</option>
      <option value="5 Years">5 Years</option>
      <option value="More Than 5 Years">More Than 5 Years</option>
      </select>
      
    </div>
    <label for="previousaddress">Previous Address</label>
    <div name="previousaddress" class="row">
        <div class="form-group col-sm-12">
          <input name="previousaddressline1" id="previousaddressline1" class="form-control" placeholder="Address Line 1" required/>
        </div>
        <div class="form-group col-sm-12">
          <input name="previousaddressline2" id="previousaddressline2" class="form-control" placeholder="Address Line 2"/>
        </div>
        <div class="form-group col-sm-6">
          <input name="previouscity" id="previouscity" class="form-control" placeholder="City/Area" required/>
        </div>
        <div class="form-group col-sm-6">
        <select name="previousstate" id="previousstate" class="form-control" required >
            <option value="">State...</option>
                <option value="ABIA">Abia</option>
                <option value="ABUJA">Abuja</option>
                <option value="CROSS RIVER">Cross River</option>
                <option value="DELTA">Delta</option>
                <option value="LAGOS">Lagos</option>
                <option value="OGUN">Ogun</option>
                <option value="OYO">Oyo</option>
            </select>
        </div>
        <div class="form-group col-sm-6">
          <input name="previouszip" id="previouszip" class="form-control" placeholder="Zip/Postal code"/>
        </div>
    </div>    
    <div class="form-group">
      <label for="timeatpreviousaddress">Time at Previous Address</label>
      <select name="timeatpreviousaddress" id="timeatpreviousaddress" class="form-control" required>
      <option value="">Select...</option>
      <option value="Less Than 1 Year">Less Than 1 Year</option>
      <option value="1 Year">1 Year</option>
      <option value="2 Years">2 Years</option>
      <option value="3 Years">3 Years</option>
      <option value="4 Years">4 Years</option>
      <option value="5 Years">5 Years</option>
      <option value="More Than 5 Years">More Than 5 Years</option>
      </select>
      
    </div>
    <div class="form-group">
      <label for="residentialstatus">Residential Status</label>
      <select name="residentialstatus" id="residentialstatus" class="form-control" required >
      <option value="">Select...</option>
      <option value="Temp. Residence">Temporary Residence</option>
      <option value="Family Owned">Family House</option>
      <option value="Rented">Rented</option>
      <option value="Own Residence">Owned</option>
      <option value="Employer Provided">Provided by Employer</option>
      </select>
    </div>
    <div class="form-group">
      <label for="maritalstatus">Marital Status</label>
      <select name="maritalstatus" id="maritalstatus" class="form-control" required >
      <option value="">Select...</option>
      <option value="Single">Single</option>
      <option value="Married">Married</option>
      <option value="Separated">Separated</option>
      <option value="Divorced">Divorced</option>
      <option value="Widowed">Widowed</option>
      </select>
    </div>
    <div class="form-group">
      <label for="dependents">Dependants</label>
      <select name="dependents" id="dependents" class="form-control" required >
      <option value="">Select...</option>
      <option value="0">0</option>
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
      <option value="6">6</option>
      <option value="7">7</option>
      <option value="8">8</option>
      <option value="9">9</option>
      <option value="10">10</option>
      <option value="More than 10">More than 10</option>
      </select>
    </div>
    <div class="form-group">
      <label for="household">Number of People in Household</label>
      <input type="number" name="household" id="household" class="form-control" placeholder="Number of People in Household" required />
    </div>
    <div class="form-group">
      <label for="monthlyexpense">Average Monthly Household Expenses</label>
      <input type="text" name="monthlyexpense" id="monthlyexpense" class="form-control" placeholder="Average Monthly Household Expenses" required />
    </div>
    <p class="text-muted">Document Upload</p>
          <hr />
    <br>
    <!-- The fileinput-button span is used to style the file input field as button -->
    <span class="btn btn-success fileinput-button">
        <i class="glyphicon glyphicon-plus"></i>
        <span>Add Documents and Click Upload...</span>
        <!-- The file input field used as target for the file upload widget -->
        <input id="fileupload" type="file" name="files[]" multiple>
    </span>
    <br>
          <p class="text-warning">
            Accepted Formats: Images(Jpeg,Gif,Png), Documents(Doc,Docx,Pdf)
          <ol>
<li>Valid identification i.e. driver’s license, international passport or national ID</li>
<li>One recent passport photograph</li>
<li>Bank statements showing 6 (most recent) salary credits</li>
<li>Employment letter and/or promotion letter, confirmation letter or salary review letter(s)</li>
<li>Monthly repayment cheques dated in line with your pay day</li>
<li>Security cheque covering loan amount</li>
<li>Pay slips are compulsory for government workers</li>
</ol>
          </p>
    <br>
    <!-- The global progress bar -->
    <div id="progress" class="progress progress-striped active">
        <div class="progress-bar progress-bar-success"></div>
    </div>
    <!-- The container for the uploaded files -->
    <div id="files" class="files"></div>
    <br>
          <input type="hidden" id="documents" name="documents" value="" />
    </fieldset>
	    </div>
	    <div class="tab-pane" id="tab2">
        <p class="text-info"></p>
	          <fieldset name="Income Details" title="Income Details">
    <div class="form-group">
      <label for="monthlyincome">Net Monthly Income</label>
      <div id="slider3"></div>
      <br />
      <input type="text" name="monthlyincome" id="monthlyincome" value="<?php echo @$_POST['pay'] ?>" class="form-control input3" placeholder="Net Monthly Income" required />
    </div>
    <div class="form-group">
      <label for="otherincome">Other Income</label>
      <input type="text" name="otherincome" id="otherincome" class="form-control" placeholder="Other Income" />
    </div>
    <div class="form-group">
      <label for="payday" >Next Payment Date</label>
      <input type="text" name="payday" id="payday" class="form-control" placeholder="Next Payment Date" required />
    </div>
    </fieldset>
	    </div>
		<div class="tab-pane" id="tab3">
        <p class="text-info"></p>
			<fieldset title="Your Existing Loans" name="Your Existing Loans">
   <div class="row">        
   <div class="form-group col-md-3">
      <label for="lender">Name of Lender</label>
      <input type="text" name="lender" id="lender" class="form-control" placeholder="Name of Lender" />
    </div>
    
    <div class="form-group col-md-3">
      <label for="loanamount0">Loan Amount</label>
      <input type="text" name="loanamount0" id="loanamount0" class="form-control" placeholder="Loan Amount" />
    </div>
    
    <div class="form-group col-md-3">
      <label for="loanbalance">Loan Balance</label>
      <input type="text" name="loanbalance" id="loanbalance" class="form-control" placeholder="Loan Balance" />
    </div>
    
    <div class="form-group col-md-3">
      <label for="monthlyrepayments0">Monthly Repayments</label>
      <input type="text" name="monthlyrepayments0" id="monthlyrepayments0" class="form-control" placeholder="Monthly Repayments" />
    </div>
    </div>
    
    <div class="row">        
   <div class="form-group col-md-3">
      <label for="lender2">Name of Lender</label>
      <input type="text" name="lender2" id="lender2" class="form-control" placeholder="Name of Lender" />
    </div>
    
    <div class="form-group col-md-3">
      <label for="loanamount2">Loan Amount</label>
      <input type="text" name="loanamount2" id="loanamount2" class="form-control" placeholder="Loan Amount" />
    </div>
    
    <div class="form-group col-md-3">
      <label for="loanbalance2">Loan Balance</label>
      <input type="text" name="loanbalance2" id="loanbalance2" class="form-control" placeholder="Loan Balance" />
    </div>
    
    <div class="form-group col-md-3">
      <label for="monthlyrepayments2">Monthly Repayments</label>
      <input type="text" name="monthlyrepayments2" id="monthlyrepayments2" class="form-control" placeholder="Monthly Repayments" />
    </div>
    </div>
    
    <div class="row">        
   <div class="form-group col-md-3">
      <label for="lender3">Name of Lender</label>
      <input type="text" name="lender3" id="lender3" class="form-control" placeholder="Name of Lender" />
    </div>
    
    <div class="form-group col-md-3">
      <label for="loanamount3">Loan Amount</label>
      <input type="text" name="loanamount3" id="loanamount3" class="form-control" placeholder="Loan Amount" />
    </div>
    
    <div class="form-group col-md-3">
      <label for="loanbalance3">Loan Balance</label>
      <input type="text" name="loanbalance3" id="loanbalance3" class="form-control" placeholder="Loan Balance" />
    </div>
    
    <div class="form-group col-md-3">
      <label for="monthlyrepayments3">Monthly Repayments</label>
      <input type="text" name="monthlyrepayments3" id="monthlyrepayments3" class="form-control" placeholder="Monthly Repayments" />
    </div>
    </div>
   <!-- <div class="form-group">
      <label for="loansinstatement">Loans Reflected in Your Statement</label>
      <input type="text" name="loansinstatement" id="loansinstatement" class="form-control" placeholder="Loans Reflected in Your Statement" required />
    </div>
    <div class="form-group">
      <label for="otherloans">Other Loans not Reflected</label>
      <input type="text" name="otherloans" id="otherloans" class="form-control" placeholder="Other Loans not Reflected" />
    </div>
    <div class="form-group">
      <label for="monthlyrepayments">Monthly Repayments</label>
      <input type="text" name="monthlyrepayments" id="monthlyrepayments" class="form-control" placeholder="Monthly Repayments" required />
    </div>-->
    </fieldset>
	    </div>
		<div class="tab-pane" id="tab4">
        <p class="text-info"></p>
    <fieldset title="Employment Details" name="Employment Details">
    <div class="form-group">
      <label for="jobtitle">Job Title/Position</label>
      <input type="text" name="jobtitle" id="jobtitle" class="form-control" placeholder="Job Title/Position" required />
    </div>
    <div class="form-group">
      <label for="employmenttype">Employment Type</label>
      <select name="employmenttype" id="employmenttype" class="form-control" required >
      <option value="">Select...</option>
      <option value="Permanent">Permanent Employment</option>
      <option value="Contract">Contract/Temporary</option>
      <option value="Self-Employed">Self-Employed</option>
      <option value="Unemployed">Unemployed</option>
      </select>
    </div>
    <div class="form-group">
      <label for="currentemployer">Current Employer</label>
      <input type="text" name="currentemployer" id="currentemployer" class="form-control" placeholder="Current Employer" required />
    </div>
    <div class="form-group">
      <label for="employmentdate">Employment Date</label>
      <input type="text" name="employmentdate" id="employmentdate" class="form-control" placeholder="Employment Date" required />
    </div>
    <div class="form-group">
      <label for="staffid">Staff ID Number</label>
      <input type="text" name="staffid" id="staffid" class="form-control" placeholder="Staff ID Number" required />
    </div>
    <label for="employersaddress">Current Employer's Address</label>
    <div name="employersaddress" class="row">
        <div class="form-group col-sm-12">
          <input name="employersaddressline1" id="employersaddressline1" class="form-control" placeholder="Address Line 1" required/>
        </div>
        <div class="form-group col-sm-12">
          <input name="employersaddressline2" id="employersaddressline2" class="form-control" placeholder="Address Line 2"/>
        </div>
        <div class="form-group col-sm-6">
          <input name="employerscity" id="employerscity" class="form-control" placeholder="City/Area" required/>
        </div>
        <div class="form-group col-sm-6">
        <select name="employersstate" id="employersstate" class="form-control" required >
            <option value="">State...</option>
                <option value="ABIA">Abia</option>
                <option value="ABUJA">Abuja</option>
                <option value="CROSS RIVER">Cross River</option>
                <option value="DELTA">Delta</option>
                <option value="LAGOS">Lagos</option>
                <option value="OGUN">Ogun</option>
                <option value="OYO">Oyo</option>
            </select>
        </div>
        <div class="form-group col-sm-6">
          <input name="employerszip" id="employerszip" class="form-control" placeholder="Zip/Postal code"/>
        </div>
    </div>
    <div class="form-group">
      <label for="employerstelephone">Employer's Telephone (HR)</label>
      <input type="text" name="employerstelephone" id="employerstelephone" class="form-control" placeholder="Employer's Telephone (HR)" required />
    </div>
    <div class="form-group">
      <label for="staffsemail">Official Email Address</label>
      <input type="text" name="staffsemail" id="staffsemail" class="form-control" placeholder="Staff's Official Email Address" required />
    </div>
    <div class="form-group">
      <label for="industry">Industry/Sector</label>
      <input type="text" name="industry" id="industry" class="form-control" placeholder="Industry/Sector" required />
    </div>
    <div class="form-group">
      <label for="personalemail">Personal Email Address</label>
      <input type="text" name="personalemail" id="personalemail" class="form-control" placeholder="Personal Email Address" required />
    </div>
    <div class="form-group">
      <label for="monthsinpreviousemployment">Number of Months in Previous Employment</label>
      <input type="text" name="monthsinpreviousemployment" id="monthsinpreviousemployment" class="form-control" placeholder="Number of Months in Previous Employment" required />
    </div>
    <div class="form-group">
      <label for="employersinlastfiveyears">Number of Employers in Last 5 Years</label>
      <select  name="employersinlastfiveyears" id="employersinlastfiveyears" class="form-control" required >
      <option value="">Select...</option>
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
      </select>
    </div>
    </fieldset>
	    </div>
		<div class="tab-pane" id="tab5">
        <p class="text-info"></p>
		 <fieldset name="Level of Education" title="Level of Education">
    
    
    <div class="form-group">
      <label for="levelofeducation">Level Of Education</label>
      <select  name="levelofeducation" id="levelofeducation" class="form-control" required >
      <option value="">Select...</option>
      <option value="Primary">Primary</option>
      <option value="Secondary">Secondary</option>
      <option value="Graduate">Graduate</option>
      <option value="Post-Graduate">Post-Graduate</option>
      <option value="Doctorate">Doctorate</option>
      </select>
      
    </div>
    <div class="form-group">
      <label for="lastinstitution">Last School Attended</label>
      <input type="text" name="lastinstitution" id="lastinstitution" class="form-control" placeholder="Last Educational Institution Attended" required />
    </div>
    </fieldset>
	    </div>
		<div class="tab-pane" id="tab6">
        <p class="text-info"></p>
			   <fieldset name="Next of Kin/Blood Relative" title="Next of Kin/Blood Relative">
    <div class="form-group">
      <label for="kinname">Next of Kin Name</label>
      <input type="text" name="kinname" id="kinname" class="form-control" placeholder="Next of Kin Name" required />
    </div>
    <div class="form-group">
      <label for="kinrelationship">Next of Kin Relationship</label>
      <select  name="kinrelationship" id="kinrelationship" class="form-control"  required >
      <option value="">Select...</option>
      <option value="Father">Father</option>
      <option value="Mother">Mother</option>
      <option value="Husband">Husband</option>
      <option value="Wife">Wife</option>
      <option value="Son">Son</option>
      <option value="Daughter">Daughter</option>
      <option value="Brother">Brother</option>
      <option value="Sister">Sister</option>
      <option value="Uncle">Uncle</option>
      <option value="Aunt">Aunt</option>
      <option value="Cousin">Cousin</option>
      <option value="Other">Other</option>
      </select>
    </div>
    <label for="kinhomeaddress">Next of Kin Home Address</label>
    <div name="kinhomeaddress" class="row">
        <div class="form-group col-sm-12">
          <input name="kinaddressline1" id="kinaddressline1" class="form-control" placeholder="Address Line 1" required/>
        </div>
        <div class="form-group col-sm-12">
          <input name="kinaddressline2" id="kinaddressline2" class="form-control" placeholder="Address Line 2"/>
        </div>
        <div class="form-group col-sm-6">
          <input name="kincity" id="kincity" class="form-control" placeholder="City/Area" required/>
        </div>
        <div class="form-group col-sm-6">
        <select name="kinstate" id="kinstate" class="form-control" required >
            <option value="">State...</option>
                <option value="ABIA">Abia</option>
                <option value="ABUJA">Abuja</option>
                <option value="CROSS RIVER">Cross River</option>
                <option value="DELTA">Delta</option>
                <option value="LAGOS">Lagos</option>
                <option value="OGUN">Ogun</option>
                <option value="OYO">Oyo</option>
            </select>
        </div>
        <div class="form-group col-sm-6">
          <input name="kinzip" id="kinzip" class="form-control" placeholder="Zip/Postal code"/>
        </div>
    </div>
    <div class="form-group">
      <label for="kinmobilenumber">Next of Kin Mobile Number</label>
      <input type="text" name="kinmobilenumber" id="kinmobilenumber" class="form-control" placeholder="Next of Kin Mobile Number" required />
    </div>
    <div class="form-group">
      <label for="kinoccupation">Next of Kin Occupation</label>
      <input type="text" name="kinoccupation" id="kinoccupation" class="form-control" placeholder="Next of Kin Occupation" required />
    </div>
    <div class="form-group">
      <label for="kinemployer">Next of Kin Employer</label>
      <input type="text" name="kinemployer" id="kinemployer" class="form-control" placeholder="Next of Kin Employer" required />
    </div>
    <div class="form-group">
      <label for="kinemail">Next of Kin Email Address</label>
      <input type="text" name="kinemail" id="kinemail" class="form-control" placeholder="Next of Kin Email Address" required />
    </div>
    </fieldset>
    
	    </div>
		<div class="tab-pane" id="tab7">
        <p class="text-info"></p>
			    <fieldset title="Bank/Disbursement Details" name="Bank/Disbursement Details">
    
    <div class="form-group">
      <label for="bankname">Bank Name</label>
      <select  name="bankname" id="bankname" class="form-control"  required >
      <option value="">Select...</option>
      <option value="Access Bank">Access Bank</option>
      <option value="Diamond Bank">Diamond Bank</option>
      <option value="EcoBank">EcoBank</option>
      <option value="Enterprise Bank">Enterprise Bank</option>
      <option value="FCMB">FCMB</option>
      <option value="Fidelity Bank">Fidelity Bank</option>
      <option value="First Bank">First Bank</option>
      <option value="GT Bank">GT Bank</option>
      <option value="Heritage Bank">Heritage Bank</option>
      <option value="Keystone Bank">Keystone Bank</option>
      <option value="Mainstreet Bank">Mainstreet Bank</option>
      <option value="One Credit">One Credit</option>
      <option value="Skye Bank">Skye Bank</option>
      <option value="Stanbic IBTC">Stanbic IBTC</option>
      <option value="Standard Chartered">Standard Chartered</option>
      <option value="Sterling Bank">Sterling Bank</option>
      <option value="UBA">UBA</option>
      <option value="Union Bank">Union Bank</option>
      <option value="Union Assurance CO LTD">Union Assurance CO LTD</option>
      <option value="Unity Bank">Unity Bank</option>
      <option value="Wema Bank">Wema Bank</option>
      <option value="CITI Bank">CITI Bank</option>
      <option value="JAIZ">JAIZ</option>
      <option value="ASO Savings">ASO Savings</option>
      </select>
    </div>
    <div class="form-group">
      <label for="accountnumber">Account Number</label>
      <input type="text" name="accountnumber" id="accountnumber" class="form-control" placeholder="Account Number" required />
    </div>
    <div class="form-group">
      <label for="accountname">Account Name</label>
      <input type="text" name="accountname" id="accountname" class="form-control" placeholder="Account Name" required />
    </div>
    <div class="form-group">
      <label for="accounttype">Account Type</label>
      <select  name="accounttype" id="accounttype" class="form-control"  required >
      <option value="">Select...</option>
      <option value="Savings Account">Savings Account</option>
      <option value="Current Account">Current Account</option>
      </select>
     
    </div>
    <div class="form-group">
      <label for="bankaddress">Bank Branch/Address</label>
      <input type="text" name="bankaddress" id="bankaddress" class="form-control" placeholder="Bank Branch/Address" required />
    </div>
    </fieldset>
	    </div>
        <div class="tab-pane" id="tab8">
        <p class="text-info"></p>
        <fieldset name="Loan Details" title="Loan Details">
    <div class="form-group">
      <label for="loanamount">Loan Amount Requested</label>
      <div id="slider1"></div>
      <br />
      <input type="text" name="loanamount" id="loanamount" value="<?php echo @$_POST['credit'] ?>"  class="form-control input1" placeholder="Loan Amount Requested" required />
    </div>
    <div class="form-group">
      <label for="loantenor">Loan Tenor (months)</label>
      <div id="slider2"></div>
      <br />
      <input type="text" name="loantenor" id="loantenor" class="form-control input2" value="<?php echo @$_POST['duration'] ?>" placeholder="Loan Tenor (months)" required />
    <p class="text-info">Can only be 3, 6, 9 or 12 months </p>
    </div>
    <div class="form-group">
      <label for="installment">Monthly Installment</label>
      <input type="text" name="installment" id="installments" class="form-control" value="<?php echo @$_POST['installments'] ?>" placeholder="Monthly Installment" required />
    </div>
    <div class="form-group">
      <label for="loanpurpose">Purpose of Loan</label>
      <input type="text" name="loanpurpose" id="loanpurpose" class="form-control" placeholder="Purpose of Loan" required />
    </div>
    <div class="checkbox">
      <label>
        <input type="checkbox" id="agree" name="agree" required /> I accept the <a href="#" data-toggle="modal" data-target="#myModal">Terms and Conditions</a> and <a href="#" data-toggle="modal" data-target="#myUnderModal">Declaration and Undertaking</a>
      </label>
    </div>
    </fieldset>
	    </div>
		<ul class="pager wizard calculatorform">
			<li class="previous"><a class="bluer" href="javascript:;">Previous</a></li>
		  	<li class="next"><a class="bluer" href="javascript:;">Next</a></li>
			<li class="next finish" style="display:none;"><button type="submit" class="btn btn-primary pull-right">Submit</button></li>
		</ul>
	</div>	
</div>
<!--
<input type="hidden" name="loanamount" value="<?php echo @$_POST['credit'] ?>" />
<input type="hidden" name="installment" value="<?php echo @$_POST['installments'] ?>" />
<input type="hidden" name="monthlyincome" value="<?php echo @$_POST['pay'] ?>" />
-->
<?php if(isset($_GET['referrer'])){ ?>
    <input type="hidden" name="referrer" value="<?php echo @$_GET['referrer'] ?>" />
<?php } ?>
</form>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Terms and Conditions</h4>
      </div>
      <div class="modal-body">
        <?php wp_reset_query(); ?>
        <?php query_posts('category_name=terms&showposts=1'); ?>
        <?php while (have_posts()) : the_post(); ?>
        <?php the_content(); ?>
        <?php endwhile;?>             
                     
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal -->
<div class="modal fade" id="myUnderModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Declaration and Undertaking</h4>
      </div>
      <div class="modal-body">
        <?php wp_reset_query(); ?>
        <?php query_posts('category_name=undertaking&showposts=1'); ?>
        <?php while (have_posts()) : the_post(); ?>
        <?php the_content(); ?>
        <?php endwhile;?>             
                     
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


 
 

 </div>
 </div>
 </section>
 
 <?php get_footer(); ?>