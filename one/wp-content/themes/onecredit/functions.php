<?php
/**
 * @author Olanipekun Olufemi <iolufemi@ymail.com>
 * @copyright 2014
 * */
 
 // drag and drop menu support
register_nav_menu( 'primary', 'Primary Menu' );

//widget support for a right sidebar
register_sidebar(array(
  'name' => 'Right SideBar',
  'id' => 'right-sidebar',
  'description' => 'Widgets in this area will be shown on the right-hand side.',
  'before_widget' => '<div id="%1$s">',
  'after_widget'  => '</div>',  
  'before_title' => '<h4>',
  'after_title' => '</h4>'
));


//widget support for the footer1
register_sidebar(array(
  'name' => 'Social Icon    ',
  'id' => 'footer-sidebar1',
  'description' => 'Widgets in this area will be shown in the footer.',
  'before_widget' => '<div id="%1$s">',
  'after_widget'  => '</div>',
  'before_title' => '<h4>',
  'after_title' => '</h4>'
));


//widget support for the footer2
register_sidebar(array(
  'name' => 'Subscription Box',
  'id' => 'footer-sidebar2',
  'description' => 'Widgets in this area will be shown in the footer.',
  'before_widget' => '<div id="%1$s">',
  'after_widget'  => '</div>',
  'before_title' => '<h4>',
  'after_title' => '</h4>'
));


//This theme uses post thumbnails
add_theme_support( 'post-thumbnails' );

//Apply do_shortcode() to widgets so that shortcodes will be executed in widgets
add_filter('widget_text', 'do_shortcode');


/**
 * function my_admin_bar_remove
 * Remove the WordPress menu from the admin bar.
 *
 * This will remove the WordPress logo from the admin bar; you
 * may wish to add your own alternative menu in its place.
 *
 * @since 1.5
 */
function my_admin_bar_remove() {
	global $wp_admin_bar;

	$wp_admin_bar->remove_node('wp-logo');
}
add_action('wp_before_admin_bar_render', 'my_admin_bar_remove');

/**
 * function my_admin_footer
 * Rewrite the text in the bottom-left footer area
 *
 * @since 1.0
 */
function my_admin_footer() {
	echo 'Designed and Developed by <a href="http://www.kvpafrica.com" target="_blank">KVP Africa IT Department</a>';
}
add_filter('admin_footer_text', 'my_admin_footer');

// Replaces the excerpt "more" text by a link
function new_excerpt_more($more) {
       global $post;
	return '<div><a class="btn btn-primary" href="'. get_permalink($post->ID) . '">Read More <span class="glyphicon glyphicon-chevron-right"></span></a></div>';
}
add_filter('excerpt_more', 'new_excerpt_more');

// Register Custom Navigation Walker
require_once('wp_bootstrap_navwalker.php');
 ?>