<?php 
/*
Template Name: Page With Sidebar
*/
 ?>
<?php get_header(); ?>
    <section>
    <div class="container">
    <div class="row">
    <div class="col-md-8">
    <?php if(have_posts()):?>
    <?php while(have_posts()): the_post();?>
    <div class="page-header">
      <h1 class="blue"><?php the_title(); ?></h1>
    </div>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php the_content(); ?>
    </article>
    <?php endwhile; ?>
    <?php else: ?>
    <p class="lead">Nothing to display.</p>
    <?php endif;?>
    </div>
    <?php get_sidebar(); ?>
    </div>
    
    </div>
    </section>
<?php get_footer(); ?>