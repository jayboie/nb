<?php
/*
Template Name: Facebook Page*/
 ?>
<?php get_header(); ?>
<section id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<div class="container">
<div id="social" class="jumbotron">
  <div id ="login" class="col-sm-6">
    <span id="textt"><h2>Link your facebook account with One Credit</h3></span>
        <p id="notify"></p>
        <form class="form-horizontal" role="form">
            <div class="form-group">
                <input type="text" class="form-control" id="clientid" placeholder="Enter your Client ID">
            </div>
        </form>
        <button id="login-button" href="#" class="btn btn-info">Connect</button>
    </div>
    <div id="in">
        <h2>You have successfully linked your account with facebook</h2>
    </div>
</div>
</div>
</section>
<?php get_footer(); ?>