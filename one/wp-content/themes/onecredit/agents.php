<?php
/*
Template Name: Agents Page
*/
get_header();
?>
<section id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <div class="container">
    <h1 class="heading">Verify a <span class="orange">One</span> <span class="blue">Credit</span> Agent</h1>
    <div class="input-group">
  <span class="input-group-addon">Filter:</span>
  <input type="search" id="agentfil" class="form-control" placeholder="Enter Firstname or Lastname..">
</div>
    <button id="loading" class="btn btn-primary btn-lg btn-block"><i class="icon icon-refresh icon-spin"></i> Loading...</button></div>
<div class="container" id="agents">
<ul id="da-thumbs" class="da-thumbs">
</ul>
</div>
</section>
<?php get_footer() ?>