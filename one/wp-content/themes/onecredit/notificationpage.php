<?php
/*
Template Name: Notification Page*/
 ?>
<?php get_header(); ?>
<section id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<div class="container">
<div class="jumbotron">
    <?php if(have_posts()):?>
    <?php while(have_posts()): the_post();?>
  <h1><?php the_title(); ?></h1>
  <p><?php the_content(); ?></p>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&appId=1424955714443959&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="fb-like" data-href="https://www.facebook.com/OneCredit" data-layout="button_count" data-action="recommend" data-show-faces="true" data-share="true"></div>
    <?php endwhile; ?>
    <?php else: ?>
    <p class="lead">Nothing to display.</p>
    <?php endif;?>
</div>
</div>
</section>
<?php get_footer(); ?>