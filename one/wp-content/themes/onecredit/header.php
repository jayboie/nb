<!DOCTYPE html>

<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->

<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->

<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->

<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->

  <head>

    <link href='http://fonts.googleapis.com/css?family=Roboto|Roboto+Condensed:400,700' rel='stylesheet' type='text/css' />

    <title><?php wp_title(''); ?></title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <meta charset="<?php bloginfo( 'charset' ); ?>" />

    

    <meta name="author" content="Olanipekun Olufemi, KVP Africa" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1" />

    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/jquery-ui.css" />

    <link href="<?php echo get_template_directory_uri(); ?>/js/datepicker/lib/themes/default.css" rel="stylesheet" type="text/css" />

    <link href="<?php echo get_template_directory_uri(); ?>/js/datepicker/lib/themes/default.date.css" rel="stylesheet" type="text/css" />

    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet" />
    
    <noscript><link href="<?php echo get_template_directory_uri(); ?>/css/noJS.css" rel="stylesheet" /></noscript>
    

    <link href="<?php echo get_template_directory_uri(); ?>/css/layout.css" rel="stylesheet" />
    <link href="<?php echo get_template_directory_uri(); ?>/css/main2.css" rel="stylesheet" />

    <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/jquery.fileupload.css">
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>

      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

    <![endif]-->


     <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" />

     <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon.png" />

     <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon-72x72.png" />

     <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon-114x114.png" />

     <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

     

     <?php wp_head(); ?>  

     

  </head>

  <body ng-app='app' <?php body_class(); ?>>
  <!-- Start Alexa Certify Javascript -->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"9yj3j1a4ZP00wD", domain:"one-cred.com",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=9yj3j1a4ZP00wD" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript -->

    <header data-spy="affix" data-offset-top="220"  data-offset-bottom="0" >

    <div class="container">

    <div class="col-md-3 col-sm-4 col-xs-6">

    <a href="<?php echo home_url(); //make logo a home link?>">

    <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" width="196" height="117" class="logo" id="logo" /></a>

    </div>

    <nav class="col-md-9 col-sm-8 col-xs-6 navbar navbar-default" role="navigation">

    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">

      <span class="sr-only">Menu</span>

      <span class="icon-bar icon-white"></span>

      <span class="icon-bar icon-white"></span>

      <span class="icon-bar icon-white"></span>

    </button>

    

    <?php wp_nav_menu( array( 'menu' => 'primary','depth' => 2,'theme_location' => 'primary', 'container' => 'div', 'container_class'=> 'collapse navbar-collapse navbar-ex1-collapse', 'menu_class' => 'nav nav-pills pull-right','fallback_cb' => 'wp_bootstrap_navwalker::fallback', 'walker' => new wp_bootstrap_navwalker())  ); ?>



    </nav>

    </div>

    </header>