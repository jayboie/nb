/*
@Author Olanipekun Olufemi <iolufemi@ymail.com>
@copyright 2014
*/


                
        
      $("#slider1").slider({
        range: "min",
        value: 40000,
        step: 10000,
        min: 40000,
        max: 500000,
        slide: function( event, ui ) {
        
        var principal = ui.value;
        var tenor = $('input.input2').val();
        var salary = $('input.input3').val();
        var repayment = getRepayment(principal,tenor);
        var ddays = repayment / tenor;
        var loan = getloanable(salary);
        if(ui.value > loan || !isTrue(ddays,salary,tenor,principal)){
            $('p.notify').html('Amount selected too low for a loan of this tenor');
            $('.calculatorform .btn').attr("disabled", true);
            $('p.notify').addClass('warn');
            //return false;
            $( ".input1" ).val(ui.value);
            calculateLoan(principal,tenor,salary);

        }else{
         $('.calculatorform .btn').attr("disabled", false);
        $( ".input1" ).val(ui.value);
        $('p.notify').removeClass('warn');
            
        calculateLoan(principal,tenor,salary);
        var message = getMaxLoan(salary);
        $('p.notify').html(message);
        }
    }
    });
    
    
    $(".input1").change(function () {
        var value = this.value;
        var principal = $('input.input1').val();
        var tenor = $('input.input2').val();
        var salary = $('input.input3').val();
        var repayment = getRepayment(principal,tenor);
        var ddays = repayment / tenor;
        var loan = getloanable(salary);
        if((principal > loan) || (!isTrue(ddays,salary,tenor,principal)) || (principal < 40000 || principal > 500000) || (tenor == 4 || tenor == 1 || tenor == 2 || tenor == 5 || tenor == 7 || tenor == 8 ||/* tenor == 9 ||*/ tenor == 10 || tenor == 11) || (salary < 47000)){
            $('p.notify').html('Amount selected too low for a loan of this tenor');
            $('.calculatorform .btn').attr("disabled", true);
            $('p.notify').addClass('warn');
            //return false;
            $("#slider1").slider("value", parseInt(value));
            calculateLoan(principal,tenor,salary);
        }else{
            $('.calculatorform .btn').attr("disabled", false);
            $('p.notify').removeClass('warn');
        //console.log(value);
        $("#slider1").slider("value", parseInt(value));

        
        calculateLoan(principal,tenor,salary);
        var message = getMaxLoan(salary);
        $('p.notify').html(message);
        }
    });
    
    $("#slider2").slider({
        range: "min",
        value: 3,
        step: 3,
        min: 3,
        max: 12,
        slide: function( event, ui ) {
        var principal = $('input.input1').val();
        var tenor = ui.value;
        var salary = $('input.input3').val();
        var mont = getmonths(salary);
            if((!gmonth(principal,tenor,salary)) || (ui.value > mont) || (ui.value == 4 || ui.value == 1 || ui.value == 2 || ui.value == 5 || ui.value == 7 || ui.value == 8 || /*ui.value == 9 || */ui.value == 10 || ui.value == 11)){
            $('p.notify').html('Amount selected too low for a loan of this tenor');
            $('.calculatorform .btn').attr("disabled", true);
            $('p.notify').addClass('warn');
            //return false;
            $( ".input2" ).val(ui.value);
            calculateLoan(principal,tenor,salary);
        }else{
            $('.calculatorform .btn').attr("disabled", false);
            $('p.notify').removeClass('warn');
            $( ".input2" ).val(ui.value);
            
        
        
        calculateLoan(principal,tenor,salary);
        var message = getMaxLoan(salary);
        $('p.notify').html(message);
        }
        }
    });
    
    
    $(".input2").change(function () {
        var value = this.value;
        var principal = $('input.input1').val();
        var tenor = $('input.input2').val();
        var salary = $('input.input3').val();
        var mont = getmonths(salary);
        if((!gmonth(principal,tenor,salary)) || (tenor > mont) || (principal < 40000 || principal > 500000) || (tenor == 4 || tenor == 1 || tenor == 2 || tenor == 5 || tenor == 7 || tenor == 8 || /*tenor == 9 || */tenor == 10 || tenor == 11) || (salary < 47000)){
            $('p.notify').html('Amount selected too low for a loan of this tenor');
            $('.calculatorform .btn').attr("disabled", true);
            $('p.notify').addClass('warn');
            //return false;
            $("#slider2").slider("value", parseInt(value));
            calculateLoan(principal,tenor,salary);
        }else{
        $('.calculatorform .btn').attr("disabled", false);
        $('p.notify').removeClass('warn');
        //console.log(value);
        $("#slider2").slider("value", parseInt(value));
        
        calculateLoan(principal,tenor,salary);
        var message = getMaxLoan(salary);
        $('p.notify').html(message);
        }
    });
    
    
    $("#slider3").slider({
        range: "min",
        value: 47000,
        step: 1000,
        min: 47000,
        max: 1000000,
        slide: function( event, ui ) {
            $( ".input3" ).val(ui.value);
            
        var principal = $('input.input1').val();
        var tenor = $('input.input2').val();
        var salary = ui.value;
        calculateLoan(principal,tenor,salary);
        
        var message = getMaxLoan(salary);
        $("#slider1").slider("value", getloanable(salary));
        $("#slider2").slider("value", getmonths(salary));
        $( ".input1" ).val(getloanable(salary));
        $( ".input2" ).val(getmonths(salary));
        $('p.notify').html(message);
        
        
        
        }
    });
    
    
    $(".input3").change(function () {
        var value = this.value;
        var salary = parseInt(value);
        var principal = getloanable(salary);
        var tenor = getmonths(salary);
        
        if((principal < 40000 || principal > 500000) || (tenor == 4 || tenor == 1 || tenor == 2 || tenor == 5 || tenor == 7 || tenor == 8 || /*tenor == 9 || */tenor == 10 || tenor == 11) || (salary < 47000)){
            $('p.notify').html('Amount selected too low for a loan of this tenor');
            $('.calculatorform .btn').attr("disabled", true);
            $('p.notify').addClass('warn');
            //return false;
            $("#slider3").slider("value", parseInt(value));
            calculateLoan(principal,tenor,salary);
        }else{
        //console.log(value);
        $('.calculatorform .btn').attr("disabled", false);
        $('p.notify').removeClass('warn');
        $("#slider3").slider("value", parseInt(value));
        
        
        calculateLoan(principal,tenor,salary);
        
        var message = getMaxLoan(salary);
        $("#slider1").slider("value", getloanable(salary));
        $("#slider2").slider("value", getmonths(salary));
        $( ".input1" ).val(getloanable(salary));
        $( ".input2" ).val(getmonths(salary));
        $('p.notify').html(message);
        }
    });
    
    
        var principal = $('input.input1').val();
        var tenor = $('input.input2').val();
        var salary = $('input.input3').val();
        
        calculateLoan(principal,tenor,salary);
    
    function addCommas(nStr)
    {
    	nStr += '';
    	x = nStr.split('.');
    	x1 = x[0];
    	x2 = x.length > 1 ? '.' + x[1] : '';
    	var rgx = /(\d+)(\d{3})/;
    	while (rgx.test(x1)) {
    		x1 = x1.replace(rgx, '$1' + ',' + '$2');
    	}
    	return x1 + x2;
    }
    
    function getRepayment(principal,tenor){
        var per = 0.0575 * tenor;
        var addit = 1 + per;
        var repayment = principal * addit;
        return repayment;
    }
    
    function isTrue(repayment,salary,tenor,principal){
        var repay = repayment * 2.5;
        if((principal < 100000 ) && (tenor > 6)){
            return false;
        }else if((repay <= salary)){
            return true;
        }else{
           return false; 
        }
    }
    
    function getMaxLoan(salary){
        if(salary < 74000){
            var tenor = 9;
            var principal = getPrincipal(salary,tenor);
        }else{
            var tenor = 12;  
            var principal = getPrincipal(salary,tenor);
            if(principal < 100000){
                var tenor = 9;
                var principal = getPrincipal(salary,tenor);
            }
        }
        
        if(principal > 500000){
            var principal = 500000;
        }
        
        
        return "With net pay of <strong>&#8358 " + addCommas(salary) + "</strong>, the maximum loan amount you can get is <strong>&#8358; " + addCommas(((principal/10000).toFixed(0))*10000) + "</strong> for <strong>" + tenor + " Months</strong>."
        
    }
    
    function getloanable(salary){
        if(salary < 74000){
            var tenor = 9;
            var principal = getPrincipal(salary,tenor);
        }else{
            var tenor = 12;  
            var principal = getPrincipal(salary,tenor);
            if(principal < 100000){
                var tenor = 9;
                var principal = getPrincipal(salary,tenor);
            }
        }
        if(principal > 500000){
            var principal = 500000;
        }
        
        return /*principal.toFixed(0)*/((principal/10000).toFixed(0))*10000;
    }
    
    function getmonths(salary){
        if(salary < 74000){
            var tenor = 9;
            var principal = getPrincipal(salary,tenor);
        }else{
            var tenor = 12;  
            var principal = getPrincipal(salary,tenor);
            if(principal < 100000){
                var tenor = 9;
                var principal = getPrincipal(salary,tenor);
            }
        }
        
        if(principal > 500000){
            var principal = 500000;
        }
        
        return tenor;
    }
    
    function getPrincipal(salary,tenor){
        var per = 0.0575 * tenor;
        var addit  = 1 + per;
        var mult = 2.5 * addit;
        var st = salary * tenor;
        var principal = st / mult;
        return principal;
    }
    
    function gmonth(principal,tenor,salary){
        var repayment = getRepayment(principal,tenor);
        var ddays = repayment / tenor;
        var canrepay = ddays * 2.5;
        if(canrepay > salary){
            return false;
        }else{
            return true;
        }
    }
    
    function calculateLoan(principal,tenor,salary){
        
        var repayment = getRepayment(principal,tenor);
        var ddays = repayment / tenor;
        var cistrue = isTrue(ddays,salary,tenor,principal);
        
        if(!cistrue){

        }else{

        }
        
        $('strong.repay').html('&#8358; ' + addCommas(repayment.toFixed(2)));
        $('strong.monthly').html('&#8358; ' + addCommas(ddays.toFixed(2)));
        $('strong.days').html(tenor + ' Months');
        
        $("input#credit").val(principal);
        $("input#duration").val(tenor);
        $("input#pay").val(salary);

        $("input#installments").val(addCommas(ddays.toFixed(2)) /*+ " for " + tenor + " Month(s)"*/);

    }
    
    

      
