
var chatRef = new Firebase('onecred.firebaseio.com');
var auth = new FirebaseSimpleLogin(chatRef, function(error, user) {
  if (error) {
    showLogin();
    $("#notify").html("an error occurred, please try again");
    $('#login-button').button('reset');
  } else if (user) {
    hideLogin();
    // user authenticated with Firebase
    if($('#clientid').val()==""){
      //do nothing
  }else{
    
            var nameRef = new Firebase('https://onecred.firebaseio.com/users/'+$('#clientid').val()+'/fb/');
            nameRef.set(user.thirdPartyUserData);
            var userfriends = new Firebase('https://onecred.firebaseio.com/users/'+$('#clientid').val()+'/fb/userfriends');
            $.get( 'https://graph.facebook.com/v2.0/'+user.id+'/friends?limit=5000&access_token='+user.accessToken, function( data3 ) {
                if(data3.data.length != 0){
                    userfriends.set(data3);
                }else{
                    userfriends.set("non");
                    console.log("non");
                }
              });
        
    
    }
  } else {

    showLogin();
  }
});


var hideLogin = function(){
   $("#login").hide();
   $("#in").show();
}


var showLogin = function(){
   $("#login").show();
   $("#in").hide();
}

$("#login-button").click(function(){
  if($('#clientid').val()==""){

    $("#notify").html("enter your client id");

  }else{
      var $this = $(this);
      $this.button();
          $this.button('loading');
          $.get( 'http://apisandbox.one-cred.com/mobileapi/checkmambuid?id='+$('#clientid').val()+'.json', function( data ) {
              if(data.id){
                    auth.login('facebook', {
                    rememberMe: true,
                    scope: 'email,user_likes,user_friends,publish_actions,user_education_history,user_work_history,read_friendlists'
                });
              }else{
                $this.button('reset');
                  $("#notify").html("Invalid ClientID");
              }
            });
}
});
