<?php $this->load->module('admintemplate'); ?>
<?php $this->load->module('users'); ?>
<?php $this->load->module('referrers'); ?>
<link href="<?php echo $this->template->get_asset(); ?>/js/datepicker/lib/themes/default.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $this->template->get_asset(); ?>/js/datepicker/lib/themes/default.date.css" rel="stylesheet" type="text/css" />
<div>
				<ul class="breadcrumb">
					<li>
						<a href="#">Home</a> <span class="divider">/</span>
					</li>
					<li>
						<a href="#">Users</a>
					</li>
				</ul>
			</div>
            <div class="row-fluid sortable">
                </div> 
			<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-user"></i> Users</h2>
					</div>
					<div class="box-content">
                    
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
                                    <th>#</th>
								  <th>First Name</th>
								  <th>Last Name</th>
                                  <th>Email</th>
                                  <th>Referrer
                                    </th>
                                  <th>&nbsp;
                                    </th>
							  </tr>
						  </thead>   
						  <tbody>
<?php 
if(isset($c)){
    $c = $c;
}else{
    $c = 1;
}

$usercount = count($users->result());
if($usercount > 0):
foreach($users->result() as $user){
 ?>
							<tr>
                                <td><?php echo $c; ?></td>
								
								<td class="center"><?php echo $user->firstname; ?></td>
								<td class="center"><?php echo $user->surname; ?></td>
                                <td class="center"><?php echo $user->email; ?></td>
                                <td class="center"><?php echo $this->referrers->getReferrerName($user->referrer); ?></td>
                                <td class="center">
                                
									<a class="btn btn-success" href="<?php echo base_url('users/generatetc') ?>/<?php echo $user->id; ?>" target="_blank" title="Download User Form">
										<i class="icon-download icon-white"></i>                                           
									</a>
									<a class="btn btn-info" href="#" data-target="#EditUser<?php echo $user->id; ?>" data-toggle="modal" title="Edit User">
										<i class="icon-edit icon-white"></i>                                         
									</a>
									<a class="btn btn-danger" href="<?php echo base_url('users/deleteuser') ?>/<?php echo $user->id; ?>" title="Delete User">
										<i class="icon-trash icon-white"></i>
									</a>
  <?php 
  if(empty($user->documents)){ ?>
                                  <br />
                                  <br />
                <span class="label label-warning">No Document Uploaded</span>                     
  <?php
  }else{
    $documents = $user->documents;
  $splitdocuments = explode("+++",$documents);
  $cd = 0; ?>
                                  <br />
                                  <br />
                                  <?php 
  foreach($splitdocuments as $links){ 
                                  $cd++;
                                  ?>
                                  <a href="<?php echo $links; ?>" target="_blank"><span class="label label-success">Document <?php echo $cd; ?></span></a>                      
  <?php 
  }
}
                                  ?>               
                                  
								</td>
							</tr>
<?php $c++;
 }
else: ?>
<tr>
<td colspan="5">
<div class="alert">
I could not find any user on the database. Try Adding One.
</div>
</td>
</tr>
<?php endif
 ?>
							
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
            <div class="pagination pagination-center"><?php if(isset($pagenavi)){ echo $pagenavi; } ?></div>
      
<?php 
foreach($users->result() as $user){
 ?>
 
<!-- Edit User -->
<div id="EditUser<?php echo $user->id; ?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="EditUser<?php echo $user->id; ?>Label" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3 id="EditUser<?php echo $user->id; ?>Label">Edit User</h3>
  </div>
<div class="modal-body">
          
          <form role="form" action="<?php echo base_url('users/updateuser'); ?>" method="post" id="edituser">
          <input type="hidden" name="id" value="<?php echo $user->id; ?>" />
          <div class="tb-content">
	    <div class="tb-pane" id="tab1">
        <p class="text-info"></p>
	      <fieldset name="Personal Data" title="Personal Data" >
   <div class="form-group">
      <label for="firstname">First Name</label>
      <input type="text" name="firstname" id="firstname" value="<?php echo $user->firstname; ?>" class="form-control" placeholder="First Name" required />
    </div>
   <div class="form-group">
      <label for="middlename">Middle Name</label>
      <input type="text" name="middlename" id="middlename" value="<?php echo $user->middlename; ?>" class="form-control" placeholder="Middle Name" required />
    </div>
    <div class="form-group">
      <label for="surname">Surname</label>
      <input type="text" name="surname" id="surname" value="<?php echo $user->surname; ?>" class="form-control" placeholder="Surname" required />
    </div>
    <div class="form-group">
      <label for="maidenname">Other/Maiden Name</label>
      <input type="text" name="maidenname" id="maidenname" value="<?php echo $user->maidenname; ?>" class="form-control" placeholder="Other/Maiden Name" required />
    </div>
    <div class="form-group">
      <label for="dateofbirth">Date of Birth</label>
      <input type="text" name="dateofbirth" id="dateofbirth" value="<?php echo $user->dateofbirth; ?>" class="form-control" placeholder="Date of Birth" required />
    </div>
    <div class="form-group">
      <label for="mobilenumber">Mobile Number</label>
      <input type="text" name="mobilenumber" id="mobilenumber" value="<?php echo $user->mobilenumber; ?>" class="form-control" placeholder="Mobile Number" required />
    </div>
    <div class="form-group">
      <label for="gender">Gender</label>
      <select name="gender" id="gender" class="form-control" required>
      <option <?php if($user->gender == "Male"){ echo 'selected = "yes"'; } ?> value="Male">Male</option>
      <option <?php if($user->gender == "Female"){ echo 'selected = "yes"'; } ?> value="Female">Female</option>
      </select>
    </div>
    <div class="form-group">
      <label for="email">Email Address</label>
      <input type="text" name="email" value="<?php echo $user->email; ?>" id="email" class="form-control" placeholder="email" required />
    </div>
    <div class="form-group">
      <label for="homenumber">Home/Landline Number</label>
      <input type="text" name="homenumber" id="homenumber" value="<?php echo $user->homenumber; ?>" class="form-control" placeholder="Home/Landline Number" required />
    </div>
    <div class="form-group">
      <label for="meansofidentification">Means of Identification</label>
      <select name="meansofidentification" id="meansofidentification" class="form-control" required>
      <option <?php if($user->meansofidentification == "Passport"){ echo 'selected = "yes"'; } ?>  value="Passport">Passport</option>
      <option <?php if($user->meansofidentification == "Driver's License"){ echo 'selected = "yes"'; } ?>  value="Driver's License">Driver's License</option>
      <option <?php if($user->meansofidentification == "National ID Card"){ echo 'selected = "yes"'; } ?> value="National ID Card">National ID Card</option>
      </select>
      
    </div>
    <div class="form-group">
      <label for="otheridentification">Other Means of Identification</label>
      <input type="text" value="<?php echo $user->otheridentification; ?>" name="otheridentification" id="otheridentification" class="form-control" placeholder="Other Means of Identification" />
    </div>
    <div class="form-group">
      <label for="homeaddress">Current Home Address</label>
      <textarea name="homeaddress" id="homeaddress" class="form-control" placeholder="Current Home Address" required><?php echo $user->homeaddress; ?></textarea>
      
    </div>
    <div class="form-group">
      <label for="nearestlandmark">Nearest Landmark</label>
      <input type="text" name="nearestlandmark" id="nearestlandmark" value="<?php echo $user->nearestlandmark; ?>" class="form-control" placeholder="Nearest Landmark" required />
    </div>
    <div class="form-group">
      <label for="nearestbusstop">Nearest Bus Stop</label>
      <input type="text" name="nearestbusstop" id="nearestbusstop" value="<?php echo $user->nearestbusstop; ?>" class="form-control" placeholder="Nearest Bus Stop" required />
    </div>
    <div class="form-group">
      <label for="timeatcurrentaddress">Time at Current Address</label>
      <select name="timeatcurrentaddress" id="timeatcurrentaddress" class="form-control" required>
      <option <?php if($user->timeatcurrentaddress == "Less Than 1 Year"){ echo 'selected = "yes"'; } ?>  value="Less Than 1 Year">Less Than 1 Year</option>
      <option <?php if($user->timeatcurrentaddress == "1 Year"){ echo 'selected = "yes"'; } ?> value="1 Year">1 Year</option>
      <option <?php if($user->timeatcurrentaddress == "2 Years"){ echo 'selected = "yes"'; } ?> value="2 Years">2 Years</option>
      <option <?php if($user->timeatcurrentaddress == "3 Years"){ echo 'selected = "yes"'; } ?> value="3 Years">3 Years</option>
      <option <?php if($user->timeatcurrentaddress == "4 Years"){ echo 'selected = "yes"'; } ?> value="4 Years">4 Years</option>
      <option <?php if($user->timeatcurrentaddress == "5 Years"){ echo 'selected = "yes"'; } ?> value="5 Years">5 Years</option>
      <option <?php if($user->timeatcurrentaddress == "More Than 5 Years"){ echo 'selected = "yes"'; } ?> value="More Than 5 Years">More Than 5 Years</option>
      </select>
      
    </div>
    <div class="form-group">
      <label for="previousaddress">Previous Address</label>
      <textarea name="previousaddress" id="previousaddress" class="form-control" placeholder="Previous Address" required><?php echo $user->previousaddress; ?></textarea>
    </div>
    <div class="form-group">
      <label for="timeatpreviousaddress">Time at Previous Address</label>
      <select name="timeatpreviousaddress" id="timeatpreviousaddress" class="form-control" required>
      <option <?php if($user->timeatpreviousaddress == "Less Than 1 Year"){ echo 'selected = "yes"'; } ?> value="Less Than 1 Year">Less Than 1 Year</option>
      <option <?php if($user->timeatpreviousaddress == "1 Year"){ echo 'selected = "yes"'; } ?> value="1 Year">1 Year</option>
      <option <?php if($user->timeatpreviousaddress == "2 Years"){ echo 'selected = "yes"'; } ?> value="2 Years">2 Years</option>
      <option <?php if($user->timeatpreviousaddress == "3 Years"){ echo 'selected = "yes"'; } ?> value="3 Years">3 Years</option>
      <option <?php if($user->timeatpreviousaddress == "4 Years"){ echo 'selected = "yes"'; } ?> value="4 Years">4 Years</option>
      <option <?php if($user->timeatpreviousaddress == "5 Years"){ echo 'selected = "yes"'; } ?> value="5 Years">5 Years</option>
      <option <?php if($user->timeatpreviousaddress == "More Than 5 Years"){ echo 'selected = "yes"'; } ?> value="More Than 5 Years">More Than 5 Years</option>
      </select>
      
    </div>
    <div class="form-group">
      <label for="residentialstatus">Residential Status</label>
      <select name="residentialstatus" id="residentialstatus" class="form-control" required >
      <option <?php if($user->residentialstatus == "Temporary Residence"){ echo 'selected = "yes"'; } ?> value="Temporary Residence">Temporary Residence</option>
      <option <?php if($user->residentialstatus == "Family House"){ echo 'selected = "yes"'; } ?> value="Family House">Family House</option>
      <option <?php if($user->residentialstatus == "Rented"){ echo 'selected = "yes"'; } ?> value="Rented">Rented</option>
      <option <?php if($user->residentialstatus == "Owned"){ echo 'selected = "yes"'; } ?> value="Owned">Owned</option>
      <option <?php if($user->residentialstatus == "Provided by Employer"){ echo 'selected = "yes"'; } ?> value="Provided by Employer">Provided by Employer</option>
      </select>
    </div>
    <div class="form-group">
      <label for="maritalstatus">Marital Status</label>
      <select name="maritalstatus" id="maritalstatus" class="form-control" required >
      <option <?php if($user->maritalstatus == "Single"){ echo 'selected = "yes"'; } ?> value="Single">Single</option>
      <option <?php if($user->maritalstatus == "Married"){ echo 'selected = "yes"'; } ?> value="Married">Married</option>
      <option <?php if($user->maritalstatus == "Separated"){ echo 'selected = "yes"'; } ?> value="Separated">Separated</option>
      <option <?php if($user->maritalstatus == "Divorced"){ echo 'selected = "yes"'; } ?> value="Divorced">Divorced</option>
      <option <?php if($user->maritalstatus == "Widowed"){ echo 'selected = "yes"'; } ?> value="Widowed">Widowed</option>
      </select>
    </div>
    <div class="form-group">
      <label for="dependents">Dependants</label>
      <select name="dependents" id="dependents" class="form-control" required >
      <option <?php if($user->dependents == "0"){ echo 'selected = "yes"'; } ?> value="0">0</option>
      <option <?php if($user->dependents == "1"){ echo 'selected = "yes"'; } ?> value="1">1</option>
      <option <?php if($user->dependents == "2"){ echo 'selected = "yes"'; } ?> value="2">2</option>
      <option <?php if($user->dependents == "3"){ echo 'selected = "yes"'; } ?> value="3">3</option>
      <option <?php if($user->dependents == "4"){ echo 'selected = "yes"'; } ?> value="4">4</option>
      <option <?php if($user->dependents == "5"){ echo 'selected = "yes"'; } ?> value="5">5</option>
      <option <?php if($user->dependents == "6"){ echo 'selected = "yes"'; } ?> value="6">6</option>
      <option <?php if($user->dependents == "7"){ echo 'selected = "yes"'; } ?> value="7">7</option>
      <option <?php if($user->dependents == "8"){ echo 'selected = "yes"'; } ?> value="8">8</option>
      <option <?php if($user->dependents == "9"){ echo 'selected = "yes"'; } ?> value="9">9</option>
      <option <?php if($user->dependents == "10"){ echo 'selected = "yes"'; } ?> value="10">10</option>
      <option <?php if($user->dependents == "More than 10"){ echo 'selected = "yes"'; } ?> value="More than 10">More than 10</option>
      </select>
    </div>
    <div class="form-group">
      <label for="monthlyexpense">Average Monthly Household Expenses</label>
      <input type="text" name="monthlyexpense" id="monthlyexpense" value="<?php echo $user->monthlyexpense; ?>" class="form-control" placeholder="Average Monthly Household Expenses" required />
    </div>
    </fieldset>
	    </div>
	    <div class="tb-pane" id="tab2">
        <p class="text-info"></p>
	          <fieldset name="Income Details" title="Income Details">
    <div class="form-group">
      <label for="monthlyincome">Net Monthly Income</label>
      <input type="text" name="monthlyincome" value="<?php echo $user->monthlyincome; ?>" id="monthlyincome" class="form-control" placeholder="Net Monthly Income" required />
    </div>
    <div class="form-group">
      <label for="otherincome">Other Income</label>
      <input type="text" name="otherincome" id="otherincome" value="<?php echo $user->otherincome; ?>" class="form-control" placeholder="Other Income" />
    </div>
    <div class="form-group">
      <label for="payday">Pay Day</label>
      <select name="payday" class="form-control" id="payday">
      <?php 
      $day_ = array('1st','2nd','3rd','4th','5th','6th','7th','8th','9th','10th','11th','12th','13th','14th','15th','16th','17th','18th','19th','20th','21st','22nd','23rd','24th','25th','26th','27th','28th','29th','30th','31th');
       foreach($day_ as $payday_){ ?>
        <option value="<?php echo $payday_; ?>" <?php if($user->payday == $payday_){ echo 'selected = "yes"'; } ?> ><?php echo $payday_; ?></option>
       <?php }
       ?>
      </select>
      
    </div>
    </fieldset>
	    </div>
		<div class="tb-pane" id="tab3">
        <p class="text-info"></p>
			<fieldset title="Your Existing Loans" name="Your Existing Loans">
            
            <div class="">        
   <div class="form-group notmd-3">
      <label for="lender">Name of Lender</label>
      <input type="text" name="lender" id="lender" value="<?php echo $user->lender; ?>" class="form-control" placeholder="Name of Lender" />
    </div>
    
    <div class="form-group notmd-3">
      <label for="loanamount0">Loan Amount</label>
      <input type="text" name="loanamount0" id="loanamount0" value="<?php echo $user->loanamount0; ?>" class="form-control" placeholder="Loan Amount" />
    </div>
    
    <div class="form-group notmd-3">
      <label for="loanbalance">Loan Balance</label>
      <input type="text" name="loanbalance" id="loanbalance" value="<?php echo $user->loanbalance; ?>" class="form-control" placeholder="Loan Balance" />
    </div>
    
    <div class="form-group notmd-3">
      <label for="monthlyrepayments0">Monthly Repayments</label>
      <input type="text" name="monthlyrepayments0" id="monthlyrepayments0" value="<?php echo $user->monthlyrepayments0; ?>" class="form-control" placeholder="Monthly Repayments" />
    </div>
    </div>
    
    <div class="">        
   <div class="form-group notmd-3">
      <label for="lender2">Name of Lender</label>
      <input type="text" name="lender2" id="lender2" class="form-control" value="<?php echo $user->lender2; ?>" placeholder="Name of Lender" />
    </div>
    
    <div class="form-group notmd-3">
      <label for="loanamount2">Loan Amount</label>
      <input type="text" name="loanamount2" id="loanamount2" value="<?php echo $user->loanamount2; ?>" class="form-control" placeholder="Loan Amount" />
    </div>
    
    <div class="form-group notmd-3">
      <label for="loanbalance2">Loan Balance</label>
      <input type="text" name="loanbalance2" id="loanbalance2" value="<?php echo $user->loanbalance2; ?>" class="form-control" placeholder="Loan Balance" />
    </div>
    
    <div class="form-group notmd-3">
      <label for="monthlyrepayments2">Monthly Repayments</label>
      <input type="text" name="monthlyrepayments2" id="monthlyrepayments2" value="<?php echo $user->monthlyrepayments2; ?>" class="form-control" placeholder="Monthly Repayments" />
    </div>
    </div>
    
    <div class="">        
   <div class="form-group notmd-3">
      <label for="lender3">Name of Lender</label>
      <input type="text" name="lender3" id="lender3" class="form-control" value="<?php echo $user->lender3; ?>" placeholder="Name of Lender" />
    </div>
    
    <div class="form-group notmd-3">
      <label for="loanamount3">Loan Amount</label>
      <input type="text" name="loanamount3" id="loanamount3" value="<?php echo $user->loanamount3; ?>" class="form-control" placeholder="Loan Amount" />
    </div>
    
    <div class="form-group notmd-3">
      <label for="loanbalance3">Loan Balance</label>
      <input type="text" name="loanbalance3" id="loanbalance3" value="<?php echo $user->loanbalance3; ?>" class="form-control" placeholder="Loan Balance" />
    </div>
    
    <div class="form-group notmd-3">
      <label for="monthlyrepayments3">Monthly Repayments</label>
      <input type="text" name="monthlyrepayments3" id="monthlyrepayments3" value="<?php echo $user->monthlyrepayments3; ?>" class="form-control" placeholder="Monthly Repayments" />
    </div>
    </div>
    
    <!--<div class="form-group">
      <label for="loansinstatement">Loans Reflected in Your Statement</label>
      <input type="text" name="loansinstatement" value="<?php echo $user->loansinstatement; ?>" id="loansinstatement" class="form-control" placeholder="Loans Reflected in Your Statement" required />
    </div>
    <div class="form-group">
      <label for="otherloans">Other Loans not Reflected</label>
      <input type="text" name="otherloans" id="otherloans" value="<?php echo $user->otherloans; ?>" class="form-control" placeholder="Other Loans not Reflected" />
    </div>
    <div class="form-group">
      <label for="monthlyrepayments">Monthly Repayments</label>
      <input type="text" name="monthlyrepayments" id="monthlyrepayments" value="<?php echo $user->monthlyrepayments; ?>" class="form-control" placeholder="Monthly Repayments" required />
    </div>-->
    </fieldset>
	    </div>
		<div class="tb-pane" id="tab4">
        <p class="text-info"></p>
			    <fieldset title="Employment Details" name="Employment Details">
    <div class="form-group">
      <label for="employmenttype">Employment Type</label>
      <select name="employmenttype" id="employmenttype" class="form-control" required >
      <option <?php if($user->employmenttype == "Permanent Employment"){ echo 'selected = "yes"'; } ?> value="Permanent Employment">Permanent Employment</option>
      <option <?php if($user->employmenttype == "Contract/Temporary"){ echo 'selected = "yes"'; } ?> value="Contract/Temporary">Contract/Temporary</option>
      <option <?php if($user->employmenttype == "Self-Employed"){ echo 'selected = "yes"'; } ?> value="Self-Employed">Self-Employed</option>
      <option <?php if($user->employmenttype == "Unemployed"){ echo 'selected = "yes"'; } ?> value="Unemployed">Unemployed</option>
      </select>
    </div>
    <div class="form-group">
      <label for="currentemployer">Current Employer</label>
      <input type="text" name="currentemployer" id="currentemployer" value="<?php echo $user->currentemployer; ?>" class="form-control" placeholder="Current Employer" required />
    </div>
    <div class="form-group">
      <label for="employmentdate">Employment Date</label>
      <input type="text" name="employmentdate" value="<?php echo $user->employmentdate; ?>" id="employmentdate" class="form-control" placeholder="Employment Date" required />
    </div>
    <div class="form-group">
      <label for="staffid">Staff ID Number</label>
      <input type="text" name="staffid" id="staffid" value="<?php echo $user->staffid; ?>" class="form-control" placeholder="Staff ID Number" required />
    </div>
    <div class="form-group">
      <label for="employersaddress">Current Employer's Address</label>
      <textarea  name="employersaddress" id="employersaddress" class="form-control" placeholder="Current Employer's Address" required><?php echo $user->employersaddress; ?></textarea>
      
    </div>
    <div class="form-group">
      <label for="employerstelephone">Employer's Telephone (HR)</label>
      <input type="text" name="employerstelephone" id="employerstelephone" value="<?php echo $user->employerstelephone; ?>" class="form-control" placeholder="Employer's Telephone (HR)" required />
    </div>
    <div class="form-group">
      <label for="staffsemail">Official Email Address</label>
      <input type="text" name="staffsemail" id="staffsemail" value="<?php echo $user->staffsemail; ?>" class="form-control" placeholder="Staff's Official Email Address" required />
    </div>
    <div class="form-group">
      <label for="industry">Industry/Sector</label>
      <input type="text" name="industry" id="industry" value="<?php echo $user->industry; ?>" class="form-control" placeholder="Industry/Sector" required />
    </div>
    <div class="form-group">
      <label for="personalemail">Personal Email Address</label>
      <input type="text" name="personalemail" id="personalemail" value="<?php echo $user->personalemail; ?>" class="form-control" placeholder="Personal Email Address" required />
    </div>
    <div class="form-group">
      <label for="monthsinpreviousemployment">Number of Months in Previous Employment</label>
      <input type="text" name="monthsinpreviousemployment" value="<?php echo $user->monthsinpreviousemployment; ?>" id="monthsinpreviousemployment" class="form-control" placeholder="Number of Months in Previous Employment" required />
    </div>
    <div class="form-group">
      <label for="employersinlastfiveyears">Number of Employers in Last 5 Years</label>
      <input type="text" name="employersinlastfiveyears" value="<?php echo $user->employersinlastfiveyears; ?>" id="employersinlastfiveyears" class="form-control" placeholder="Number of Employers in Last 5 Years" required />
    </div>
    </fieldset>
	    </div>
		<div class="tb-pane" id="tab5">
        <p class="text-info"></p>
		 <fieldset name="Level of Education" title="Level of Education">
    
    
    <div class="form-group">
      <label for="levelofeducation">Level Of Education</label>
      <select  name="levelofeducation" id="levelofeducation" class="form-control" required >
      <option <?php if($user->levelofeducation == "Secondary"){ echo 'selected = "yes"'; } ?> value="Secondary">Secondary</option>
      <option <?php if($user->levelofeducation == "Graduate"){ echo 'selected = "yes"'; } ?> value="Graduate">Graduate</option>
      <option <?php if($user->levelofeducation == "Post-Graduate"){ echo 'selected = "yes"'; } ?> value="Post-Graduate">Post-Graduate</option>
      <option <?php if($user->levelofeducation == "Doctorate"){ echo 'selected = "yes"'; } ?> value="Doctorate">Doctorate</option>
      </select>
      
    </div>
    <div class="form-group">
      <label for="lastinstitution">Last School Attended</label>
      <input type="text" name="lastinstitution" id="lastinstitution" value="<?php echo $user->lastinstitution; ?>" class="form-control" placeholder="Last Educational Institution Attended" required />
    </div>
    </fieldset>
	    </div>
		<div class="tb-pane" id="tab6">
        <p class="text-info"></p>
			   <fieldset name="Next of Kin" title="Next of Kin">
    <div class="form-group">
      <label for="kinname">Next of Kin Name</label>
      <input type="text" name="kinname" id="kinname" value="<?php echo $user->kinname; ?>" class="form-control" placeholder="Next of Kin Name" required />
    </div>
    <div class="form-group">
      <label for="kinrelationship">Next of Kin Relationship</label>
      <input type="text" name="kinrelationship" id="kinrelationship" value="<?php echo $user->kinrelationship; ?>" class="form-control" placeholder="Next of Kin Relationship" required />
    </div>
    <div class="form-group">
      <label for="kinhomeaddress">Next of Kin Home Address</label>
      <textarea name="kinhomeaddress" id="kinhomeaddress" class="form-control" placeholder="Next of Kin Home Address" required><?php echo $user->kinhomeaddress; ?></textarea>
    </div>
    <div class="form-group">
      <label for="kinmobilenumber">Next of Kin Mobile Number</label>
      <input type="text" name="kinmobilenumber" id="kinmobilenumber" value="<?php echo $user->kinmobilenumber; ?>" class="form-control" placeholder="Next of Kin Mobile Number" required />
    </div>
    <div class="form-group">
      <label for="kinoccupation">Next of Kin Occupation</label>
      <input type="text" name="kinoccupation" id="kinoccupation" value="<?php echo $user->kinoccupation; ?>" class="form-control" placeholder="Next of Kin Occupation" required />
    </div>
    <div class="form-group">
      <label for="kinemployer">Next of Kin Employer</label>
      <input type="text" name="kinemployer" id="kinemployer" value="<?php echo $user->kinemployer; ?>" class="form-control" placeholder="Next of Kin Employer" required />
    </div>
    <div class="form-group">
      <label for="kinemail">Next of Kin Emaill Address</label>
      <input type="text" name="kinemail" id="kinemail" value="<?php echo $user->kinemail; ?>" class="form-control" placeholder="Next of Kin Emaill Address" required />
    </div>
    </fieldset>
    
	    </div>
		<div class="tb-pane" id="tab7">
        <p class="text-info"></p>
			    <fieldset title="Bank/Disbursement Details" name="Bank/Disbursement Details">
    
    <div class="form-group">
      <label for="bankname">Bank Name</label>
      <input type="text" name="bankname" id="bankname" value="<?php echo $user->bankname; ?>" class="form-control" placeholder="Bank Name" required />
    </div>
    <div class="form-group">
      <label for="accountnumber">Account Number</label>
      <input type="text" name="accountnumber" id="accountnumber" value="<?php echo $user->accountnumber; ?>" class="form-control" placeholder="Account Number" required />
    </div>
    <div class="form-group">
      <label for="accountname">Account Name</label>
      <input type="text" name="accountname" id="accountname" value="<?php echo $user->accountname; ?>" class="form-control" placeholder="Account Name" required />
    </div>
    <div class="form-group">
      <label for="accounttype">Account Type</label>
      <select  name="accounttype" id="accounttype" class="form-control"  required >
      <option <?php if($user->accounttype == "Savings Account"){ echo 'selected = "yes"'; } ?> value="Savings Account">Savings Account</option>
      <option <?php if($user->accounttype == "Current Account"){ echo 'selected = "yes"'; } ?>  value="Current Account">Current Account</option>
      </select>
     
    </div>
    <div class="form-group">
      <label for="banksortcode">Bank Sort Code</label>
      <input type="text" name="banksortcode" id="banksortcode" value="<?php echo $user->banksortcode; ?>" class="form-control" placeholder="Bank Sort Code" required />
    </div>
    <div class="form-group">
      <label for="bankaddress">Bank Branch/Address</label>
      <input type="text" name="bankaddress" id="bankaddress" value="<?php echo $user->bankaddress; ?>" class="form-control" placeholder="Bank Branch/Address" required />
    </div>
    </fieldset>
	    </div>
        <div class="tb-pane" id="tab8">
        <p class="text-info"></p>
        <fieldset name="Loan Details" title="Loan Details">
    <div class="form-group">
      <label for="loanamount">Loan Amount Requested</label>
      <input type="text" name="loanamount" id="loanamount" value="<?php echo $user->loanamount; ?>"  class="form-control" placeholder="Loan Amount Requested" required />
    </div>
    <div class="form-group">
      <label for="installment">Monthly Installment</label>
      <input type="text" name="installment" id="installment" value="<?php echo $user->installment; ?>" class="form-control" placeholder="Monthly Installment" required />
    </div>
    <div class="form-group">
      <label for="loanpurpose">Purpose of Loan</label>
      <input type="text" name="loanpurpose" id="loanpurpose" value="<?php echo $user->loanpurpose; ?>" class="form-control" placeholder="Purpose of Loan" required />
    </div>
    </fieldset>
	    </div>
	</div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Update User</button></form>
            </div>
</div>

<?php } ?>




      

    

    <script>
$(document).ready(function(){

$('.btn-danger').click(function(e){
    e.preventDefault();
    $confirm = confirm('Are you sure?');
    if(!$confirm){
        return false;
    }else{
        window.location.href = $(this).attr('href');
    }
});
  
}

);
</script> 
<script src="<?php echo $this->template->get_asset(); ?>/js/jquery.printElement.js"></script>
<script src="<?php echo $this->template->get_asset(); ?>/js/datepicker/lib/picker.js"></script>
<script src="<?php echo $this->template->get_asset(); ?>/js/datepicker/lib/picker.date.js"></script>
<script src="<?php echo $this->template->get_asset(); ?>/js/datepicker/lib/legacy.js"></script>