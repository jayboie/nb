<?php $this->load->module('template'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo "$firstname $surname" ?></title>
<link href="<?php echo $this->template->get_asset(); ?>/css/bootstrap.css" rel="stylesheet" />
<style>
input,select {
    padding: 0px !important;
    height: auto !important;
    margin: auto !important;
}
h1 {
    margin: 0px !important;
}
</style>
</head>
 
<body>
<div class="container">
<div style="text-align: center;"><img src="<?php echo $this->template->get_asset(); ?>/images/logo.png" /></div>
<h1 class="page-header" style="text-align: center;">One Credit Online Loan Application Form</h1>
 <form role="form">

          <div class="tb-content">
	    <div class="tb-pane" id="tab1">
        <p class="text-muted"> Personal Data</p>
	      <fieldset name="Personal Data" title="Personal Data" >
          
   <div class="row">
   <div class="form-group col-xs-4">
      <label for="firstname">First Name</label>
      <input type="text" name="firstname" id="firstname" value="<?php echo $firstname; ?>" class="form-control" placeholder="First Name" required />
    </div>
   <div class="form-group  col-xs-4">
      <label for="middlename">Middle Name</label>
      <input type="text" name="middlename" id="middlename" value="<?php echo $middlename; ?>" class="form-control" placeholder="Middle Name" required />
    </div>
    <div class="form-group  col-xs-4">
      <label for="surname">Surname</label>
      <input type="text" name="surname" id="surname" value="<?php echo $surname; ?>" class="form-control" placeholder="Surname" required />
    </div>
    </div>
    <div class="row">
    <div class="form-group col-xs-4">
      <label for="maidenname">Other/Maiden Name</label>
      <input type="text" name="maidenname" id="maidenname" value="<?php echo $maidenname; ?>" class="form-control" placeholder="Other/Maiden Name" required />
    </div>
    <div class="form-group col-xs-4">
      <label for="dateofbirth">Date of Birth</label>
      <input type="text" name="dateofbirth" id="dateofbirth" value="<?php echo $dateofbirth; ?>" class="form-control" placeholder="Date of Birth" required />
    </div>
    <div class="form-group col-xs-4">
      <label for="mobilenumber">Mobile Number</label>
      <input type="text" name="mobilenumber" id="mobilenumber" value="<?php echo $mobilenumber; ?>" class="form-control" placeholder="Mobile Number" required />
    </div>
    </div>
    <div class="row">
    <div class="form-group  col-xs-4">
      <label for="gender">Gender</label>
      <select name="gender" id="gender" class="form-control" required>
      <option <?php if($gender == "Male"){ echo 'selected = "yes"'; } ?> value="Male">Male</option>
      <option <?php if($gender == "Female"){ echo 'selected = "yes"'; } ?> value="Female">Female</option>
      </select>
    </div>
    <div class="form-group  col-xs-8">
      <label for="email">Email Address</label>
      <input type="text" name="email" value="<?php echo $email; ?>" id="email" class="form-control" placeholder="email" required />
    </div>
    </div>
    <div class="row">
    <div class="form-group col-xs-4">
      <label for="homenumber">Home/Landline Number</label>
      <input type="text" name="homenumber" id="homenumber" value="<?php echo $homenumber; ?>" class="form-control" placeholder="Home/Landline Number" required />
    </div>
    <div class="form-group col-xs-8">
      <label for="meansofidentification">Means of Identification</label>
      <select name="meansofidentification" id="meansofidentification" class="form-control" required>
      <option <?php if($meansofidentification == "Passport"){ echo 'selected = "yes"'; } ?>  value="Passport">Passport</option>
      <option <?php if($meansofidentification == "Driver's License"){ echo 'selected = "yes"'; } ?>  value="Driver's License">Driver's License</option>
      <option <?php if($meansofidentification == "National ID Card"){ echo 'selected = "yes"'; } ?> value="National ID Card">National ID Card</option>
      </select>
      
    </div>
    </div>
    <div class="form-group">
      <label for="otheridentification">Other Means of Identification</label>
      <input type="text" value="<?php echo $otheridentification; ?>" name="otheridentification" id="otheridentification" class="form-control" placeholder="Other Means of Identification" />
    </div>
    <div class="form-group">
      <label for="homeaddress">Current Home Address</label>
      <textarea name="homeaddress" id="homeaddress" class="form-control" placeholder="Current Home Address" required><?php echo $homeaddress; ?></textarea>
      
    </div>
    <div class="form-group">
      <label for="nearestlandmark">Nearest Landmark</label>
      <input type="text" name="nearestlandmark" id="nearestlandmark" value="<?php echo $nearestlandmark; ?>" class="form-control" placeholder="Nearest Landmark" required />
    </div>
    <div class="form-group">
      <label for="nearestbusstop">Nearest Bus Stop</label>
      <input type="text" name="nearestbusstop" id="nearestbusstop" value="<?php echo $nearestbusstop; ?>" class="form-control" placeholder="Nearest Bus Stop" required />
    </div>
    <div class="form-group">
      <label for="timeatcurrentaddress">Time at Current Address</label>
      <select name="timeatcurrentaddress" id="timeatcurrentaddress" class="form-control" required>
      <option <?php if($timeatcurrentaddress == "Less Than 1 Year"){ echo 'selected = "yes"'; } ?>  value="Less Than 1 Year">Less Than 1 Year</option>
      <option <?php if($timeatcurrentaddress == "1 Year"){ echo 'selected = "yes"'; } ?> value="1 Year">1 Year</option>
      <option <?php if($timeatcurrentaddress == "2 Years"){ echo 'selected = "yes"'; } ?> value="2 Years">2 Years</option>
      <option <?php if($timeatcurrentaddress == "3 Years"){ echo 'selected = "yes"'; } ?> value="3 Years">3 Years</option>
      <option <?php if($timeatcurrentaddress == "4 Years"){ echo 'selected = "yes"'; } ?> value="4 Years">4 Years</option>
      <option <?php if($timeatcurrentaddress == "5 Years"){ echo 'selected = "yes"'; } ?> value="5 Years">5 Years</option>
      <option <?php if($timeatcurrentaddress == "More Than 5 Years"){ echo 'selected = "yes"'; } ?> value="More Than 5 Years">More Than 5 Years</option>
      </select>
      
    </div>
    <div class="form-group">
      <label for="previousaddress">Previous Address</label>
      <textarea name="previousaddress" id="previousaddress" class="form-control" placeholder="Previous Address" required><?php echo $previousaddress; ?></textarea>
    </div>
    <div class="row">
    <div class="form-group col-xs-4">
      <label for="timeatpreviousaddress">Time at Previous Address</label>
      <select name="timeatpreviousaddress" id="timeatpreviousaddress" class="form-control" required>
      <option <?php if($timeatpreviousaddress == "Less Than 1 Year"){ echo 'selected = "yes"'; } ?> value="Less Than 1 Year">Less Than 1 Year</option>
      <option <?php if($timeatpreviousaddress == "1 Year"){ echo 'selected = "yes"'; } ?> value="1 Year">1 Year</option>
      <option <?php if($timeatpreviousaddress == "2 Years"){ echo 'selected = "yes"'; } ?> value="2 Years">2 Years</option>
      <option <?php if($timeatpreviousaddress == "3 Years"){ echo 'selected = "yes"'; } ?> value="3 Years">3 Years</option>
      <option <?php if($timeatpreviousaddress == "4 Years"){ echo 'selected = "yes"'; } ?> value="4 Years">4 Years</option>
      <option <?php if($timeatpreviousaddress == "5 Years"){ echo 'selected = "yes"'; } ?> value="5 Years">5 Years</option>
      <option <?php if($timeatpreviousaddress == "More Than 5 Years"){ echo 'selected = "yes"'; } ?> value="More Than 5 Years">More Than 5 Years</option>
      </select>
      
    </div>
    <div class="form-group col-xs-4">
      <label for="residentialstatus">Residential Status</label>
      <select name="residentialstatus" id="residentialstatus" class="form-control" required >
      <option <?php if($residentialstatus == "Temporary Residence"){ echo 'selected = "yes"'; } ?> value="Temporary Residence">Temporary Residence</option>
      <option <?php if($residentialstatus == "Family House"){ echo 'selected = "yes"'; } ?> value="Family House">Family House</option>
      <option <?php if($residentialstatus == "Rented"){ echo 'selected = "yes"'; } ?> value="Rented">Rented</option>
      <option <?php if($residentialstatus == "Owned"){ echo 'selected = "yes"'; } ?> value="Owned">Owned</option>
      <option <?php if($residentialstatus == "Provided by Employer"){ echo 'selected = "yes"'; } ?> value="Provided by Employer">Provided by Employer</option>
      </select>
    </div>
    <div class="form-group col-xs-4">
      <label for="maritalstatus">Marital Status</label>
      <select name="maritalstatus" id="maritalstatus" class="form-control" required >
      <option <?php if($maritalstatus == "Single"){ echo 'selected = "yes"'; } ?> value="Single">Single</option>
      <option <?php if($maritalstatus == "Married"){ echo 'selected = "yes"'; } ?> value="Married">Married</option>
      <option <?php if($maritalstatus == "Separated"){ echo 'selected = "yes"'; } ?> value="Separated">Separated</option>
      <option <?php if($maritalstatus == "Divorced"){ echo 'selected = "yes"'; } ?> value="Divorced">Divorced</option>
      <option <?php if($maritalstatus == "Widowed"){ echo 'selected = "yes"'; } ?> value="Widowed">Widowed</option>
      </select>
    </div>
    </div>
    <div class="row">
    <div class="form-group col-xs-6">
      <label for="dependents">Dependants</label>
      <select name="dependents" id="dependents" class="form-control" required >
      <option <?php if($dependents == "0"){ echo 'selected = "yes"'; } ?> value="0">0</option>
      <option <?php if($dependents == "1"){ echo 'selected = "yes"'; } ?> value="1">1</option>
      <option <?php if($dependents == "2"){ echo 'selected = "yes"'; } ?> value="2">2</option>
      <option <?php if($dependents == "3"){ echo 'selected = "yes"'; } ?> value="3">3</option>
      <option <?php if($dependents == "4"){ echo 'selected = "yes"'; } ?> value="4">4</option>
      <option <?php if($dependents == "5"){ echo 'selected = "yes"'; } ?> value="5">5</option>
      <option <?php if($dependents == "6"){ echo 'selected = "yes"'; } ?> value="6">6</option>
      <option <?php if($dependents == "7"){ echo 'selected = "yes"'; } ?> value="7">7</option>
      <option <?php if($dependents == "8"){ echo 'selected = "yes"'; } ?> value="8">8</option>
      <option <?php if($dependents == "9"){ echo 'selected = "yes"'; } ?> value="9">9</option>
      <option <?php if($dependents == "10"){ echo 'selected = "yes"'; } ?> value="10">10</option>
      <option <?php if($dependents == "More than 10"){ echo 'selected = "yes"'; } ?> value="More than 10">More than 10</option>
      </select>
    </div>
    <div class="form-group col-xs-6">
      <label for="monthlyexpense">Average Monthly Household Expenses</label>
      <input type="text" name="monthlyexpense" id="monthlyexpense" value="<?php echo $monthlyexpense; ?>" class="form-control" placeholder="Average Monthly Household Expenses" required />
    </div>
    </div>
    </fieldset>
	    </div>
	    <div class="tb-pane" id="tab2">
        <p class="text-muted">Income Details</p>
	          <fieldset name="Income Details" title="Income Details">
         <div class="row">   
    <div class="form-group col-xs-4">
      <label for="monthlyincome">Net Monthly Income</label>
      <input type="text" name="monthlyincome" value="<?php echo $monthlyincome; ?>" id="monthlyincome" class="form-control" placeholder="Net Monthly Income" required />
    </div>
    <div class="form-group col-xs-4">
      <label for="otherincome">Other Income</label>
      <input type="text" name="otherincome" id="otherincome" value="<?php echo $otherincome; ?>" class="form-control" placeholder="Other Income" />
    </div>
    <div class="form-group col-xs-4">
      <label for="payday">Pay Day</label>
      <input type="text" name="payday" id="payday" value="<?php echo $payday; ?>" class="form-control" placeholder="Pay Day" required />
    </div>
    </div>     
    </fieldset>
	    </div>
		<div class="tb-pane" id="tab3">
        <p class="text-muted">Your Existing Loans</p>
			<fieldset title="Your Existing Loans" name="Your Existing Loans">
            
            <div class="">        
   <div class="form-group notmd-3">
      <label for="lender">Name of Lender</label>
      <input type="text" name="lender" id="lender" value="<?php echo $lender; ?>" class="form-control" placeholder="Name of Lender" />
    </div>
    <div class="row">
    <div class="form-group col-xs-4">
      <label for="loanamount0">Loan Amount</label>
      <input type="text" name="loanamount0" id="loanamount0" value="<?php echo $loanamount0; ?>" class="form-control" placeholder="Loan Amount" />
    </div>
    
    <div class="form-group col-xs-4">
      <label for="loanbalance">Loan Balance</label>
      <input type="text" name="loanbalance" id="loanbalance" value="<?php echo $loanbalance; ?>" class="form-control" placeholder="Loan Balance" />
    </div>
    
    <div class="form-group col-xs-4">
      <label for="monthlyrepayments0">Monthly Repayments</label>
      <input type="text" name="monthlyrepayments0" id="monthlyrepayments0" value="<?php echo $monthlyrepayments0; ?>" class="form-control" placeholder="Monthly Repayments" />
    </div></div>
    </div>
    
           
   <div class="form-group notmd-3">
      <label for="lender2">Name of Lender</label>
      <input type="text" name="lender2" id="lender2" class="form-control" value="<?php echo $lender2; ?>" placeholder="Name of Lender" />
    </div>
    <div class="row"> 
    <div class="form-group col-xs-4">
      <label for="loanamount2">Loan Amount</label>
      <input type="text" name="loanamount2" id="loanamount2" value="<?php echo $loanamount2; ?>" class="form-control" placeholder="Loan Amount" />
    </div>
    
    <div class="form-group col-xs-4">
      <label for="loanbalance2">Loan Balance</label>
      <input type="text" name="loanbalance2" id="loanbalance2" value="<?php echo $loanbalance2; ?>" class="form-control" placeholder="Loan Balance" />
    </div>
    
    <div class="form-group col-xs-4">
      <label for="monthlyrepayments2">Monthly Repayments</label>
      <input type="text" name="monthlyrepayments2" id="monthlyrepayments2" value="<?php echo $monthlyrepayments2; ?>" class="form-control" placeholder="Monthly Repayments" />
    </div>
    </div>
    
           
   <div class="form-group notmd-3">
      <label for="lender3">Name of Lender</label>
      <input type="text" name="lender3" id="lender3" class="form-control" value="<?php echo $lender3; ?>" placeholder="Name of Lender" />
    </div>
    <div class="row"> 
    <div class="form-group col-xs-4">
      <label for="loanamount3">Loan Amount</label>
      <input type="text" name="loanamount3" id="loanamount3" value="<?php echo $loanamount3; ?>" class="form-control" placeholder="Loan Amount" />
    </div>
    
    <div class="form-group col-xs-4">
      <label for="loanbalance3">Loan Balance</label>
      <input type="text" name="loanbalance3" id="loanbalance3" value="<?php echo $loanbalance3; ?>" class="form-control" placeholder="Loan Balance" />
    </div>
    
    <div class="form-group col-xs-4">
      <label for="monthlyrepayments3">Monthly Repayments</label>
      <input type="text" name="monthlyrepayments3" id="monthlyrepayments3" value="<?php echo $monthlyrepayments3; ?>" class="form-control" placeholder="Monthly Repayments" />
    </div>
    </div>
    
    <!--<div class="form-group">
      <label for="loansinstatement">Loans Reflected in Your Statement</label>
      <input type="text" name="loansinstatement" value="<?php echo $loansinstatement; ?>" id="loansinstatement" class="form-control" placeholder="Loans Reflected in Your Statement" required />
    </div>
    <div class="form-group">
      <label for="otherloans">Other Loans not Reflected</label>
      <input type="text" name="otherloans" id="otherloans" value="<?php echo $otherloans; ?>" class="form-control" placeholder="Other Loans not Reflected" />
    </div>
    <div class="form-group">
      <label for="monthlyrepayments">Monthly Repayments</label>
      <input type="text" name="monthlyrepayments" id="monthlyrepayments" value="<?php echo $monthlyrepayments; ?>" class="form-control" placeholder="Monthly Repayments" required />
    </div>-->
    </fieldset>
	    </div>
		<div class="tb-pane" id="tab4">
        <p class="text-muted">Employment Details</p>
			    <fieldset title="Employment Details" name="Employment Details">
    <div class="form-group">
      <label for="employmenttype">Employment Type</label>
      <select name="employmenttype" id="employmenttype" class="form-control" required >
      <option <?php if($employmenttype == "Permanent Employment"){ echo 'selected = "yes"'; } ?> value="Permanent Employment">Permanent Employment</option>
      <option <?php if($employmenttype == "Contract/Temporary"){ echo 'selected = "yes"'; } ?> value="Contract/Temporary">Contract/Temporary</option>
      <option <?php if($employmenttype == "Self-Employed"){ echo 'selected = "yes"'; } ?> value="Self-Employed">Self-Employed</option>
      <option <?php if($employmenttype == "Unemployed"){ echo 'selected = "yes"'; } ?> value="Unemployed">Unemployed</option>
      </select>
    </div>
    <div class="form-group">
      <label for="currentemployer">Current Employer</label>
      <input type="text" name="currentemployer" id="currentemployer" value="<?php echo $currentemployer; ?>" class="form-control" placeholder="Current Employer" required />
    </div>
    <div class="row">
    <div class="form-group col-xs-6">
      <label for="employmentdate">Employment Date</label>
      <input type="text" name="employmentdate" value="<?php echo $employmentdate; ?>" id="employmentdate" class="form-control" placeholder="Employment Date" required />
    </div>
    <div class="form-group col-xs-6">
      <label for="staffid">Staff ID Number</label>
      <input type="text" name="staffid" id="staffid" value="<?php echo $staffid; ?>" class="form-control" placeholder="Staff ID Number" required />
    </div>
    </div>
    
    <div class="form-group">
      <label for="employersaddress">Current Employer's Address</label>
      <textarea  name="employersaddress" id="employersaddress" class="form-control" placeholder="Current Employer's Address" required><?php echo $employersaddress; ?></textarea>
      
    </div>
    <div class="row">
    <div class="form-group col-xs-4">
      <label for="employerstelephone">Employer's Telephone (HR)</label>
      <input type="text" name="employerstelephone" id="employerstelephone" value="<?php echo $employerstelephone; ?>" class="form-control" placeholder="Employer's Telephone (HR)" required />
    </div>
    <div class="form-group col-xs-8">
      <label for="staffsemail">Official Email Address</label>
      <input type="text" name="staffsemail" id="staffsemail" value="<?php echo $staffsemail; ?>" class="form-control" placeholder="Staff's Official Email Address" required />
    </div>
    </div>
    <div class="form-group">
      <label for="industry">Industry/Sector</label>
      <input type="text" name="industry" id="industry" value="<?php echo $industry; ?>" class="form-control" placeholder="Industry/Sector" required />
    </div>
    <div class="form-group">
      <label for="personalemail">Personal Email Address</label>
      <input type="text" name="personalemail" id="personalemail" value="<?php echo $personalemail; ?>" class="form-control" placeholder="Personal Email Address" required />
    </div>
    <div class="row">
    <div class="form-group col-xs-6">
      <label for="monthsinpreviousemployment">Number of Months in Previous Employment</label>
      <input type="text" name="monthsinpreviousemployment" value="<?php echo $monthsinpreviousemployment; ?>" id="monthsinpreviousemployment" class="form-control" placeholder="Number of Months in Previous Employment" required />
    </div>
    <div class="form-group  col-xs-6">
      <label for="employersinlastfiveyears">Number of Employers in Last 5 Years</label>
      <input type="text" name="employersinlastfiveyears" value="<?php echo $employersinlastfiveyears; ?>" id="employersinlastfiveyears" class="form-control" placeholder="Number of Employers in Last 5 Years" required />
    </div></div>
    </fieldset>
	    </div>
		<div class="tb-pane" id="tab5">
        <p class="text-muted">Level of Education</p>
		 <fieldset name="Level of Education" title="Level of Education">
    
    
    <div class="form-group">
      <label for="levelofeducation">Level Of Education</label>
      <select  name="levelofeducation" id="levelofeducation" class="form-control" required >
      <option <?php if($levelofeducation == "Secondary"){ echo 'selected = "yes"'; } ?> value="Secondary">Secondary</option>
      <option <?php if($levelofeducation == "Graduate"){ echo 'selected = "yes"'; } ?> value="Graduate">Graduate</option>
      <option <?php if($levelofeducation == "Post-Graduate"){ echo 'selected = "yes"'; } ?> value="Post-Graduate">Post-Graduate</option>
      <option <?php if($levelofeducation == "Doctorate"){ echo 'selected = "yes"'; } ?> value="Doctorate">Doctorate</option>
      </select>
      
    </div>
    <div class="form-group">
      <label for="lastinstitution">Last School Attended</label>
      <input type="text" name="lastinstitution" id="lastinstitution" value="<?php echo $lastinstitution; ?>" class="form-control" placeholder="Last Educational Institution Attended" required />
    </div>
    </fieldset>
	    </div>
		<div class="tb-pane" id="tab6">
        <p class="text-muted">Next of Kin/Blood Relative</p>
			   <fieldset name="Next of Kin/Blood Relative" title="Next of Kin/Blood Relative">
    <div class="form-group">
      <label for="kinname">Next of Kin Name</label>
      <input type="text" name="kinname" id="kinname" value="<?php echo $kinname; ?>" class="form-control" placeholder="Next of Kin Name" required />
    </div>
    <div class="form-group">
      <label for="kinrelationship">Next of Kin Relationship</label>
      <input type="text" name="kinrelationship" id="kinrelationship" value="<?php echo $kinrelationship; ?>" class="form-control" placeholder="Next of Kin Relationship" required />
    </div>
    <div class="form-group">
      <label for="kinhomeaddress">Next of Kin Home Address</label>
      <textarea name="kinhomeaddress" id="kinhomeaddress" class="form-control" placeholder="Next of Kin Home Address" required><?php echo $kinhomeaddress; ?></textarea>
    </div>
    <div class="row">
    <div class="form-group  col-xs-6">
      <label for="kinmobilenumber">Next of Kin Mobile Number</label>
      <input type="text" name="kinmobilenumber" id="kinmobilenumber" value="<?php echo $kinmobilenumber; ?>" class="form-control" placeholder="Next of Kin Mobile Number" required />
    </div>
    <div class="form-group  col-xs-6">
      <label for="kinoccupation">Next of Kin Occupation</label>
      <input type="text" name="kinoccupation" id="kinoccupation" value="<?php echo $kinoccupation; ?>" class="form-control" placeholder="Next of Kin Occupation" required />
    </div></div>
    <div class="form-group">
      <label for="kinemployer">Next of Kin Employer</label>
      <input type="text" name="kinemployer" id="kinemployer" value="<?php echo $kinemployer; ?>" class="form-control" placeholder="Next of Kin Employer" required />
    </div>
    <div class="form-group">
      <label for="kinemail">Next of Kin Email Address</label>
      <input type="text" name="kinemail" id="kinemail" value="<?php echo $kinemail; ?>" class="form-control" placeholder="Next of Kin Emaill Address" required />
    </div>
    </fieldset>
    
	    </div>
		<div class="tb-pane" id="tab7">
        <p class="text-muted">Bank/Disbursement Details</p>
			    <fieldset title="Bank/Disbursement Details" name="Bank/Disbursement Details">
    
    <div class="form-group">
      <label for="bankname">Bank Name</label>
      <input type="text" name="bankname" id="bankname" value="<?php echo $bankname; ?>" class="form-control" placeholder="Bank Name" required />
    </div>
    <div class="row">
    <div class="form-group  col-xs-6">
      <label for="accountnumber">Account Number</label>
      <input type="text" name="accountnumber" id="accountnumber" value="<?php echo $accountnumber; ?>" class="form-control" placeholder="Account Number" required />
    </div>
    <div class="form-group  col-xs-6">
      <label for="accountname">Account Name</label>
      <input type="text" name="accountname" id="accountname" value="<?php echo $accountname; ?>" class="form-control" placeholder="Account Name" required />
    </div></div>
    <div class="row">
    <div class="form-group  col-xs-6">
      <label for="accounttype">Account Type</label>
      <select  name="accounttype" id="accounttype" class="form-control"  required >
      <option <?php if($accounttype == "Savings Account"){ echo 'selected = "yes"'; } ?> value="Savings Account">Savings Account</option>
      <option <?php if($accounttype == "Current Account"){ echo 'selected = "yes"'; } ?>  value="Current Account">Current Account</option>
      </select>
     
    </div>
    <div class="form-group  col-xs-6">
      <label for="banksortcode">Bank Sort Code</label>
      <input type="text" name="banksortcode" id="banksortcode" value="<?php echo $banksortcode; ?>" class="form-control" placeholder="Bank Sort Code" required />
    </div></div>
    <div class="form-group">
      <label for="bankaddress">Bank Branch/Address</label>
      <input type="text" name="bankaddress" id="bankaddress" value="<?php echo $bankaddress; ?>" class="form-control" placeholder="Bank Branch/Address" required />
    </div>
    </fieldset>
	    </div>
        <div class="tb-pane" id="tab8">
        <p class="text-muted">Loan Details</p>
        <fieldset name="Loan Details" title="Loan Details">
        <div class="row">
    <div class="form-group  col-xs-4">
      <label for="loanamount">Loan Amount Requested</label>
      <input type="text" name="loanamount" id="loanamount" value="<?php echo $loanamount; ?>"  class="form-control" placeholder="Loan Amount Requested" required />
    </div>
    <div class="form-group col-xs-4">
      <label for="loantenor">Loan Tenor (months)</label>
      <input type="text" name="loantenor" id="loantenor" class="form-control" value="<?php echo $loantenor; ?>" placeholder="Loan Tenor (months)" required />
    </div>
    <div class="form-group  col-xs-4">
      <label for="installment">Monthly Installment</label>
      <input type="text" name="installment" id="installment" value="<?php echo $installment; ?>" class="form-control" placeholder="Monthly Installment" required />
    </div></div>
    <div class="form-group">
      <label for="loanpurpose">Purpose of Loan</label>
      <input type="text" name="loanpurpose" id="loanpurpose" value="<?php echo $loanpurpose; ?>" class="form-control" placeholder="Purpose of Loan" required />
    </div>
    </fieldset>
	    </div>
	</div>
    </form>
    
    <div class="text-info">
    <h2 class="page-header">Terms and Conditions</h2>
          
        <p>These Terms and Conditions apply to and regulate the provision of credit facilities advanced by One Credit Limited (“One Credit”) to the Borrower herein. These Terms and Conditions, together with One Credit’s offer letter set out the terms governing this Loan Agreement.</p>
<p><b>1. INTEREST</b></p>
<p><b>●</b>The monthly interest rate for the Loan is ___% fixed for the term of this loan.</p>
<p><b>●</b>One Credit may in its sole discretion increase or decrease the prevailing interest rate for any reasons with or without prior recourse to the Borrower.<b></b></p>
<p><b>●</b>Any change in interest rate will take effect on the Borrower’s account following a minimum of 7 days written notice.</p>
<p><b>●</b>Total interest for the term of the loan will still be due in the event of the Borrower liquidating the loan before expiration.</p>
<p><b>●</b>All charges will be capitalized into the interest calculation.</p>
<p><b>2. PAYMENT</b></p>
<p>All payments by the Borrower will be made by one of the following methods: Direct Deposit at One Credit, Personal Cheque, Standing Order, or through an acceptable electronic channel. All other methods will be accepted with One Credit’s consent only.</p>
<p>●The Borrower will be given a dedicated and customized repayment plan upon approval of the loan application;</p>
<p>●The Borrower will be expected to make monthly repayments in accordance with the repayment plan; and</p>
<p>●The Borrower hereby agrees that nonpayment of the amount owed by the due date shall render the Borrower in default and entitles One Credit to take steps to recover the outstanding loan amount in accordance with section 5 below.</p>
<p>●In cases where the Borrower makes a direct cash payment in advance of due date, One Credit reserves the right to destroy all&nbsp; cheques (including post-dated) at the expiration of the loan or on payment of installment by the Borrower.</p>
<p>●Any refund made to the Borrower for duplicate payment will attract a refund processing fee of <span style="text-decoration: line-through;">N</span>500</p>
<p><b>3. CREDIT REFERENCE</b></p>
<p><b>●</b>One Credit or its duly authorized agents will utilize a dedicated Credit Agency for a credit report on the Borrower in considering any application for credit.<b></b></p>
<p><b>●</b>The Borrower authorizes One Credit to access any information available to One Credit as provided by the Credit Agency.</p>
<p><b>●</b>The Borrower also agrees that the Borrower’s details and the loan application decision may be registered with the Credit Agency.<b></b></p>
<p><b>4.&nbsp;&nbsp; NOTICES</b></p>
<p>The Borrower agrees that One Credit may communicate with them by sending notices, messages, alerts and statements in relation to this Agreement in the following manner:</p>
<p>●To the most recent physical address One Credit holds for the Borrower on file</p>
<p>●By delivery to any email address provided during the application process.</p>
<p>●By delivery of an SMS to any mobile telephone number the Borrower has provided to One Credit.</p>
<p><b>5. EVENT OF DEFAULT</b></p>
<p><b>5.1 Default in terms of this Agreement will occur if:</b></p>
<p><b>●</b>The Borrower fails to make any scheduled repayment in full on or before the payment date in accordance with the monthly repayment plan given to the Borrower;<b></b></p>
<p><b>●</b>Any representation, warranty or assurance made or given by the Borrower in connection with the application for this loan or any information or documentation supplied by the Borrower is later discovered to be materially incorrect; or<b></b></p>
<p><b>●</b>The Borrower does or omits to do anything which may prejudice One Credit’s rights in terms of this Agreement or causes One Credit to suffer any loss or damage.</p>
<p><b>5.2</b> <b>In the event of any default by the Borrower subject to clause 5.1 above –</b></p>
<p><b>●</b>One Credit reserves the right to assign its right, title and interest under the Agreement to an external Collections Agency who will take all reasonable steps to collect the outstanding loan amount.<b></b></p>
<p><b>●</b>One Credit also reserves the right to institute legal proceedings against the defaulting Borrower and is under no obligation to inform the Borrower before such proceedings commence.</p>
<p><b>●</b>The Borrower shall be responsible for all legal costs and expenses incurred by One Credit in attempting to obtain repayment of any outstanding loan balance owed by the Borrower. Interest on any amount which becomes due and payable shall be charged.</p>
<p><b>6. GENERAL</b></p>
<p><b>●</b>This Agreement represents the entire understanding between One Credit and the Borrower. No amendment shall be made unless same is agreed in writing by the parties.</p>
<p><b>●</b>The Borrower agrees and undertakes that for the period of this Agreement, the Borrower will not close the Borrower’s specified bank account.</p>
<p><b>●</b>This Agreement shall be governed by the laws of the Federal Republic of Nigeria and shall be subject to the jurisdiction of the courts of the Federal Republic of Nigeria.<b></b></p>
<p><b>●</b>If One Credit does not strictly enforce its rights under this Agreement (including its right to insist on the repayment of all sums due on the Repayment Due Date) or grant the Borrower an indulgence, One Credit will not be deemed to have lost those rights and will not be prevented from insisting upon its strict rights at a later date.<b></b></p>
<p><b>●</b>One Credit reserves the right to transfer or assign its rights and obligations under this Agreement (including its obligation to lend money to the Borrower or the amount owed under this Agreement) to another person. One Credit will only inform the Borrower if such a transfer causes the arrangements for the administration of this Agreement to change.<b></b></p>
<p><b>●</b>The Borrower authorizes and consents to all lawful access, use or disclosure of the Borrower’s particulars in the application by One Credit which may include but shall not be limited to purposes necessary to promote or sustain the business of One Credit; and the Borrower waives any claims the Borrower may have against One Credit arising from any such access, use or disclosure.</p>
<p><b>●</b>Applicants/Borrowers that submit fake or fraudulent documentation shall be reported to appropriate regulatory bodies including, but not limited to the Nigerian Police, for prosecution and/or any other penalties as the regulatory body deems fit.</p>
<p>●The Lender reserves the right to review, verify and screen all details, including but not limited to the loan amount applied for by the Borrower and the Lender is not bound to grant the exact loan amount applied for by the Borrower.</p>
<p>&nbsp;</p>
<p>I,____________________________________________________________________________ (the Borrower) hereby confirm that I have read, understood and agreed to the above terms and conditions. I also authorize One Credit to present the cheques issued by me (in favour of One Credit) for the repayment of the Loan as and when due until the Loan has been fully paid and if, for any reason whatsoever, my account is not funded at the time of presentation, I shall be criminally liable under the Dishonored</p>
<p>Cheques (Offences) Act, CAP D1, LFN 2004.</p>
<p>&nbsp;</p>
<p>Applicant’s Name____________________________________________________ Applicant’s Signature____________________________________________</p>
             
                     

             
                     
      
    </div>
    </div>
</body>
</html>