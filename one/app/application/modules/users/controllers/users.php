<?php

class Users extends MX_Controller
{
private $server = 'https://onecred.sandbox.mambu.com';// ToDo: Change URL to live
function __construct() {
parent::__construct();

$this->load->module('template');
$this->load->module('admintemplate');
$this->load->library('pagination');
//$this->output->enable_profiler(TRUE);
}

function get($order_by){
$this->load->model('mdl_users');
$query = $this->mdl_users->get($order_by);
return $query;
}

function get_with_limit($limit, $offset, $order_by) {
$this->load->model('mdl_users');
$query = $this->mdl_users->get_with_limit($limit, $offset, $order_by);
return $query;
}

function get_where($id){
$this->load->model('mdl_users');
$query = $this->mdl_users->get_where($id);
return $query;
}

function get_where_custom($col, $value) {
$this->load->model('mdl_users');
$query = $this->mdl_users->get_where_custom($col, $value);
return $query;
}

function get_where_like($field,$key){
$this->load->model('mdl_users');
$query = $this->mdl_users->get_where_like($field,$key);
return $query;
}


function _insert($data){
$this->load->model('mdl_users');
$this->mdl_users->_insert($data);
}

function _update($id, $data){
$this->load->model('mdl_users');
$this->mdl_users->_update($id, $data);
}

function _update_where($col,$col_val, $data){
    $this->load->model('mdl_users');
    $this->mdl_users->_update_where($col,$col_val, $data);
}

function _delete($id){
$this->load->model('mdl_users');
$this->mdl_users->_delete($id);
}

function count_where($column, $value) {
$this->load->model('mdl_users');
$count = $this->mdl_users->count_where($column, $value);
return $count;
}

function get_max() {
$this->load->model('mdl_users');
$max_id = $this->mdl_users->get_max();
return $max_id;
}

function _custom_query($mysql_query) {
$this->load->model('mdl_users');
$query = $this->mdl_users->_custom_query($mysql_query);
return $query;
}

function get_where_custom_with_limit($col, $value, $limit, $offset, $order_by) {
$this->load->model('mdl_users');
$query = $this->mdl_users->get_where_custom_with_limit($col, $value, $limit, $offset, $order_by);
return $query;
}

function get_form_data(){
    $data = $this->input->post();
    return $data;
}

function index(){
    $this->getusers();

    //$this->getusers();
}

function adduser(){
$userdata = $this->get_form_data();
$data = $userdata;
unset($userdata['agree']);
//check if user exist in database
$checkemail = $this->count_where('email',$userdata['email']);

if($checkemail < 1){

    $sendtomambu = $this->addtomambu($userdata);
  //print_r($sendtomambu->client->id);
    if(!isset($sendtomambu->returnCode)){
      //Todo: uncomment the function below 
      $this->_insert($userdata);
    $message = '
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
  <head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Notification Email</title>
  </head>
  <body bgcolor="#f6f6f6" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; margin: 0; padding: 0;">&#13;
&#13;
<!-- body -->&#13;
<table class="body-wrap" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 20px;"><tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>&#13;
		<td class="container" bgcolor="#FFFFFF" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto; padding: 20px; border: 1px solid #f0f0f0;">&#13;
&#13;
			<!-- content -->&#13;
			<div class="content" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; max-width: 600px; display: block; margin: 0 auto; padding: 0;">&#13;
			<table style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 0;"><tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">&#13;
						<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">Hello '.$userdata['firstname'].',</p>&#13;
						<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">Thank you for applying for a One Credit Loan.</p>&#13;
						<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">We will contact you within the next 48 hours to review your details.</p>&#13;
            <p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">You can track your application on our <a href="http://www.one-cred.com" target="_blank">website</a> using your CLIENT ID: '.$sendtomambu->client->id.'</p>&#13;
            
						<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">For further information on this mail or queries regarding the application, please contact us on <a href="call:08091112274" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; color: #348eda; margin: 0; padding: 0;">0809-111-CASH (08091112274)</a> or email <a href="mailto:sales@one-cred.com" target="_blank" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; color: #348eda; margin: 0; padding: 0;">sales@one-cred.com</a> </p>&#13;
						<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">Thank You.</p>&#13;
						<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;"><a href="http://www.one-cred.com" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; color: #348eda; margin: 0; padding: 0;"><img src="'.$this->template->get_asset().'/images/logo.png" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; max-width: 100%; margin: 0; padding: 0;" /></a></p>&#13;
					</td>&#13;
				</tr></table></div>&#13;
			<!-- /content -->&#13;
									&#13;
		</td>&#13;
		<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>&#13;
	</tr></table><!-- /body --></body>
</html>
    ';
    $this->sendmail($userdata['email'],'Your One Credit Loan Application',$message);
    $message2 = '
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
  <head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Notification Email</title>
  </head>
  <body bgcolor="#f6f6f6" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; margin: 0; padding: 0;">&#13;
&#13;
<!-- body -->&#13;
<table class="body-wrap" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 20px;"><tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>&#13;
		<td class="container" bgcolor="#FFFFFF" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto; padding: 20px; border: 1px solid #f0f0f0;">&#13;
&#13;
			<!-- content -->&#13;
			<div class="content" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; max-width: 600px; display: block; margin: 0 auto; padding: 0;">&#13;
			<table style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 0;"><tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">&#13;
						<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">Hi,</p>&#13;
						<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">Someone has just applied for a Loan on One Credit Website</p>&#13;
						<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">Below are the details.</p>&#13;
						<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
<table style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">

<tr>
<td colspan="2">
<h4>Personal Data</h4>
<hr />
</td>
</tr>

<tr>
<td>First Name</td>
<td>'. $userdata['firstname'] .'</td>
</tr>
<tr>
<td>Middle Name</td>
<td>'. $userdata['middlename'] .'</td>
</tr>
<tr>
<td>Surname</td>
<td>'. $userdata['surname'] .'</td>
</tr>
<tr>
<td>Other/Maiden Name</td>
<td>'. $userdata['maidenname'] .'</td>
</tr>
<tr>
<td>Date of Birth</td>
<td>'. $userdata['dateofbirth'] .'</td>
</tr>
<tr>
<td>Mobile Number</td>
<td>'. $userdata['mobilenumber'] .'</td>
</tr>
<tr>
<td>Gender</td>
<td>'. $userdata['gender'] .'</td>
</tr>
<tr>
<td>Email Address</td>
<td>'. $userdata['email'] .'</td>
</tr>
<tr>
<td>Home/Landline Number</td>
<td>'. $userdata['homenumber'] .'</td>
</tr>
<tr>
<td>Means of Identification</td>
<td>'. $userdata['meansofidentification'] .'</td>
</tr>
<tr>
<td>Identification Number</td>
<td>'. $userdata['idnumber'] .'</td>
</tr>
<tr>
<td>Other Means of Identification</td>
<td>'. $userdata['otheridentification'] .'</td>
</tr>
<tr>
<td>Current Home Address Line 1</td>
<td>'. $userdata['addressline1'] .'</td>
</tr>
<tr>
<td>Current Home Address Line 2</td>
<td>'. $userdata['addressline2'] .'</td>
</tr>
<tr>
<td>City</td>
<td>'. $userdata['city'] .'</td>
</tr>
<tr>
<td>State</td>
<td>'. $userdata['state'] .'</td>
</tr>
<tr>
<td>Nearest Landmark</td>
<td>'. $userdata['nearestlandmark'] .'</td>
</tr>
<tr>
<td>Time at Current Address</td>
<td>'. $userdata['timeatcurrentaddress'] .'</td>
</tr>
<tr>
<td>Previous Address</td>
<td>'. $userdata['previousaddressline1'] .'</td>
<td>'. $userdata['previousaddressline2'] .'</td>
<td>'. $userdata['previouscity'] .'</td>
<td>'. $userdata['previousstate'] .'</td>
</tr>
<tr>
<td>Time at Previous Address</td>
<td>'. $userdata['timeatpreviousaddress'] .'</td>
</tr>
<tr>
<td>Residential Status</td>
<td>'. $userdata['residentialstatus'] .'</td>
</tr>
<tr>
<td>Marital Status</td>
<td>'. $userdata['maritalstatus'] .'</td>
</tr>
<tr>
<td>Dependants</td>
<td>'. $userdata['dependents'] .'</td>
</tr>
<tr>
<td>Average Monthly Household Expenses</td>
<td>'. $userdata['monthlyexpense'] .'</td>
</tr>


<tr>
<td colspan="2">
<h4>Income Details</h4>
<hr />
</td>
</tr>

<tr>
<td>Net Monthly Income</td>
<td>'. $userdata['monthlyincome'] .'</td>
</tr>
<tr>
<td>Other Income</td>
<td>'. $userdata['otherincome'] .'</td>
</tr>
<tr>
<td>Pay Day</td>
<td>'. $userdata['payday'] .'</td>
</tr>

<tr>
<td colspan="2">
<h4>Your Existing Loans</h4>
<hr />
</td>
</tr>

<tr>
<td>Name of Lender</td>
<td>'. $userdata['lender'] .'</td>
</tr>
<tr>
<td>Loan Amount</td>
<td>'. $userdata['loanamount0'] .'</td>
</tr>
<tr>
<td>Loan Balance</td>
<td>'. $userdata['loanbalance'] .'</td>
</tr>
<tr>
<td>Monthly Repayments</td>
<td>'. $userdata['monthlyrepayments0'] .'</td>
</tr>

<tr>
<td>Name of Lender</td>
<td>'. $userdata['lender2'] .'</td>
</tr>
<tr>
<td>Loan Amount</td>
<td>'. $userdata['loanamount2'] .'</td>
</tr>
<tr>
<td>Loan Balance</td>
<td>'. $userdata['loanbalance2'] .'</td>
</tr>
<tr>
<td>Monthly Repayments</td>
<td>'. $userdata['monthlyrepayments2'] .'</td>
</tr>

<tr>
<td>Name of Lender</td>
<td>'. $userdata['lender3'] .'</td>
</tr>
<tr>
<td>Loan Amount</td>
<td>'. $userdata['loanamount3'] .'</td>
</tr>
<tr>
<td>Loan Balance</td>
<td>'. $userdata['loanbalance3'] .'</td>
</tr>
<tr>
<td>Monthly Repayments</td>
<td>'. $userdata['monthlyrepayments3'] .'</td>
</tr>

<tr>
<td colspan="2">
<h4>Employment Details</h4>
<hr />
</td>
</tr>

<tr>
<td>Employment Type</td>
<td>'. $userdata['employmenttype'] .'</td>
</tr>
<tr>
<td>Current Employer</td>
<td>'. $userdata['currentemployer'] .'</td>
</tr>
<tr>
<td>Employment Date</td>
<td>'. $userdata['employmentdate'] .'</td>
</tr>
<tr>
<td>Staff ID Number</td>
<td>'. $userdata['staffid'] .'</td>
</tr>
<tr>
<td>Current Employer\'s Address</td>
<td>'. $userdata['employersaddressline1'] .'</td>
<td>'. $userdata['employersaddressline2'] .'</td>
<td>'. $userdata['employerscity'] .'</td>
<td>'. $userdata['employersstate'] .'</td>
</tr>
<tr>
<td>Employer\'s Telephone (HR)</td>
<td>'. $userdata['employerstelephone'] .'</td>
</tr>
<tr>
<td>Official Email Address</td>
<td>'. $userdata['staffsemail'] .'</td>
</tr>
<tr>
<td>Industry/Sector</td>
<td>'. $userdata['industry'] .'</td>
</tr>
<tr>
<td>Personal Email Address</td>
<td>'. $userdata['personalemail'] .'</td>
</tr>
<tr>
<td>Number of Months in Previous Employment</td>
<td>'. $userdata['monthsinpreviousemployment'] .'</td>
</tr>
<tr>
<td>Number of Employers in Last 5 Years</td>
<td>'. $userdata['employersinlastfiveyears'] .'</td>
</tr>


<tr>
<td colspan="2">
<h4>Level of Education</h4>
<hr />
</td>
</tr>


<tr>
<td>Level Of Education</td>
<td>'. $userdata['levelofeducation'] .'</td>
</tr>
<tr>
<td>Last School Attended</td>
<td>'. $userdata['lastinstitution'] .'</td>
</tr>


<tr>
<td colspan="2">
<h4>Next of Kin/Blood Relative</h4>
<hr />
</td>
</tr>


<tr>
<td>Next of Kin Name</td>
<td>'. $userdata['kinname'] .'</td>
</tr>
<tr>
<td>Next of Kin Relationship</td>
<td>'. $userdata['kinrelationship'] .'</td>
</tr>
<tr>
<td>Next of Kin Home Address</td>
<td>'. $userdata['kinaddressline1'] .'</td>
<td>'. $userdata['kinaddressline2'] .'</td>
<td>'. $userdata['kincity'] .'</td>
<td>'. $userdata['kinstate'] .'</td>
</tr>
<tr>
<td>Next of Kin Mobile Number</td>
<td>'. $userdata['kinmobilenumber'] .'</td>
</tr>
<tr>
<td>Next of Kin Occupation</td>
<td>'. $userdata['kinoccupation'] .'</td>
</tr>
<tr>
<td>Next of Kin Employer</td>
<td>'. $userdata['kinemployer'] .'</td>
</tr>
<tr>
<td>Next of Kin Email Address</td>
<td>'. $userdata['kinemail'] .'</td>
</tr>


<tr>
<td colspan="2">
<h4>Bank/Disbursement Details</h4>
<hr />
</td>
</tr>



<tr>
<td>Bank Name</td>
<td>'. $userdata['bankname'] .'</td>
</tr>
<tr>
<td>Account Number</td>
<td>'. $userdata['accountnumber'] .'</td>
</tr>
<tr>
<td>Account Name</td>
<td>'. $userdata['accountname'] .'</td>
</tr>
<tr>
<td>Account Type</td>
<td>'. $userdata['accounttype'] .'</td>
</tr>
<tr>
<td>Bank Branch/Address</td>
<td>'. $userdata['bankaddress'] .'</td>
</tr>


<tr>
<td colspan="2">
<h4>Loan Details</h4>
<hr />
</td>
</tr>


<tr>
<td>Loan Amount Requested</td>
<td>'. $userdata['loanamount'] .'</td>
</tr>
<tr>
<td>Loan Tenor</td>
<td>'. $userdata['loantenor'] .'</td>
</tr>
<tr>
<td>Monthly Installment</td>
<td>'. $userdata['installment'] .'</td>
</tr>
<tr>
<td>Purpose of Loan</td>
<td>'. $userdata['loanpurpose'] .'</td>
</tr>
</table>
                        </p>&#13;
						<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;"><a href="http://www.one-cred.com" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; color: #348eda; margin: 0; padding: 0;"><img src="'.$this->template->get_asset().'/images/logo.png" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; max-width: 100%; margin: 0; padding: 0;" /></a></p>&#13;
					</td>&#13;
				</tr></table></div>&#13;
			<!-- /content -->&#13;
									&#13;
		</td>&#13;
		<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>&#13;
	</tr></table><!-- /body --></body>
</html>
    ';
  if(empty($userdata['documents'])){
    $this->sendmail('info@one-cred.com','New One Credit Loan Application',$message2);
  }else{
    $documents = $userdata['documents'];
    $splitdocs = explode("+++",$documents);
    $attachmentfiles = array();
    foreach($splitdocs as $docs){
    $splitdoc = explode("/app/",$docs);
      $attachmentfiles[] = './'.$splitdoc[1]; 
    }
    //todo 
    foreach($attachmentfiles as $attach){
        $this->sendattachment($sendtomambu->client->encodedKey, $attach);
    }   
    $this->sendmailmultiattach('info@one-cred.com','New One Credit Loan Application',$message2,$attachmentfiles);
  }

        $data['alert'] = "Account Created Successfully";
        $data['alert_type'] = "success";
        $data['message'] = $data['alert'];
        $data['type'] = $data['alert_type'];
        if($this->session->userdata('id')){
        $this->getusers($data['alert_type'],$data['message']);
        }else{
          redirect('http://www.one-cred.com/application-successful/');
        }
        // ToDo: Uncomment
 
    

  }else{
      unset($this->input);
    log_message('error', $sendtomambu);
    if($this->session->userdata('id')){
        $data['alert'] = "An error occured. User could not be created.";
    $data['alert_type'] = "error";
    $data['message'] = $data['alert'];
    $data['type'] = $data['alert_type'];
        $this->getusers($data['alert_type'],$data['message']);
        }else{
        redirect('http://www.one-cred.com/application-failed/ ');
        }
    }
}else{
    unset($this->input);
    
    if($this->session->userdata('id')){
        $data['alert'] = "This user already exists.";
    $data['alert_type'] = "warning";
    $data['message'] = $data['alert'];
    $data['type'] = $data['alert_type'];
        $this->getusers($data['alert_type'],$data['message']);
        }else{
        redirect('http://www.one-cred.com/application-failed/ ');
        }
    
        
}

}


function getusers($alerttype = "",$alertmessage = ""){
    
    $alluser = $this->get('id');
    $total = $alluser->num_rows();
    $config['base_url'] = base_url('users/getusers');
    $config['total_rows'] = $total;
    $config['per_page'] = 50;
    $config['full_tag_open'] = '<ul>';
    $config['full_tag_close'] = '</ul>';
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $config['prev_tag_open'] = '<li>';
    $config['prev_tag_close'] = '</li>';
    $config['next_tag_close'] = '</li>';
    $config['next_tag_open'] = '<li>';
    $config['cur_tag_open'] = '<li class="active"><a href="#">';
    $config['cur_tag_close'] = '</a></li>';
    $this->pagination->initialize($config); 
    $data['pagenavi'] = $this->pagination->create_links();
    if($alerttype != "" && $alertmessage != ""){
        $data['alert'] = $alertmessage;
        $data['alert_type'] = $alerttype;
    }
    
    if(!$this->uri->segment(3)){
        $offset = 0;
    }else{
        $offset = $this->uri->segment(3);
        $data['c'] = $offset + 1;
    }
    $users = $this->get_with_limit($config['per_page'],$offset,'id');
    
    $data['access'] = 'administrator';
    
    $data['users'] = $users;
    $data['title'] = "Users";
    $data['view_file'] = "users";
    $data['module'] = "users";
    $this->admintemplate->build(true,$data); 
}

function updateuser(){
    $userdata = $this->get_form_data();

    unset($userdata['agree']);

    $this->_update($userdata['id'],$userdata);

    $alert['message'] = "The user has been Updated successfully";
    $alert['type'] = "success";
    if($this->session->userdata('id')){
            $this->getusers($alert['type'],$alert['message']);
        }
}

function deleteuser(){
    $id = $this->uri->segment(3);
    $this->_delete($id);
//    redirect('users');
}

function searchuser(){
    $data['access'] = "administrator";
    $key = $this->get_form_data();
    $users = $this->get_where_like('firstname',$key['usersearch']);
        $count = count($users->result());
        if($count > 0){
            $data['users'] = $users;
            }else{
        $users = $this->get_where_like('lastname',$key['usersearch']);
        $count = count($users->result());
        if($count > 0){
            $data['users'] = $users;
            }else{
                $users = $this->get_where_like('email',$key['usersearch']);
        $count = count($users->result());
        if($count > 0){
            $data['users'] = $users;
        }else{
            
        }
            }
            }
    $data['users'] = $users;
    $data['title'] = "User Search Result";
    $data['view_file'] = "users";
    $data['module'] = "users";
    $this->admintemplate->build(true,$data);
}

/**
 * Users::makeHash()
 * generate a secure hash string from a sring and a salt
 * @param mixed $data
 * @param string $salt
 * @return string
 */
function makeHash($data, $salt = "%gd:FG{hdfVFDds6egNNKYRfe dr"){
    
    /* Really? I think this is a little crazy, but not too crazy though..  */
    
    $secString = "";
    $hashData = do_hash($data);
    $hashSalt = do_hash($salt);
    $secString .= $hashData;
    $secString .= $hashSalt;
    $secString .= $hashData;
    
    $hash = do_hash($secString);
    $secHash = do_hash($hash);
    $token = do_hash($secHash);
   // echo $token."<br />";
    return $token;
    
}

/**
 * Users::accessLocker()
 * use 0 to accept all users and the respective role id for other users
 * @param mixed $role
 * @return boolean
 */
function accessLocker($role){
    $userrole = $this->session->userdata('usertype');
    if($userrole = "administrator"){
        return true;
    }elseif($role == "all"){
        return true;
    }elseif($role != $userrole){
        return false;
    }else{
        return false;
    }
    
}

function getusername($userid){
    $userdetail = $this->get_where($userid);
    foreach($userdetail->result() as $user){
        $username = $user->username;
    }
    return $username;
}

function getuserid($email){
    $userdetail = $this->get_where_custom("email",$email);
    foreach($userdetail->result() as $user){
        $userid = $user->id;
    }
    return $userid;
}

function importusers(){
   $userdata = $this->get_form_data();
   $config['upload_path'] = './assets/importusers/';
	$config['allowed_types'] = 'csv';
	$config['max_size']	= '4123';
    $this->upload->initialize($config);

	if ( ! $this->upload->do_upload())
	{
		$alert['message'] = $this->upload->display_errors();
        $alert['type'] = "error";
        $this->getusers($alert['type'],$alert['message']);
	}
	else
	{
    $uploaddata = $this->upload->data();
    $file = fopen(base_url('assets/importusers/'.$uploaddata['file_name']),'r');
    $cc = 0;
     while (($filedecode = fgetcsv($file)) !== FALSE) {
        $cc++;
        $splitname = explode(' ',$filedecode[0]);
        $firstname = $splitname[0];
        $lastname = $splitname[1];
        $email = $filedecode[1];
        
        $firstname = trim($firstname);
        $lastname = trim($lastname);
        $email = trim($email);
        $firstname = explode(' ',$firstname);
        $lastname = explode(' ',$lastname);
        $email = explode(' ',$email);
        $firstname = implode('_',$firstname);
        $lastname = implode('_',$lastname);
        $email = implode('_',$email);
        
        $checkemail = $this->count_where('email',$email);
        if($checkemail < 1){
            $data['firstname'] = $firstname;
            $data['lastname'] = $lastname;
            $data['email'] = $email;
            $data['status'] = "pending";
            $data['link'] = base_url('users/updateuser/'.urlencode($data['firstname']).'/'.urlencode($data['lastname']).'/'.urlencode($data['email']).'/'.$this->makeHash($firstname.'-'.$lastname.'-'.$email));
             $data['hash'] = $this->makeHash($firstname.'-'.$lastname.'-'.$email);
             $data['uniqueid'] = $this->createuniqueid();
             $data['vendor'] = $userdata['vendor'];
             $this->_insert($data);
        }
        
       }  
        $alert['message'] = 'You have successfully imported '.$cc.' users';
        $alert['type'] = "success";
        $this->getusers($alert['type'],$alert['message']);
	}
   

}

function createuniqueid(){
    $count;
     for($count = 1; $count > 0; $uid = random_string('numeric',10), $count = $this->count_where('uniqueid',$uid), $final = $uid);
     return $final;
}

function downloaduserlinks(){
    $this->load->helper('file');
    $this->load->model('mdl_users');
    $csvdata = $this->mdl_users->get_userlinks_as_csv();
    $time = time();
    if ( !write_file('./assets/csvdownloads/'.$time.'.csv', $csvdata)){
        $alert['message'] = "User Export Failed";
        $alert['type'] = "error";
        $this->getusers($alert['type'],$alert['message']);
    }else{
        $alert['message'] = "User Export Successful";
        $alert['type'] = "success";
        $this->getusers($alert['type'],$alert['message']);
        $data = file_get_contents('./assets/csvdownloads/'.$time.'.csv'); // Read the file's contents
        if(!$data){
        die('File does not exist');
        }
        $name = 'users_'.$time.'.csv';
        
        force_download($name, $data);  
    }

    
}
 
function currenttime(){    
$timestamp = time();
$timezone = 'UP1';
$daylight_saving = FALSE;
$times = gmt_to_local($timestamp, $timezone, $daylight_saving);
return $times;
}

function formattime($timestamp){
    $datestring = "%d/%m/%Y";
    $time = mdate($datestring, $timestamp);
    return $time;
}

function sendmail($to,$subject,$message,$attach = false,$attachment_path = ""){
    $this->email->from('admin@one-cred.com', 'One Credit');
    $this->email->to($to);
    
    $this->email->subject($subject);
    $this->email->message($message);
   	if($attach){
    $this->email->attach($attachment_path);
    }
    $send = $this->email->send();
    if(!$send){
        return false;
    }else{
        return true;
    }
}
  //todo
function sendmailmultiattach($to,$subject,$message,$attachment_path){
    $this->email->from('admin@one-cred.com', 'One Credit');
    $this->email->to($to);
    
    $this->email->subject($subject);
    $this->email->message($message);
   	foreach($attachment_path as $attach){
    $this->email->attach($attach);
    }
    $send = $this->email->send();
    if(!$send){
        return false;
    }else{
        return true;
    }
}
/*
function testmail(){
    $this->sendmail('olufemi@kvpafrica.com','Test','fooooo');
}
*/

function generatetc(){

    $id = $this->uri->segment(3);
    $userdata = $this->get_where($id);
    foreach($userdata->result('array') as $data){
        $this->load->view('tc',$data);

    }   

}

function downloadactiveuserscsv(){
    $this->load->helper('file');
    $this->load->model('mdl_users');
    $csvdata = $this->mdl_users->get_activeusers_as_csv();
    $time = time();
    if ( !write_file('./assets/activeusersdownloads/'.$time.'.csv', $csvdata)){
        $alert['message'] = "User Export Failed";
        $alert['type'] = "error";
        $this->getusers($alert['type'],$alert['message']);
    }else{
        $alert['message'] = "User Export Successful";
        $alert['type'] = "success";
        $this->getusers($alert['type'],$alert['message']);
        $data = file_get_contents('./assets/activeusersdownloads/'.$time.'.csv'); // Read the file's contents
        if(!$data){
        die('File does not exist');
        }
        $name = 'registed_users_'.$time.'.csv';
        
        force_download($name, $data);  
    }

    
}


function viewactiveusers(){
    $data['access'] = "administrator";
    $users = $this->get_where_custom('status','Active');

    $data['users'] = $users;

    $data['title'] = "Registered Users";
    $data['view_file'] = "users";
    $data['module'] = "users";
    $this->admintemplate->build(true,$data);
}

function totalusers(){
    $this->load->model('mdl_users');
    $count = $this->mdl_users->count_all();
    return $count;
}

function registeredtoday(){
    $this->load->model('mdl_users');
    $count = $this->mdl_users->todaysusers();
    return $count;
}

function registeredusers(){
    $count = $this->count_where('status','Active');
    return $count;
}

function fixblank(){
    $this->load->model('mdl_users');
    $fixblank = $this->mdl_users->fixblank();
    $fixed = $this->get_where_custom('lastname','_');
    foreach($fixed->result('array') as $user){
        $data = $user;
        $data['link'] = base_url('users/updateuser/'.urlencode($data['firstname']).'/'.urlencode($data['lastname']).'/'.urlencode($data['email']).'/'.$this->makeHash($data['firstname'].'-'.$data['lastname'].'-'.$data['email']));
        $data['hash'] = $this->makeHash($data['firstname'].'-'.$data['lastname'].'-'.$data['email']);
        $this->_update($data['id'],$data);
    }
    $this->load->helper('file');
    $this->load->model('mdl_users');
    $csvdata = $this->mdl_users->get_fixblankusers_as_csv();
    $time = time();
    if ( !write_file('./assets/csvdownloads/fixedusers_'.$time.'.csv', $csvdata)){
        $alert['message'] = "User Export Failed";
        $alert['type'] = "error";
        $this->getusers($alert['type'],$alert['message']);
    }else{
        $alert['message'] = "User Export Successful";
        $alert['type'] = "success";
        $this->getusers($alert['type'],$alert['message']);
        $data = file_get_contents('./assets/csvdownloads/fixedusers_'.$time.'.csv'); // Read the file's contents
        if(!$data){
        die('File does not exist');
        }
        $name = 'fixedusers_'.$time.'.csv';
        
        force_download($name, $data);  
    }

    
}

function countreferrers($hash){
    $count = $this->count_where('referrer',$hash);
    if(!$count){
        return false;
    }else{
        return $count;
    }
    
}

function viewreferrers(){
    $vendorhash = $this->uri->segment(3);
    $data['access'] = "administrator";
    $users = $this->get_where_custom('referrer',$vendorhash);

    $data['users'] = $users;

    $data['title'] = "Refered Users";
    $data['view_file'] = "users";
    $data['module'] = "users";
    $this->admintemplate->build(true,$data);
    
}


function sendattachment($encodedkey, $attach){
   $server = $this->server;//'http://requestb.in';
  $url = 'api/documents/';//'1g6jni71';
  $this->load->library('Restclient');
  $config = array('server' => $server,
                    'api_key'         => 'd7f1569b1f00cadcda3e80acdd019230',
                    'api_name'        => '80e0153b',
                    'http_user'       => 'oapplication',
                    'http_pass'       => 'changemeplease',
                    'http_auth'       => 'basic');
   
   
   // foreach($attachments as $attach){
        
        $file = file_get_contents($attach);
        $attach64 = base64_encode($file);
//        print_r($file);
        $split = explode('.',$attach);
        $split2 = explode('/', $split[1]);
    
    
                    
  $params = array(
            'document' => array(
                    'documentHolderKey' => $encodedkey,
                    'documentHolderType' => 'CLIENT',
                    'name' => 'attachment',//end($split2),
                    'type' => end($split)
            ),
            'documentContent' => $attach64
  );
  
   $en = json_encode($params);     
    // Run some setup
    $this->restclient->initialize($config);
  $call = $this->restclient->post($url,$en,'json'/*$params*/);
    //$this->restclient->debug();
    //print_r($call);
    //print_r($en);
    return $call;
  //}
  
                    
}
 function getbankcode($bankname){
   $code =  array(
    'First Bank' => 011,  
    'Mainstreet Bank' => 014,
    'Heritage Bank' => 030,
    'Union Bank' => 032,
    'UBA' => 033,
    'Wema Bank' => 035,
    'Access Bank' => 044,
    'EcoBank' => 050,
    'Zenith Bank' => 057,
    'GT Bank' => 058,
    'Standard Chartered' => 068,
    'Fidelity Bank' => 070,
    'Skye Bank' => 076,
    'Keystone Bank' => 082,
    'Enterprise Bank' => 084,
    'FCMB' => 214,
    'Unity Bank' => 215,
    'Stanbic IBTC' => 221,
    'Sterling Bank' => 232,
    'Diamond Bank' => 0,
    'One Credit' => 0,
    'Union Assurance CO LTD' => 0,
    'CITI Bank' => 023,
    'JAIZ' => 233,
    'ASO Savings' =>401
);

    return ($code[$bankname]);
}

function addtomambu($userdetail){
  $server = $this->server;//'http://requestb.in';
  $url = 'api/clients/';//'1g6jni71';
  $this->load->library('Restclient');
    // Set config options (only 'server' is required to work)
    
    $newDate = date("c", strtotime($userdetail['dateofbirth']));
   // $newDate2 = $newDate->format('c');
    
    $config = array('server' => $server,
                    'api_key'         => 'd7f1569b1f00cadcda3e80acdd019230',
                    'api_name'        => '80e0153b',
                    'http_user'       => 'oolanipekun',//'oapplication',
                    'http_pass'       => 'command555',//'changemeplease',
                    'http_auth'       => 'basic');
                    
    //Parameters
    $params = array(
                    'client' => array(
                        'firstName' => $userdetail['firstname'],
                        'lastName' => $userdetail['surname'],
						'middleName' => $userdetail['middlename'],
                        'mobilePhone1' => '234'.ltrim($userdetail['mobilenumber'],'0'),
                        'homePhone' => '234'.ltrim($userdetail['homenumber'],'0'),
                        'birthDate'=>$newDate,
                        'gender' => $userdetail['gender'], 
                        'emailAddress' => $userdetail['email'],
                    ),
                   'idDocuments' => Array ( 
                                 '0' => Array ( 
                                        "documentType" => 'Staff ID',
                                        "documentId" => $userdetail['staffid']
                                 ),
                                 '1' => Array (
                                        "documentType" => $userdetail['meansofidentification'],
                                        "documentId" => $userdetail['idnumber']
                                 )
                  ),
                    'addresses' => array ( 
                          '0' => array ( 
                          'line1' => $userdetail['addressline1'],
						  'line2' => $userdetail['addressline2'].'  Landmark: '.$userdetail['nearestlandmark'],
                          'city'  => $userdetail['city'],
                          'region'=> $userdetail['state'],
                          'postcode'=> $userdetail['zip']
                     )
                  ),
                  'customInformation' => Array (
                                        '0' => Array ( 
                                                'value' => $userdetail['dependents'], 
                                                'customFieldID' => 'Dependents:_No._of_Children_Clie'
                                        ),
                                        '1' => Array ( 
                                                'value' => $userdetail['currentemployer'], 
                                                'customFieldID' => 'Employer_Clients'
                                        ),
                                        '2' => Array ( 
                                                'value' => $userdetail['employersaddressline1'],
                                                'customFieldID' => 'Office_Address_Clients'
                                        ),
                                        '3' => Array ( 
                                                'value' => $userdetail['monthlyincome'], 
                                                'customFieldID' => 'Net_pay_Clients'
                                        ),
                                        '4' => Array ( 
                                                'value' => $userdetail['accountnumber'],
                                                'customFieldID' => 'Account_Number_Clients'
                                        ),
                                        '5' => Array ( 
                                                'value' => ($userdetail['loanamount2'] + $userdetail['loanamount3']), 
                                                'customFieldID' => 'Other_Loans_NOT_reflected_in_you'
                                        ),
                                        '6' => Array ( 
                                                'value' => $userdetail['bankname'],
                                                'customFieldID' => 'Bank_Name_Clients'
                                        ),
                                        '7' => Array(
                                                'value' => $userdetail['bankaddress'],
                                                'customFieldID' => 'Bank_Branch_Clients'
                                        ),
                                        '8' => Array(
                                                'value' => $userdetail['employmenttype'],
                                                'customFieldID' => 'Employment_Status_Clients'
                                        ),
                                        '9' => Array(
                                                'value' => $userdetail['levelofeducation'],
                                                'customFieldID' => 'Level_of_Education__Clients'
                                        ),
                                        '10' => Array(
                                                'value' =>$userdetail['maritalstatus'],
                                                'customFieldID' => 'Marital_Status__Clients'
                                        ),
                                        '11' => Array(
                                                'value' => $userdetail['employersinlastfiveyears'], 
                                                'customFieldID' => 'Number_of_Employers_in_the_last_'
                                        ),
                                        '12' => Array(
                                                'value' => $userdetail['household'],
                                                'customFieldID' => 'Number_of_people_in_Household_Cl'
                                        ),
                                        '13' => Array(
                                                'value' => $userdetail['employmentdate'],
                                                'customFieldID' => 'Date_of_Employment_Clients'
                                        ),
                                        '14' => Array ( 
                                                'value' => $userdetail['residentialstatus'],
                                                'customFieldID' => 'Residential_Status_Clients'
                                        ),
                                        '15' => Array(
                                                'value' => $userdetail['jobtitle'],
                                                'customFieldID' => 'Occupation_Clients'
                                        ),
                                        '16' => Array(
                                                'value' => $userdetail['monthlyrepayments0'] + $userdetail['monthlyrepayments2'] + $userdetail['monthlyrepayments3'],
                                                'customFieldID' => 'Monthly_Repayments_of_other_outs'
                                        ),
                                        '17' => Array(
                                                'value' => $userdetail['kinname'],
                                                'customFieldID' => 'Next_of_Kin_Name_Clients'
                                        ),
                                        '18' => Array(
                                                'value' => $userdetail['kinrelationship'],
                                                'customFieldID' => 'Next_of_Kin_Relationship_Clients'
                                        ),
                                        '19' => Array(
                                                'value' => '234'.ltrim($userdetail['kinmobilenumber'],'0'),
                                                'customFieldID' => 'Next_of_Kin_Phone_Number_Clients'
                                        ),
                                        '20' => Array(
                                                'value' => $userdetail['kinaddressline1'],
                                                'customFieldID' => 'Next_of_Kin_Phone_Address_Client'
                                        ),
                                        '21' => Array(
                                                'value' => $userdetail['lastinstitution'],
                                                'customFieldID' => 'Last_Educational_Institution_Att'
                                        ),
                                        '22' => Array(
                                                'value' => $userdetail['loanamount0']+0,
                                                'customFieldID' => 'Loans_Reflected_in_your_Statemen'
                                        ),
                                        '23' => Array(
                                                'value' => $userdetail['payday'],
                                                'customFieldID' => 'Pay_Date_Clients'
                                        ),
                                        '24' => Array(
                                                'value' => $userdetail['monthlyexpense'],
                                                'customFieldID' => 'Average_Monthly_Household_Expens'
                                        ),
                                        '25' => Array(
                                                'value' => $userdetail['staffid'],
                                                'customFieldID' => 'Employee_Number_Clients'
                                        ),
                                        '26' => Array(
                                                'value' => $userdetail['staffsemail'],
                                                'customFieldID' => 'Official_email_Clients'
                                        ),
                                        '27' => Array(
                                                'value' => $this->getbankcode($userdetail['bankname']),
                                                'customFieldID' => 'bank_code'
                                        ),
                                        '28' => Array(
                                                'value' => $userdetail['kinaddressline2']." ",
                                                'customFieldID' => 'Next_of_Kin_Address_Line2'
                                        ),
                                        '29' => Array(
                                                'value' => $userdetail['kinstate'],
                                                'customFieldID' => 'nextofkin_state'
                                        ),
                                        '30' => Array(
                                                'value' => $userdetail['employersaddressline2']." ",
                                                'customFieldID' => 'Office_Address_Line2'
                                        ),
                                        '31' => Array(
                                                'value' => $userdetail['employersstate'],
                                                'customFieldID' => 'office_state'
                                        )
            )
                  
                  
                  
          );
     
   $en = json_encode($params);     
    // Run some setup
    $this->restclient->initialize($config);
  $call = $this->restclient->post($url,$en,'json'/*$params*/);
   // $this->restclient->debug();
  //print_r($params);
  //print_r($en);
  //print_r($call);
  //echo($userdetail['employersaddressline2']);
    return $call;
    

}
  
function checkapplicationstatus($clientid){
   $server = "http://apisandbox.one-cred.com";//'http://requestb.in';
  $url = 'mobileapi/checkmambuid.json';//'1g6jni71';
  $this->load->library('Restclient');
  $config = array('server' => $server);
  $params1 = array('id' => $clientid);
    // Run some setup
    $this->restclient->initialize($config);
    $call1 = $this->restclient->get($url,$params1);
  if(isset($call1->returnCode)){
    $clientStatus = "invalid";
  }elseif(!$call1){
    $clientStatus = "invalid";
  }elseif($call1->state == "ACTIVE"){
    $clientStatus = "active";
  }else{
    $clientStatus = "inactive";
  }

  $url = 'mobileapi/getactiveloan.json';
  $params2 = array('id' => $clientid);
  $call2 = $this->restclient->get($url,$params2);
  if(isset($call2->returnCode)){
    $loanStatus = "invalid";
  }elseif(!$call2){
    $loanStatus = "invalid";
  }elseif($call2[0]->accountState == "ACTIVE"){
    $loanStatus = "active";
  }else{
    $loanStatus = "inactive";
  }
    header("Content-Type: application/json");
  $response = json_encode(array('clientStatus' => $clientStatus,'loanStatus' => $loanStatus));
  echo $response;

}

}
?>
