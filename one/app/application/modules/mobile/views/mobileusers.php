<?php $this->load->module('admintemplate'); ?>

<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a> <span class="divider">/</span>
        </li>
        <li>
            <a href="#">Mobile Users</a>
        </li>
    </ul>
</div>

<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title >
            <h2><i class="icon-user"></i> Users</h2>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable ">
                <thead>
				    <tr>
                        <th>#</th>
                        <th>Client ID</th>
						<th>Firstname</th>
                        <th>Lastname</th>
                        <th>Email</th>
                        <th>Status</th>
						<th>&nbsp;</th>
				    </tr>
				</thead>   
				<tbody>
<?php
$count = 0;
$url = 'https://one-credit-mobile.firebaseio.com/users.json';
$this->load->library('Restclient');
$call = $this->restclient->get($url);
$array = get_object_vars($call);
//$json = json_decode($call);
//$array[57767]->fb->first_name);
foreach ($call as $key=>$value){
    $count++;
?>
                    <tr>
                        <td><?php echo ($count);?></td>
				        <td class="center"><?php if (!empty($value->clientid)){
                            echo ($value->clientid);
                        }else echo('<font color=\'red\'>&ltnot set&gt</font>'); ?>
                        </td>
                        <td class="center"><?php if (!empty($value->clientid)){
                            echo ($value->mambuclient->firstName);
                        }else echo('<font color=\'red\'>&ltnot set&gt</font>'); ?>
                        </td>
                        <td class="center"><?php if (!empty($value->clientid)){
                            echo ($value->mambuclient->lastName);
                        }else echo('<font color=\'red\'>&ltnot set&gt</font>'); ?>
                        </td>
                        <td class="center"><?php if(!empty($value->email)){echo ($value->email);}else echo('<font color=\'red\'>&ltnot set&gt</font>'); ?></td>
                        <td class="center"><?php if (!empty($value->clientid)){
                            echo ('<font color=\'green\'>Activated</font>');
                        } else{
                            echo('<font color=\'red\'>Not Activated</font>');
                        } ?>
                        </td>
                        <td class="center">
				            <a class=" edit btn btn-info <?php if (!empty($value->clientid)){echo("disabled");}?>" href=<?php if (!empty($value->clientid)){
                            echo ("https://onecred.mambu.com/#client.id=".$value->mambuclient->encodedKey.".type=indiv");
                            
                        } else{
                            echo('#');
                        } /*echo base_url('mobile/profile/'.$key);*/ ?> target="_blank" title="View User on Mambu">
				                <i class="icon-user icon-white"></i>                                     
				            </a>
				        </td>
                    </tr>
<?php
}
?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pagination pagination-center"><?php if(isset($pagenavi)){ echo $pagenavi; } ?></div>
</div>
<script type="text/javascript">
 $( "search" ).val();
</script>