<?php

class mobile extends MX_Controller
{
function __construct() {
parent::__construct();

$this->load->module('template');
$this->load->module('admintemplate');
$this->load->library('pagination');
}


function index(){
        $data['title'] = "Mobile";
        $data['view_file'] = "mobileusers";
        $data['module'] = "mobile";
        $data['access'] = "administrator";
        $this->admintemplate->build(true,$data); 
    }

function profile($id){
        $data['title'] = "Profile";
        $data['view_file'] = "profile";
        $data['module'] = "mobile";
        $data['id'] = $id;
        $data['access'] = "administrator";
        $this->admintemplate->build(true,$data); 
    }
function search($id){
        $data = $this->input->post();
        $data['title'] = "Search";
        $data['view_file'] = "search";
        $data['module'] = "mobile";
        $data['id'] = $data['searchh'];
        $data['access'] = "administrator";
        $this->admintemplate->build(true,$data); 
    }
}
?>