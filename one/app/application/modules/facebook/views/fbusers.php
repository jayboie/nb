<?php $this->load->module('admintemplate'); ?>

<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a> <span class="divider">/</span>
        </li>
        <li>
            <a href="#">Facebook Users</a>
        </li>
    </ul>
</div>

<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title >
            <h2><i class="icon-user"></i> Users</h2>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
				    <tr>
                        <th>#</th>
                        <th>ID</th>
						<th>Name</th>
                        <th>Email</th>
						<th>&nbsp;</th>
				    </tr>
				</thead>   
				<tbody>
<?php
$count = 0;
$url = 'https://onecred.firebaseio.com/users.json';
$this->load->library('Restclient');
$call = $this->restclient->get($url);
$array = get_object_vars($call);
//$json = json_decode($call);
//$array[57767]->fb->first_name);
foreach ($call as $key=>$value){
    $count++;
?>
                    <tr>
                        <td><?php echo ($count);?></td>
				        <td class="center"><?php echo ($key); ?></td>
                        <td class="center"><?php echo ($value->fb->first_name." ".$value->fb->last_name); ?></td>
                        <td class="center"><?php echo ($value->fb->email); ?></td>
                        <td class="center">
				            <a id="<?php echo ($key);?>" class=" edit btn btn-info" href="#" data-target="#ViewUser" data-toggle="modal" title="View User">
				                <i class="icon-user icon-white"></i>                                     
				            </a>
				        </td>
                    </tr>
<?php
}
?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pagination pagination-center"><?php if(isset($pagenavi)){ echo $pagenavi; } ?></div>
</div>

<!-- Edit User -->
<div id="ViewUser" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="ViewUserLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 id="EditUserLabel">User Profile</h3>
    </div>
    <div class="modal-body">
        <form class="form-horizontal">
            <div class="control-group">
                <label class="control-label" for="id">ID</label>
                <div class="controls">
                    <label class="control-label" id="idd">id number</label>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="email">Email</label>
                <div class="controls">
                    <label class="control-label" id="email">email</label>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="firstname">Firstname</label>
                <div class="controls">
                    <label class="control-label" id="firstname">firstname</label>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="lastname">Lastname</label>
                <div class="controls">
                    <label class="control-label" id="lastname">lastname</label>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="lang">Lang</label>
                <div class="controls">
                    <ul class="control-group" id="lang">
                    </ul>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="work">Work</label>
                <div class="controls">
                    <ul class="control-group" id="work">
                    </ul>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="edu">Edu</label>
                <div class="controls">
                    <ul class="control-group" id="edu">
                        
                    </ul>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="friends">Friends</label>
                <div class="controls">
                    <ul class="control-group" id="friends">
                        <li >1</li>
                        <li >2</li>
                    </ul>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
			$(document).ready(function(){
				$(".edit").on("click", function(){
				    $("#lang,#edu,#work,#friends").html("") ;
					$essay_id = $(this).attr('id');
                var $txt = <?php echo json_encode($array)?>;
					$('#idd').html($essay_id);
					$("#firstname").html($txt[$essay_id].fb.first_name);
					$("#lastname").html($txt[$essay_id].fb.last_name);
					$("#email").html($txt[$essay_id].fb.email);
                    var edu = $txt[$essay_id].fb.education;
                    var work = $txt[$essay_id].fb.work;
                    var lang = $txt[$essay_id].fb.languages;
                    var friends = $txt[$essay_id].fb.userfriends;
                    
                    $.each(
                            edu ,
                                function(i,v) {
                                $("#edu").append("<li class="+"list-group-item"+">" + v.type+": "+v.school.name +" ("+v.year.name+")"+ "</li>") ;
                                }
                            ) ;
                    $.each(
                            lang ,
                                function(i,v) {
                                $("#lang").append("<li class="+"list-group-item"+">" + v.name+ "</li>") ;
                                }
                            ) ;
                    $.each(
                            work ,
                                function(i,v) {
                                $("#work").append("<li class="+"list-group-item"+">" + v.position.name+" at "+v.employer.name+ "   ( "+check(v.start_date) +" to "+ check(v.end_date) +" )</li>") ;
                                }
                            ) ;
                   /* $.each(
                            friends ,
                                function(i,v) {
                                $("#work").append("<li class="+"list-group-item"+">" + v.position.name+" at "+v.employer.name+ "   ( "+check(v.start_date) +" to "+ check(v.end_date) +" )</li>") ;
                                }
                            ) ;
                            */      
				});
                
                function check(val){
                    if(typeof val == "undefined") 
                        return "   -";
                        else
                        return val;
                    }
			     })
</script>