<?php

class facebook extends MX_Controller
{

function __construct() {
parent::__construct();

$this->load->module('template');
$this->load->module('admintemplate');
$this->load->library('pagination');
}


function index(){
        $data['title'] = "Users";
        $data['view_file'] = "fbusers";
        $data['module'] = "facebook";
        $this->admintemplate->build(true,$data);   
    }
}

?>