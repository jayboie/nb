<?php

class Agents extends MX_Controller
{
private $firebaseURL = "https://onecred.firebaseio.com";
function __construct() {
parent::__construct();
$this->load->module('admintemplate');
}

private function get_form_data(){
    $data = $this->input->post();
    return $data;
}

function index(){
    $this->getAgents();
}

function getAgents($alert = '',$alert_type = ''){
    if($alert != ''){
        $data['alert'] = $alert;
        $data['alert_type'] = $alert_type;
    }
    $this->load->library('firebase', $this->firebaseURL);
    $data['title'] = "Agents";
    $data['view_file'] = "agents";
    $data['module'] = "agents";
    $data['access'] = "administrator";
    $data['agents'] = json_decode($this->firebase->get('/agents/'));
    $this->admintemplate->build(true,$data); 
}

function save(){
    $this->load->library('firebase', $this->firebaseURL);
    $formdata = $this->get_form_data();
    $upload = $this->uploadid(base64_encode($formdata['email']),'picture');
    if(!$upload){
        $data['alert'] = "Picture upload failed. Please, Try again";
        $data['alert_type'] = "error";
        $data['title'] = "Picture Upload Failed";
        $data['view_file'] = "agents";
        $data['module'] = "agents";
        $data['access'] = "administrator";
        $this->getAgents($data['alert'],$data['alert_type']);
    }else{
        $formdata['picture'] = $upload;
        $store = $this->firebase->push('/agents/',$formdata);
        if(!$store){
            $data['alert'] = "Jim is dead. He could not be saved :(";
            $data['alert_type'] = "error";
            $data['title'] = "Could not save agent";
            $data['view_file'] = "agents";
            $data['module'] = "agents";
            $data['access'] = "administrator";
            $this->getAgents($data['alert'],$data['alert_type']);
        }else{
            $data['alert'] = "Agent Saved";
            $data['alert_type'] = "success";
            $data['title'] = "Agent Added";
            $data['view_file'] = "agents";
            $data['module'] = "agents";
            $data['access'] = "administrator";
            $this->getAgents($data['alert'],$data['alert_type']);
        }
    }

}

function update(){
    $this->load->library('firebase', $this->firebaseURL);
    $formdata = $this->get_form_data();
    $id = $formdata['id'];
    unset($formdata['id']);
    if($_FILES['picture']['name']){
       $upload = $this->uploadid(base64_encode($formdata['email']),'picture');
       if(!$upload){
        $data['alert'] = "Picture upload failed. Please, Try again";
        $data['alert_type'] = "error";
        $data['title'] = "Picture Upload Failed";
        $data['view_file'] = "agents";
        $data['module'] = "agents";
        $data['access'] = "administrator";
        $this->getAgents($data['alert'],$data['alert_type']);
    }else{
        $formdata['picture'] = $upload;
        $store = $this->firebase->update('/agents/'.$id,$formdata);
        if(!$store){
            $data['alert'] = "He is asleep, nothing can be done for now. Please, Try again later :|";
            $data['alert_type'] = "error";
            $data['title'] = "Could not update agent";
            $data['view_file'] = "agents";
            $data['module'] = "agents";
            $data['access'] = "administrator";
            $this->getAgents($data['alert'],$data['alert_type']);
        }else{
            $data['alert'] = "Agent Updated";
            $data['alert_type'] = "success";
            $data['title'] = "Agent Updated";
            $data['view_file'] = "agents";
            $data['module'] = "agents";
            $data['access'] = "administrator";
            $this->getAgents($data['alert'],$data['alert_type']);
        }
    }
    }else{
        $store = $this->firebase->update('/agents/'.$id,$formdata);
        if(!$store){
            $data['alert'] = "He is asleep, nothing can be done for now. Please, Try again later :|";
            $data['alert_type'] = "error";
            $data['title'] = "Could not update agent";
            $data['view_file'] = "agents";
            $data['module'] = "agents";
            $data['access'] = "administrator";
            $this->getAgents($data['alert'],$data['alert_type']);
        }else{
            $data['alert'] = "Agent Updated";
            $data['alert_type'] = "success";
            $data['title'] = "Agent Updated";
            $data['view_file'] = "agents";
            $data['module'] = "agents";
            $data['access'] = "administrator";
            $this->getAgents($data['alert'],$data['alert_type']);
        }
    
    }
    
    
}

function delete($id){
    $this->load->library('firebase', $this->firebaseURL);
    $this->firebase->delete('/agents/'.$id);
    redirect('agents');
}

function uploadid($filename, $fieldname = ''){
	$config['upload_path'] = './assets/agents';
	$config['allowed_types'] = 'jpg|jpeg|gif|png';
	$config['max_size']	= '2048';
    $config['file_name'] = $filename;
    $config['overwrite'] = TRUE;
    if($fieldname == ''){
        $fieldname = 'userfile';
    }else{
        $fieldname = $fieldname;
    }
	$this->upload->initialize($config);
    
	if (!$this->upload->do_upload($fieldname)){
		return false;
	}
	else{
	   $savename = $this->upload->data();
		return $savename['file_name'];
	}
    
}

private function getdaysDiff($date){
    $today = date_create();
    $datetime2 = date_create($date);
    $interval = date_diff($datetime2, $today);
    return $interval->format('%a');
}

function setposition(){
    $rank = 1;
    $rankings = array();
    $this->load->library('firebase', "https://one-credit-agent.firebaseio.com");
    $agents_json = $this->firebase->get('standings');
    $agents = json_decode($agents_json);
    
    foreach ($agents as $key=>$value){
        $rankings[$key] = $value->Total;
   }
   arsort($rankings);
     
     
     foreach($rankings as $key=>$value){
        $data['Rank'] = $rank;
        $this->firebase->update('standings/'.$key,$data);
        $rank+=1;
     }
}

function delayedloans($username){
    $today = date("Y-m-d");      
    $this->load->library('firebase', "https://one-credit-agent.firebaseio.com");
    $delay = $this->firebase->get('standings/'.$username.'/Delayed');
    $url ='http://localhost/middleware/mobileapi/getloansbyagent?username='.$username.'&state=All.json';  //change to LIVE
    $this->load->library('Restclient');
    $accounts = $this->restclient->get($url);
    $delayed = 0;
    
    foreach($accounts as $account){
        $date = $account->creationDate;
        $state = $account->accountState;
        $olddate = strtotime($date);
       $new_date =  date("Y-m-d", $olddate); 
       if($today == $new_date && $state == 'PARTIAL_APPLICATION'){
            $delayed+=1;
       }
       
    }
    $data['Delayed'] = intval($delay) + $delayed;
    $this->firebase->update('standings/'.$username,$data);
    
    
}

function standings ($username){
    $this->load->library('firebase', "https://one-credit-agent.firebaseio.com");
    $data = array();
    $all = 0;
    $count_all = 0;
    $active = 0;
    $count_active = 0;
    $arr = 0;
    $count_arr = 0;
    $npl = 0;
    $count_npl = 0;
    $denied = 0;
    $delayed = 0;
    $count_delayed =0;
    $url ='http://localhost/middleware/mobileapi/getloansbyagent?username='.$username.'&state=All.json';  //change to LIVE
    $this->load->library('Restclient');
    $accounts = $this->restclient->get($url);
    //print_r($accounts);
    foreach($accounts as $account){
        $state = $account->accountState;
        $loanamount = $account->loanAmount;
        $principalBalance = $account->principalBalance;
        if($state != 'CLOSED_REJECTED' && $state !=  'PENDING_APPROVAL' && $state != 'PARTIAL_APPLICATION'){
            $all+=$loanamount;
            $count_all += 1;
            
        }
        if($state == 'ACTIVE_IN_ARREARS'){
            $last = $account->lastSetToArrearsDate;
            $days = $this->getdaysDiff($last);
            $arr += $loanamount;
            $count_arr +=1;
            if($days > 90 ){
                $npl+=$principalBalance;
                $count_npl+=1;
            }
        }
        if($state == 'ACTIVE'){
            $active+=$loanamount;
            $count_active += 1;
        }
        if($state == 'CLOSED_REJECTED'){
            $denied+=1;
        }
       
    }
    $Sall = $all;
    $Snpl = $npl;
    $totDiff = $Snpl - $denied;
    $total = (($active+$arr) - ($npl+$delayed+$denied))/10000;
    $total_count = $count_all;
     //$data['Agent'] = $username;
     $data['Sales'] = $Sall;
     $data['Arrears'] = $arr;
     $data['Active_count'] = $count_active;
     $data['Active'] = $active;
     $data['Arrears_count'] = $count_arr;
     $data['Sales_count'] = $count_all;
     $data['Npl'] = $Snpl;
     $data['Npl_count'] = $count_npl;
     $data['Denied'] = $denied;
     $data['Total'] = $total;
     $data['Total_count'] = $total_count;
     //$this->_insert($data);
     $store = $this->firebase->update('standings/'.$username,$data);
     $this->setposition();
}


function updatestandings(){
    $url = 'http://localhost/middleware/mobileapi/getalluser.json';
    $this->load->library('Restclient');
    $users = $this->restclient->get($url);
    foreach ($users as $user){
        if ($user->isCreditOfficer == 1 && $user->userState == 'ACTIVE'){
            echo("<br/>");
            print_r($user->username);
        }
    }
}


function _insert($data){
$this->load->model('mdl_agent');
$this->mdl_agent->_insert($data);
}

}

?>