<?php $this->load->module('admintemplate'); ?>
<?php $this->load->module('template'); ?>
<div>
				<ul class="breadcrumb">
					<li>
						<a href="#">Home</a> <span class="divider">/</span>
					</li>
					<li>
						<a href="#">Agents</a>
					</li>
				</ul>
			</div>
            <div class="row-fluid sortable">

                		<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-user"></i> Agents</h2>
                        <div class="box-icon">
					<a class="btn btn-primary" href="#" data-target="#AddAgents" data-toggle="modal" title="Add Agent">
										<i class="icon-plus icon-white"></i>
									</a>
                                    </div>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
                                    <th>#</th>
								  <th>Name</th>
								  <th>
                                    Phone Number
                                    </th>
                                     <th>
                                    Email
                                    </th>
                                    <th>&nbsp;</th>
							  </tr>
						  </thead>
						  <tbody>
<?php
$count = count($agents);
$c = 0;
if($count > 0){

forEach($agents as $key => $agent){
    $c++;
    ?>

							<tr>
                                <td><?php echo $c; ?></td>
								<td class="center"><?php echo $agent->name; ?></td>
                                <td class="center">
                                    <?php echo $agent->phone; ?>
                                    </td>
                                     <td class="center">
                                    <?php echo $agent->email; ?>
                                    </td>
                                    <td>
									<a class="btn btn-info" href="#" data-target="#EditAgent<?php echo $key; ?>" data-toggle="modal" title="Edit Agent">
										<i class="icon-edit icon-white"></i>
									</a>
									<a class="btn btn-danger" href="<?php echo base_url('agents/delete') ?>/<?php echo $key; ?>" title="Delete Agent">
										<i class="icon-trash icon-white"></i>
									</a>
                                    </td>
							</tr>
                            <?php }
                            }else { ?>
                                <tr>
<td colspan="5">
<div class="alert">
I could not find any Agent on the database. Try Adding One.
</div>
</td>
</tr>
                            <?php } ?>


						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
                </div> 
                
                <!-- Add User -->
<div class="modal fade" id="AddAgents" tabindex="-1" role="dialog" aria-labelledby="AddAgentsLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="AddAgentsLabel">Add Agent</h4>
          </div>
          <div class="modal-body">
          
          <form role="form" action="<?php echo base_url('agents/save') ?>" method="post" enctype="multipart/form-data">
          
          <div class="form-group">
            <label for="picture">Picture</label>
            <input type="file" class="form-control" name="picture" id="picture" required="yes"  />
          </div>
          <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" id="name" required="yes"  />
          </div>
          <div class="form-group">
            <label for="phone">Phone Number</label>
            <input type="tel" class="form-control" name="phone" id="phone" required="yes"  />
          </div>
          <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" name="email" id="email" required="yes"  />
          </div>
            
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Add Agent</button></form>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
<!-- Edit Agents -->
<?php 
forEach($agents as $key => $agent){ 
    ?>

<div id="EditAgent<?php echo $key; ?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="EditAgent<?php echo $key; ?>Label" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3 id="EditAgent<?php echo $key; ?>Label">Edit Agent</h3>
  </div>
<div class="modal-body">
          
          <form role="form" action="<?php echo base_url('agents/update') ?>" method="post" enctype="multipart/form-data">
          
          <div class="form-group">
            <label for="picture">Picture</label>
            <input type="file" class="form-control" name="picture" id="picture"  />
          </div>
          <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" value="<?php echo $agent->name; ?>" id="name" required="yes"  />
          </div>
          <div class="form-group">
            <label for="phone">Phone Number</label>
            <input type="tel" class="form-control" name="phone" value="<?php echo $agent->phone; ?>" id="phone" required="yes"  />
          </div>
          <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" name="email" value="<?php echo $agent->email; ?>" id="email" required="yes"  />
          </div>
            <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $key; ?>"  />
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Edit Agent</button></form>
            </div>
</div>

<?php } ?>