<?php $this->load->module('admintemplate'); ?>

<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a> <span class="divider">/</span>
        </li>
        <li>
            <a href="#">Message</a>
        </li>
    </ul>
</div>

<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title >
            <h2><i class="icon-envelope"></i> Message</h2>
        </div>
        <div class="box-content">
            <div>
                <form role="form" action="<?php echo base_url('message/send') ?>" method="post" enctype="multipart/form-data">

                <!-- Text input-->
                <div class="form-group">
                  <label for="recipient">To</label>
                    <input class="form-control" id="recipient" name="recipient" type="text" placeholder="client id" required>
                </div>
                
                <!-- Select Basic -->
                <div class="form-group">
                  <label class="control-label" for="Type">Type</label>
                  <div class="controls">
                    <select id="type" name="type" class="input-medium" required>
                      <option>Update</option>
                      <option>Special Offers</option>
                    </select>
                  </div>
                </div>
                
                <!-- Textarea -->
                <div class="form-group">
                  <label class="control-label" for="message">Message</label>
                  <div class="controls">                     
                    <textarea id="message" name="message" placeholder="message" required></textarea>
                  </div>
                </div>
                
                <!-- Button (Double) -->
                <div class="form-group">
                  <label class="control-label" for="send"></label>
                  <div class="controls">
                    <button type="submit" class="btn btn-primary">Send</button>
                    <button type="reset" class="btn btn-danger">Reset</button>
                  </div>
                </div>
                
                
                </form>

            </div>
        </div>
    </div>
</div>