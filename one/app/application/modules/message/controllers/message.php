<?php

class message extends MX_Controller
{

function __construct() {
parent::__construct();
date_default_timezone_set('Africa/Lagos');
$this->load->module('template');
$this->load->module('admintemplate');
$this->load->library('pagination');
}

private function get_form_data(){
    $data = $this->input->post();
    return $data;
}


function index($alert = '',$alert_type = ''){
    if($alert != ''){
        $data['alert'] = $alert;
        $data['alert_type'] = $alert_type;
    }
        $data['title'] = "Message";
        $data['view_file'] = "send";
        $data['module'] = "message";
        $this->admintemplate->build(true,$data);   
    }
    
    
function send(){
    $date = date('d/m/y-H:i:s');
    $timestamp = time();
        $this->load->library('firebase','https://one-credit-mobile.firebaseio.com');
        $formdata = $this->get_form_data();
        $formdata['date'] = $date;
        $formdata['unix'] = $timestamp;
        if($formdata['recipient'] == '*'){
            $data = json_decode($this->firebase->get('/Users/'));           
            foreach($data as $value){
                if(!empty($value->clientid)){
                    $store = $this->firebase->push('/notifications/'.$value->clientid.'/',$formdata);
                    if(!$store){
                        $dat['alert'] = "Error in sending message";
                        $dat['alert_type'] = "error";
                        $dat['title'] = "Could not send message";
                        $dat['view_file'] = "send";
                        $dat['module'] = "message";
                        $dat['access'] = "administrator";
                        $this->index($dat['alert'],$dat['alert_type']);
                    }else{
                        $dat['alert'] = "Message Sent";
                        $dat['alert_type'] = "success";
                        $dat['title'] = "Your message has been sent";
                        $dat['view_file'] = "send";
                        $dat['module'] = "message";
                        $dat['access'] = "administrator";
                        $this->index($dat['alert'],$dat['alert_type']);
                    }
                }
            }
            
            //loop
        }else{
            $store = $this->firebase->push('/notifications/'.$formdata['recipient'].'/',$formdata);
            if(!$store){
                $data['alert'] = "Error in sending message";
                $data['alert_type'] = "error";
                $data['title'] = "Could not send message";
                $data['view_file'] = "send";
                $data['module'] = "message";
                $data['access'] = "administrator";
                $this->index($data['alert'],$data['alert_type']);
            }else{
                $data['alert'] = "Message Sent";
                $data['alert_type'] = "success";
                $data['title'] = "Your message has been sent";
                $data['view_file'] = "send";
                $data['module'] = "message";
                $data['access'] = "administrator";
                $this->index($data['alert'],$data['alert_type']);
            }
        }
    
    
    }
}

?>