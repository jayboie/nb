<?php $this->load->module('admintemplate');?>

<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a> <span class="divider">/</span>
        </li>
        <li>
            <a href="#">Documents</a>
        </li>
    </ul>
</div>

<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title >
            <h2><i class="icon-file"></i>Document</h2>
        </div>
        <div class="box-content">
            <div class="tabbable"> <!-- Only required for left/right tabs -->
              <ul class="nav nav-tabs">
                <li class="<?php echo($uactive);?>"><a href="#tab1" data-toggle="tab">Upload</a></li>
                <li class="<?php echo($sactive);?>"><a href="<?php  if ($this->session->userdata('usertype') == "user"){echo('#');}else{echo('#tab2');}?>" data-toggle="tab">Search</a></li>
                <li class="<?php echo($bactive);  if ($this->session->userdata('usertype') == "user" ){echo('disabled');}?>"><a href="<?php  if ($this->session->userdata('usertype') == "user"){echo('#');}else{echo('#tab3');}?>" data-toggle="tab">Bulk</a></li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane <?php echo($uactive)?>" id="tab1">
                  <form class="form-horizontal" action="<?php echo base_url('bulkdocumentupload/upload'); ?>" method="post" enctype="multipart/form-data">
                    <div class="control-group">
                        <label for="type" class="control-label">
        					Document Type
        				</label>
        				<div class="controls">
                        <select name="type" id="type" required>
        						<option value="">-- choose type --</option>
        						<option value="BANK STATEMENT">BANK STATEMENT</option>
                                <option value="PASSPORT PHOTOGRAPH">PASSPORT PHOTOGRAPH</option>
                                <option value="CREDIT CHECK REPORT-CRS">CREDIT CHECK REPORT-CRS</option>
                                <option value="CREDIT CHECK REPORT-CRC">CREDIT CHECK REPORT-CRC</option>
                                <option value="APPLICATION DOCUMENTS">APPLICATION DOCUMENTS</option>
                        </select>
        				</div>
        			</div>
                
                    <div class="control-group">
        				<div class="controls">
                        <input type="file" id="userfile" name="userfile[]" multiple required>
        				</div>
        			</div>
                    
                    <div class="control-group">
        				<div class="controls">
                        <input type="submit"  name="submit" value="Upload" class="btn btn-primary" />
        				</div>
        			</div>
                </form>
            <?php
            $success = 0;
            $fail = 0;
           
            if(!empty($error)){
                echo($error);
            }
            if (!empty($response)){
                
           
            echo('Client ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Response<br/>');
                foreach($response as $key=>$value){
                    if($value != 'Success'){
                        $fail = $fail + 1;
                        echo($key.'&nbsp;&nbsp;&nbsp;&nbsp;<span style="color: #ffffff; background-color: red">'.$value.'</span><br/>');
                    }else{
                        $success = $success +1;
                        echo($key.'&nbsp;&nbsp;&nbsp;&nbsp;<span style="color: green;">'.$value.'</span><br/>');
                    }
                    
                } 
                
                echo('<br/>'.$success.' sucessful uploads<br/>'.$fail.' fail uploads') ; 
                 }
                 else{
                    
                 }         
            ?>
                </div>
                <div class="tab-pane <?php echo($sactive)?>" id="tab2">
                  <form class="form-horizontal" action="<?php echo base_url('bulkdocumentupload/getDocuments'); ?>" method="post" enctype="multipart/form-data">
                     <div class="control-group">
                    <label for="id" class="control-label">	
        					Client ID
        				</label>
        				<div class="controls">
                        <input type="text" id="id" name="id" multiple required>
        				</div>
        			</div>
                    
                    <div class="control-group">
                        <label for="type" class="control-label">	
        					Document Type
        				</label>
        				<div class="controls">
                        <select name="type" id="type" required>
        						<option value="">-- choose type --</option>
                                <option value="All">All</option>        						
                        </select>
        				</div>
        			</div>
                
                   
                    <div class="control-group">
        				<div class="controls">
                        <input type="submit"  name="submit" value="Search" class="btn btn-primary" />
                        <input type="reset" class="btn btn-danger" value="Reset"/>
        				</div>
        			</div>
                </form>
                <?php
                if(!empty($error)){
                    echo($error);
                }else{
                    echo('&nbsp;&nbsp;&nbsp;&nbsp;<span style="align:center; color: green;font-size: 20px;">'.$name.'</span><br/>');
                }
                if (!empty($res)){
                   // print_r($res);
                    
                ?>
                <br />
                <table class="table table-striped table-bordered bootstrap-datatable datatable ">
                <thead>
				    <tr>
                        <th>#</th>
                        <th>Document Type</th>
						<th>file Type</th>
                        <th>Created</th>
						<th>&nbsp;</th>
				    </tr>
				</thead>   
				<tbody>
            
                <?php
                $counter = 0;
                    foreach($res as $doc){
                        $counter = $counter + 1;
                        
                ?>
                
                <tr>
                    <th ><?php echo($counter);?></th>
                    <th ><?php echo($doc->name);?></th>
                    <th ><?php echo($doc->type);?></th>
                    <th ><?php echo(date_format(date_create($doc->creationDate),'Y-M-d'));?></th>
                    <th ><a class=" edit btn btn-info" href="<?php echo base_url('bulkdocumentupload/download/'.$doc->encodedKey.'/'.$doc->name.'/'.$doc->type) ?>" title="View User on Mambu">
				                <i class="icon-download icon-white"></i>                                     
				            </a></th>
                </tr>
                
                <?php
                 }
                 ?>
                 
                 </tbody>
                 </table>
                 <?php
                }?>
                </div>
                <div class="tab-pane <?php echo($bactive)?>" id="tab3">
                <form class="form-horizontal" action="<?php echo base_url('bulkdocumentupload/bulkUpload'); ?>" method="post" enctype="multipart/form-data">
                    <div class="control-group">
                        <label for="type" class="control-label">	
        					Document Type
        				</label>
        				<div class="controls">
                        <select name="type" id="type" required>
        						<option value="">-- choose type --</option>
                                <option value="all">All</option>
        						<option value="BANK STATEMENT">BANK STATEMENT</option>
                                <option value="PASSPORT PHOTOGRAPH">PASSPORT PHOTOGRAPH</option>
                                <option value="CREDIT CHECK REPORT-CRS">CREDIT CHECK REPORT-CRS</option>
                                <option value="CREDIT CHECK REPORT-CRC">CREDIT CHECK REPORT-CRC</option>
                                <option value="APPLICATION DOCUMENTS">APPLICATION DOCUMENTS</option>
                        </select>
        				</div>
        			</div>
                
                    <div class="control-group">
                    <label for="file" class="control-label">	
        					Client IDs
        				</label>
        				<div class="controls">
                        <input type="file" id="userfile" name="userfile" required>
        				</div>
        			</div>
                    
                    <div class="control-group">
        				<div class="controls">
                        <input type="submit"  name="submit" value="Get Files" class="btn btn-primary" />
        				</div>
        			</div>
                </form>
                <?php
                if(!empty($error)){
                    echo($error);
                    if(!empty($name)){
                         echo('<div class="controls">');
                    echo('<br/><a class="btn btn-default" href="'.base_url('bulkdocumentupload/viewlog').'">View log</a>');
                    echo('&nbsp;&nbsp;');
                    echo('<a class="btn btn-primary" href="'.base_url('bulkdocumentupload/getarchive/'.$name.'/').'">Get Archive</a>');
                    echo('</div>');
                    }
                }
                ?>
                </div>
              </div>
            </div>   
        </div>
    </div>
</div>
