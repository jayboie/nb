<?php

class Bulkdocumentupload extends MX_Controller
{
var $server = 'https://onecred.mambu.com';
var $apiname = '80e0153b';//'80e0153b';
var $apikey = 'd7f1569b1f00cadcda3e80acdd019230';//'1a99df9493b72bb26f7609be1e211fab';
var $user = 'oapplication';//'bulkupload';
var $pass = 'changemeplease';

function __construct() {
parent::__construct();
$this->load->module('template');
$this->load->module('admintemplate');
$this->load->library('pagination');
}

function index(){
    $response = array();
    $error = "";
    $res = array();
    $name = '';
     $data['title'] = "Documents";
     $data['view_file'] = "upload";
     $data['module'] = "bulkdocumentupload";
     $data['access'] = "administrator";
      $data['response'] = $response;
      $data['error'] = $error;
      $data['uactive'] = 'active';
      $data['sactive'] = '';
      $data['bactive'] = '';
      $data['res'] = $res;
      $data['name'] = $name;
     $this->admintemplate->build(true,$data); 
}

function upload(){ 
    $data = $this->input->post();
    if(isset($_POST['submit'])){
		$config['upload_path'] = './documents/';
		$config['allowed_types'] = 'pdf|txt|doc|docx|jpg|jpeg|png|xls|xlsx';
		$this->upload->initialize($config);
        
        
        if($this->upload->do_multi_upload('userfile')){
            $path = './documents/';
            $files = array_diff(scandir($path), array('..', '.'));
            foreach($files as $file){
                $res = $this->uploadDocument($file,$data['type'],$path.$file);
                $split = explode('.',$file);
                $response[$split[0]] = $res;
            }
             $data['title'] = "Documents";
             $data['view_file'] = "upload";
             $data['module'] = "bulkdocumentupload";
             $data['access'] = "administrator";
             $data['response'] = $response;
             $data['uactive'] = 'active';
             $data['bactive'] = '';
             $data['sactive'] = '';
             $this->admintemplate->build(true,$data); 
           // print_r($response);
		}else{
		  $error  = array('error' => $this->upload->display_errors());
          $data['title'] = "Documents";
             $data['view_file'] = "upload";
             $data['module'] = "bulkdocumentupload";
             $data['access'] = "administrator";
             $data['uactive'] = 'active';
             $data['bactive'] = '';
             $data['sactive'] = '';
             $data['error'] = $error['error'];
             $this->admintemplate->build(true,$data); 
        }
	
    }
}

/**
 * Mambu::uploadDocument()
 * 
 * @param mixed $id
 * @param mixed $file
 * @return Object
 */
private function uploadDocument($file,$name,$dir){
    
    $this->load->library('restclient');
    $server = $this->server;
    $url = 'api/documents/';
   
    
       
        $split = explode('.',$file);
         $doc = file_get_contents($dir);
        $doc64 = base64_encode($doc);
        
        $res = $this->getEncodedkey($split[0]);
        
        if(array_key_exists('returnStatus', $res)){
            //echo($res->returnStatus);
            unlink($dir);
            return $res->returnStatus;
        }else{
            $holderKey = $res->encodedKey;
       
        
        
    $config = array('server' => $server,
                    'api_key'         => $this->apikey,
                            'api_name'        => $this->apiname,
                            'http_user'       => $this->user,//'oolanipekun',
                            'http_pass'       => $this->pass,//'command555',
                            'http_auth'       => 'basic');
                        
                    
    //Parameters
    $params = array( 'document'=>array(
                        'documentHolderKey' => $holderKey,
                        'documentHolderType' => 'CLIENT',
                        'name' => $name,
                        'type' => end($split),
                ),
                'documentContent' => $doc64
    );
                
    // Run some setup
    $en = json_encode($params); 
    $this->restclient->initialize($config);
    
    $call = $this->restclient->post($url,$en,'json');
        if(array_key_exists('returnStatus', $call)){
            return $call->returnStatus;
        }else{
            unlink($dir);
            return 'Success';
        }
    
     }
} 



/**
 * Quickteller::getEncodedkey()
 * 
 * @param mixed $id
 * @return String
 */
private function getEncodedkey($id){
            $this->load->library('restclient');
            
            $server = $this->server;
            $url = 'api/clients/'.$id;
            // Set config options (only 'server' is required to work)
            
            $config = array('server' => $server,
                            'api_key'         => $this->apikey,
                            'api_name'        => $this->apiname,
                            'http_user'       => $this->user,//'oolanipekun',
                            'http_pass'       => $this->pass,//'command555',
                            'http_auth'       => 'basic');
                        
            // Run some setup
            $this->restclient->initialize($config);
            
            $call = $this->restclient->get($url);
           // $this->restclient->debug();
            return $call;
}



/**
 * Quickteller::getDocuments()
 * 
 * @param mixed $id
 * @return String
 */
function getDocuments(){
    $data = $this->input->post();
    $id = $data['id'];
    $type = $data['type'];
    
    $res= $this->getEncodedkey($id);
        
        if(array_key_exists('returnStatus', $res)){
            $clierror = $res->returnStatus;
            $data['title'] = "Documents";
             $data['view_file'] = "upload";
             $data['module'] = "bulkdocumentupload";
             $data['access'] = "administrator";
             $data['uactive'] = '';
             $data['sactive'] = 'active';
             $data['bactive'] = '';
             $data['error'] = $clierror;
             $this->admintemplate->build(true,$data); 
        }else{
            $name = $res->firstName.' '.$res->lastName;
                $encoded = $res->encodedKey;
            $this->load->library('restclient');
            $server = $this->server;
            $url = '/api/clients/'.$encoded.'/documents'; //'api/clients/'.$id;
            // Set config options (only 'server' is required to work)
            
            $config = array('server' => $server,
                            'api_key'         => $this->apikey,
                            'api_name'        => $this->apiname,
                            'http_user'       => $this->user,//'oolanipekun',
                            'http_pass'       => $this->pass,//'command555',
                            'http_auth'       => 'basic');
                        
            // Run some setup
            $this->restclient->initialize($config);
            
            $call = $this->restclient->get($url);
            //$this->restclient->debug();
            $data['title'] = "Documents";
             $data['view_file'] = "upload";
             $data['module'] = "bulkdocumentupload";
             $data['access'] = "superadministrator";
             $data['uactive'] = '';
             $data['sactive'] = 'active';
             $data['bactive'] = '';
             $data['res'] = $call;
             $data['name'] = $name;
             $this->admintemplate->build(true,$data); 
            //return $call;
}

}

/**
 * Quickteller::download()
 * 
 * @param mixed $id
 * @return String
 */
function download($id,$name,$ext){
            $this->load->library('restclient');
            $this->load->helper('download');
            $server = $this->server;
            $url = '/api/documents/'.$id;
            // Set config options (only 'server' is required to work)
            
            $config = array('server' => $server,
                            'api_key'         => $this->apikey,
                            'api_name'        => $this->apiname,
                            'http_user'       => $this->user,//'oolanipekun',
                            'http_pass'       => $this->pass,//'command555',
                            'http_auth'       => 'basic');
                        
            // Run some setup
            $this->restclient->initialize($config);
            
            $call = $this->restclient->get($url);
            $doc_decoded = base64_decode ($call);
            //$this->restclient->debug();
            force_download($name.'.'.$ext, $doc_decoded);
            return null;
}

function deletedata(){
    //$name = urldecode($namee);
    //unlink('./download/log/log.txt');
    //array_map('unlink', glob('./download/'.$name.'/*'));
    $this->rrmdir('./download/');
    mkdir('./download/');
    mkdir('./download/BANK STATEMENT/');
    mkdir('./download/APPLICATION DOCUMENTS/');
    mkdir('./download/PASSPORT PHOTOGRAPH/');
    mkdir('./download/CREDIT CHECK REPORT-CRS/');
    mkdir('./download/CREDIT CHECK REPORT-CRC/');
    mkdir('./download/log/');
    mkdir('./download/all/');
}

function rrmdir($dir) {
   if (is_dir($dir)) {
     $objects = scandir($dir);
     foreach ($objects as $object) {
       if ($object != "." && $object != "..") {
         if (filetype($dir."/".$object) == "dir") $this->rrmdir($dir."/".$object); else unlink($dir."/".$object);
       }
     }
     reset($objects);
     rmdir($dir);
   }
}

function viewlog(){
    $this->load->helper('download');
    if(file_exists('./download/log/log.txt')){
        $data = file_get_contents('./download/log/log.txt'); // Read the file's contents
        // unlink('./download/log/log.txt');
        $name = 'log.txt';
        force_download($name, $data);
    }else{
        $data['title'] = "Documents";
        $data['view_file'] = "upload";
        $data['module'] = "bulkdocumentupload";
        $data['access'] = "superadministrator";
        $data['uactive'] = '';
        $data['sactive'] = '';
        $data['bactive'] = 'active';
        $data['error'] = 'No Log file';
        $this->admintemplate->build(true,$data);
    }
   
}

function getarchive($namee){
    $name = urldecode($namee);
    $this->load->library('zip');
    $dir = './download/'.$name.'/';
    if(is_dir($dir)){
        if(sizeof(scandir($dir)) > 2){
            $this->zip->read_dir($dir,FALSE); 
            //array_map('unlink', glob('./download/'.$name.'/*'));
            $this->zip->download($name.'.zip');
            
            
        }else{
            $data['title'] = "Documents";
        $data['view_file'] = "upload";
        $data['module'] = "bulkdocumentupload";
        $data['access'] = "superadministrator";
        $data['uactive'] = '';
        $data['sactive'] = '';
        $data['bactive'] = 'active';
        $data['name'] = $name;
        $data['error'] = 'Files Not present, Please view log to check errors';
        $this->admintemplate->build(true,$data);
        }
    }else{
        $data['title'] = "Documents";
        $data['view_file'] = "upload";
        $data['module'] = "bulkdocumentupload";
        $data['access'] = "superadministrator";
        $data['uactive'] = '';
        $data['sactive'] = '';
        $data['bactive'] = 'active';
        $data['error'] = 'Files Not present, Please Re-run the operation';
        $this->admintemplate->build(true,$data);
        
    }
    
}


private function getMultiDocument($arr,$name){
    $docArryy = array(); //array containing documents
        $this->load->library('restclient');
        $this->load->helper('download');
        $server = $this->server;
        $config = array('server' => $server,
                        'api_key'         => $this->apikey,
                        'api_name'        => $this->apiname,
                        'http_user'       => $this->user,//'oolanipekun',
                        'http_pass'       => $this->pass,//'command555',
                        'http_auth'       => 'basic');
         $oldID='';           
        // Run some setup
        $this->restclient->initialize($config);
        foreach($arr as $encod){
        $url = '/api/clients/'.trim($encod).'/documents';
        $calld = $this->restclient->get($url);
        //print_r($calld);
        if(empty($calld)){
            file_put_contents('./download/log/log.txt', trim($encod)."\tVALID BUT NO DOCUMENT ATTACHED TO THE ID\n", FILE_APPEND | LOCK_EX);
        }else{
            foreach ($calld as $doc){
               // print_r($calld);
                    if($name = "all"){
                        $url2 = '/api/documents/'.$doc->encodedKey;
                         $call2 = $this->restclient->get($url2);
                         //print_r($call2);
                        $doc_decoded = base64_decode($call2);
                        if(file_exists('./download/all/'.trim($encod).'/')){
                            //unlink('./download/log/log.txt');
                            }else{
                                mkdir('./download/all/'.trim($encod).'/');
                            }
                       
                        
                        file_put_contents('./download/all/'.trim($encod).'/'.$doc->name.'.'.$doc->type, $doc_decoded);
                        file_put_contents('./download/log/log.txt', trim($encod)."\tVALID WITH DOCUMENT\n", FILE_APPEND | LOCK_EX);
                    }
                    elseif($doc->name == $name ){
                        $url2 = '/api/documents/'.$doc->encodedKey;
                         $call2 = $this->restclient->get($url2);
                        $doc_decoded = base64_decode ($call2);
                        file_put_contents('./download/'.$name.'/'.trim($encod).'.'.$doc->type, $doc_decoded);
                        file_put_contents('./download/log/log.txt', trim($encod)."\tVALID WITH DOCUMENT\n", FILE_APPEND | LOCK_EX);
                    }else{
                        if ($oldID == $encod){
                            //do nothing
                        }else{
                        file_put_contents('./download/log/log.txt', trim($encod)."\tVALID BUT NO DOCUMENT OF THIS TYPE\n", FILE_APPEND | LOCK_EX);
                    } 
                    }
                    $oldID = $encod;     
                }
                $oldID = '';
            }
    }
    return true;
}


function bulkUpload(){ 
    $this->deletedata();
    if(file_exists('./download/log/log.txt')){
    unlink('./download/log/log.txt');
    }
    $data = $this->input->post();
    if(isset($_POST['submit'])){
		$config['upload_path'] = './documents/';
		$config['allowed_types'] = 'txt';
        $config['file_name'] = 'clients'.rand(1000,9000);
		$this->upload->initialize($config);
        $count = 0;
        $clients = array();//stores the client id from file
        $status = array();//stores the status client id using the id as keys and the staus and value
        $valid = array();// stores valid IDs
        
        if($this->upload->do_upload('userfile')){
            $this->load->helper('file');
            $path = './documents/';
            
            $contents = fopen($path.$config['file_name'].'.txt','r');
            if($contents){
                while (!feof($contents)){
                    $clients[$count] = fgets($contents);
                    $count++;                    
                }
            }else{
                    //error
                }
            fclose($contents);
            unlink($path.$config['file_name'].'.txt');
            foreach ($clients as $client){
                $res = $this->getEncodedkey($client);
                if(array_key_exists('returnStatus', $res)){
                    $status[$client] = $res->returnStatus;
                    file_put_contents('./download/log/log.txt', trim($client)."\t".$res->returnStatus."\n", FILE_APPEND | LOCK_EX);
                }else{
                    $status[$client] = 'VALID';
                    array_push($valid,$client);
                    }
                
            }
           // print_r($valid);
            $this->getMultiDocument($valid,$data['type']);
            
             $data['title'] = "Documents";
             $data['view_file'] = "upload";
             $data['module'] = "bulkdocumentupload";
             $data['access'] = "superadministrator";
             $data['uactive'] = '';
             $data['sactive'] = '';
             $data['error'] = 'success: '.sizeof($clients).'  entries recieved';
             $data['bactive'] = 'active';
             $data['name'] = $data['type'];
             $this->admintemplate->build(true,$data); 
		}else{
		  $error  = array('error' => $this->upload->display_errors());
          $data['title'] = "Documents";
             $data['view_file'] = "upload";
             $data['module'] = "bulkdocumentupload";
             $data['access'] = "superadministrator";
             $data['uactive'] = '';
             $data['sactive'] = '';
             $data['bactive'] = 'active';
             $data['error'] = $error['error'];
             $this->admintemplate->build(true,$data); 
        }
	
    }
}
}


?>
