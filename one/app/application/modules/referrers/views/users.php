<?php $this->load->module('admintemplate'); ?>
<?php $this->load->module('users'); ?>
<div>
				<ul class="breadcrumb">
					<li>
						<a href="#">Home</a> <span class="divider">/</span>
					</li>
					<li>
						<a href="#">Referrers</a>
					</li>
				</ul>
			</div>
			
			<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-user"></i> Referrers</h2>
                        <div class="box-icon">
					<a class="btn btn-primary" href="#" data-target="#AddReferrer" data-toggle="modal" title="Add Referrer">
										<i class="icon-plus icon-white"></i>                                          
									</a>
                                    </div>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
                                    <th>#</th>
								  <th>Referrer Name</th>
								  <th>
                                    Hash
                                    </th>
                                    <th>Users</th>
                                    <th>
                                    &nbsp;
                                    </th>
							  </tr>
						  </thead>   
						  <tbody>
<?php 
if(isset($c)){
    $c = $c;
}else{
    $c = 1;
}

$usercount = count($users->result());
if($usercount > 0):
foreach($users->result() as $user){
 ?>
							<tr>
                                <td><?php echo $c; ?></td>
								<td class="center"><?php echo $user->referrer; ?></td>
                                <td class="center"><?php echo $user->hash; ?></td>
                                <td class="center"><?php echo $this->users->countreferrers($user->hash); ?></td>
                                <td class="center">
                                <a class="btn btn-success" href="<?php echo base_url('users/viewreferrers') ?>/<?php echo $user->hash; ?>"  title="View Referrer Users">
										<i class="icon-search icon-white"></i>                                     
									</a>
                                	<a class="btn btn-info" href="#" data-target="#EditReferrer<?php echo $user->id; ?>" data-toggle="modal" title="Edit Referrer">
										<i class="icon-edit icon-white"></i>                                     
									</a>
									<a class="btn btn-danger" href="<?php echo base_url('referrers/deleteuser') ?>/<?php echo $user->id; ?>" title="Delete Referrer">
										<i class="icon-trash icon-white"></i>
									</a>
								</td>
							</tr>
<?php $c++;
 }
else: ?>
<tr>
<td colspan="4">
<div class="alert">
I could not find any user on the database. Try Adding One.
</div>
</td>
</tr>
<?php endif
 ?>
							
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
            <div class="pagination pagination-center"><?php if(isset($pagenavi)){ echo $pagenavi; } ?></div>
      
<?php 
foreach($users->result() as $user){
 ?>


<!-- Edit Referrer -->
<div id="EditReferrer<?php echo $user->id; ?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="EditReferrer<?php echo $user->id; ?>Label" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3 id="EditReferrer<?php echo $user->id; ?>Label">Edit Referrer</h3>
  </div>
<div class="modal-body">
          
          <form role="form" action="<?php echo base_url('referrers/updateuser'); ?>" method="post">
          
          <div class="form-group">
            <label for="referrer">Referrer Name</label>
            <input type="text" class="form-control" name="referrer" id="referrer" value="<?php echo @$user->referrer; ?>"   />
          </div>
          
          <input type="hidden" name="id" value="<?php echo @$user->id; ?>" />
            <!--<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />-->
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Update Referrer</button></form>
            </div>
</div>

<?php } ?>




      
<!-- Add Referrer -->
<div class="modal fade" id="AddReferrer" tabindex="-1" role="dialog" aria-labelledby="AddReferrerLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="AddReferrerLabel">Add Referrer</h4>
          </div>
          <div class="modal-body">
          
          <form role="form" action="<?php echo base_url('referrers/adduser'); ?>" method="post" >
          
          <div class="form-group">
            <label for="referrer">Referrer Name</label>
            <input type="text" class="form-control" name="referrer" id="referrer"   />
          </div>

<!--            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />-->
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Add Referrer</button></form>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
    <script>
$(document).ready(function(){

$('.btn-danger').click(function(e){
    e.preventDefault();
    $confirm = confirm('Are you sure?');
    if(!$confirm){
        return false;
    }else{
        window.location.href = $(this).attr('href');
    }
});
  
}

);

</script>