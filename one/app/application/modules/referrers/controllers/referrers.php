<?php

class Referrers extends MX_Controller
{

function __construct() {
parent::__construct();

$this->load->module('admintemplate');
$this->load->library('pagination');
//$this->output->enable_profiler(TRUE);
}

function get($order_by){
$this->load->model('mdl_referrers');
$query = $this->mdl_referrers->get($order_by);
return $query;
}

function get_with_limit($limit, $offset, $order_by) {
$this->load->model('mdl_referrers');
$query = $this->mdl_referrers->get_with_limit($limit, $offset, $order_by);
return $query;
}

function get_where($id){
$this->load->model('mdl_referrers');
$query = $this->mdl_referrers->get_where($id);
return $query;
}

function get_where_custom($col, $value) {
$this->load->model('mdl_referrers');
$query = $this->mdl_referrers->get_where_custom($col, $value);
return $query;
}

function get_where_like($field,$key){
$this->load->model('mdl_referrers');
$query = $this->mdl_referrers->get_where_like($field,$key);
return $query;
}


function _insert($data){
$this->load->model('mdl_referrers');
$this->mdl_referrers->_insert($data);
}

function _update($id, $data){
$this->load->model('mdl_referrers');
$this->mdl_referrers->_update($id, $data);
}

function _update_where($col,$col_val, $data){
    $this->load->model('mdl_referrers');
    $this->mdl_referrers->_update_where($col,$col_val, $data);
}

function _delete($id){
$this->load->model('mdl_referrers');
$this->mdl_referrers->_delete($id);
}

function count_where($column, $value) {
$this->load->model('mdl_referrers');
$count = $this->mdl_referrers->count_where($column, $value);
return $count;
}

function get_max() {
$this->load->model('mdl_referrers');
$max_id = $this->mdl_referrers->get_max();
return $max_id;
}

function _custom_query($mysql_query) {
$this->load->model('mdl_referrers');
$query = $this->mdl_referrers->_custom_query($mysql_query);
return $query;
}

function get_where_custom_with_limit($col, $value, $limit, $offset, $order_by) {
$this->load->model('mdl_referrers');
$query = $this->mdl_referrers->get_where_custom_with_limit($col, $value, $limit, $offset, $order_by);
return $query;
}

function get_form_data(){
    $data = $this->input->post();
    return $data;
}

function index(){
$this->getreferrers();
}


function adduser(){
$userdata = $this->get_form_data();
$data = $userdata;
$data['access'] = 'administrator';
//check if user exist in database
$checkusername = $this->count_where('referrer',$userdata['referrer']);

if($checkusername < 1){

    $userdata['hash'] = substr($this->makeHash($userdata['referrer']),0,7);
        $this->_insert($userdata);

        $data['alert'] = "Referrer created successfully";
        $data['alert_type'] = "success";
        $data['message'] = $data['alert'];
        $data['type'] = $data['alert_type'];

        $this->getreferrers($data['alert_type'],$data['message']);

}else{
    unset($this->input);
    $data['alert'] = "This referrer already exists.";
    $data['alert_type'] = "warning";
    $data['message'] = $data['alert'];
    $data['type'] = $data['alert_type'];

    $this->getreferrers($data['type'],$data['message']);

}
}


function getreferrers($alerttype = "",$alertmessage = ""){
    
    $alluser = $this->get('id');
    $total = $alluser->num_rows();
    $config['base_url'] = base_url('referrers/getreferrers');
    $config['total_rows'] = $total;
    $config['per_page'] = 50;
    $config['full_tag_open'] = '<ul>';
    $config['full_tag_close'] = '</ul>';
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $config['prev_tag_open'] = '<li>';
    $config['prev_tag_close'] = '</li>';
    $config['next_tag_close'] = '</li>';
    $config['next_tag_open'] = '<li>';
    $config['cur_tag_open'] = '<li class="active"><a href="#">';
    $config['cur_tag_close'] = '</a></li>';
    $this->pagination->initialize($config); 
    $data['pagenavi'] = $this->pagination->create_links();
    if($alerttype != "" && $alertmessage != ""){
        $data['alert'] = $alertmessage;
        $data['alert_type'] = $alerttype;
    }
    
    if(!$this->uri->segment(3)){
        $offset = 0;
    }else{
        $offset = $this->uri->segment(3);
        $data['c'] = $offset + 1;
    }
    $referrers = $this->get_with_limit($config['per_page'],$offset,'id');
    
    
    
    $data['access'] = 'administrator';
    
    $data['users'] = $referrers;
    $data['title'] = "Referrers";
    $data['view_file'] = "users";
    $data['module'] = "referrers";
    $this->admintemplate->build(true,$data);
}

function updateuser(){
    $userdata = $this->get_form_data();
    if(isset($userdata['id'])){

    $userdata['hash'] = $this->makeHash($userdata['referrer']); 

    $this->_update($userdata['id'],$userdata);
    $alert['message'] = "The Referrer has been updated successfully";
    $alert['type'] = "success";
    $this->getreferrers($alert['type'],$alert['message']);
}
}

function deleteuser(){
    $id = $this->uri->segment(3);
    $this->_delete($id);
    redirect('referrers/getreferrers');
}

/**
 * referrers::makeHash()
 * generate a secure hash string from a sring and a salt
 * @param mixed $data
 * @param string $salt
 * @return string
 */
function makeHash($data, $salt = "%gd:FG{hdfVFDds6egNNKYRfe dr"){
    
    /* Really? I think this is a little crazy, but not too crazy though..  */
    
    $secString = "";
    $hashData = do_hash($data);
    $hashSalt = do_hash($salt);
    $secString .= $hashData;
    $secString .= $hashSalt;
    $secString .= $hashData;
    
    $hash = do_hash($secString);
    $secHash = do_hash($hash);
    $token = do_hash($secHash);
   // echo $token."<br />";
    return $token;
    
}


function getReferrerName($hash){
    $referrers = $this->get_where_custom('hash',$hash);
    foreach($referrers->result() as $referrer){
        $name = $referrer->referrer;
    }
    if(count($referrers->result()) < 1){
        return false;
    }else{
       return $name; 
    }
    
}

function getReferrerHash($referrer){
    $referrers = $this->get_where_custom('referrer',$referrer);
    foreach($referrers->result() as $referrer){
        $hash = $referrer->hash;
    }
    if(count($referrers->result()) < 1){
        return false;
    }else{
       return $hash;
    }
    
}

function readReferrers(){
    $referrers = $this->get('referrer');
    return $referrers;
}

function countreferrers(){
    $this->load->model('mdl_referrers');
    $count = $this->mdl_referrers->count_all();
    return $count;
}
 
}

?>